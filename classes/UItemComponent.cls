public class UItemComponent {
    
    public static void setSuggestedRetail(List<Item_Component__c> records, Map<Id,Item_Component__c> oldMap) {
		for(Item_Component__c ic : records){
			//Multiply Unit Cost by 1.58, and then round to next $X.X9
			//Then set as Suggested Retail
			Decimal retail = ic.Unit_Cost__c*1.58;
			Decimal other = retail.setScale(1, RoundingMode.DOWN);
			Decimal actual = other + 0.09;
			String allTogether = '$' + actual;
			ic.Suggested_Retail__c = allTogether;
		}
	}

	
	public static void checkItemCost(List<Item_Component__c> records, Map<Id,Item_Component__c> oldMap) {
		Set<Id> itemIds = new Set<Id>();
		for(Item_Component__c ic : records){
			itemIds.add(ic.Item__c);
		}

		List<Item__c> items = [SELECT Id, Display_in__c, (SELECT Unit_Cost__c FROM Display_Shipper_Components__r) FROM Item__c WHERE Id IN: itemIds];

		if(!items.isEmpty()){
			for(Item__c i : items){
			    Decimal d = 0;
			    Boolean sameCost = true;
			    if(i.Display_Shipper_Components__r.size() == 1){
			        for(Item_Component__c ic : i.Display_Shipper_Components__r){
			            i.Cost_Per_Item__c = ic.Unit_Cost__c;
			        }
			    }else{
			        for(Item_Component__c ic : i.Display_Shipper_Components__r){
			        	if(d == 0){
			            	d = ic.Unit_Cost__c;
			            }else{
			                if(d != ic.Unit_Cost__c){
			                    samecost = false;
			                }
			            }
			        }
			        if(sameCost){
			        	i.Cost_per_Item__c = d;
			    	}else{
			    		i.Cost_Per_Item__c = null;
			    	}
			    }
			}
		}
		update items;
	}
}
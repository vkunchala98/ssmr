@isTest
public class SMR_myAccountCntrlTest {
    private static testmethod void getAccountDetails() {
        account a= new account();
        a.Name='acc1';
        insert a;
        contact c=new contact();
        c.LastName='ccc1';
        c.AccountId=a.Id;
        insert c;
        
        Profile p = [SELECT Id  FROM Profile WHERE Name='Vendor Community User']; 
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                          EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US', ProfileId = p.Id, 
                          TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@testorg.com', ContactId =c.Id);
       
        insert u;
        system.runAs(u){
            
          SMR_myAccountCntrl.getAccountDetails() ;  
        }
        
    }
   private static testmethod void getAccountDetails1() {
       account a= new account();
        a.Name='acc1';
        insert a;
        contact c=new contact();
        c.LastName='ccc1';
        c.AccountId=a.Id;
        insert c;
        
        Profile p = [SELECT Id  FROM Profile WHERE Name='Vendor Community User']; 
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                          EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US', ProfileId = p.Id, 
                          TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@testorg.com', ContactId =c.Id);
       
        insert u;
        test.startTest();
       try{
          SMR_myAccountCntrl.getAccountDetails() ; 
       }
       catch(DmlException ex){
           system.debug('DmlException '+ex);
       }
        test.stopTest();
        
    }
    private static testmethod void getAccountDetails2() {
      /*  account a= new account();
        a.Name='acc1';
        insert a;*/
        contact c=new contact();
       c.LastName='con1';
        //c.AccountId=a.Id;
        insert c;
        
       /* Profile p = [SELECT Id  FROM Profile WHERE Name='Vendor Community User']; 
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                          EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US', ProfileId = p.Id, 
                          TimeZoneSidKey='America/Los_Angeles', UserName='standarduser@testorg.com', ContactId =c.Id);
       
        insert u;*/
        test.startTest();
       try{
          SMR_myAccountCntrl.getAccountDetails() ; 
       }
       catch(DmlException ex){
           system.debug('DmlException '+ex);
       }
        test.stopTest();
        
    }
            
}
public with sharing class PoLineItemTriggerHandler {
	
	public static void handleAfterInsert()
	{
		List<PO__c> poUpdateList = PoLineItemService.concatenateCommentsToPo(Trigger.new);
		if(!poUpdateList.isEmpty()){
			update poUpdateList;
		}
	}
	
	public static void handleAfterUpdate()
	{
		// update the PO order status when all PO line items have been shipped
		List<PO__c> poUpdateList = PoLineItemService.setCompletedPoOrderStatus(Trigger.new);
		if(!poUpdateList.isEmpty()){
			update poUpdateList;
		}
		
		// if PO line item comments have been changed then update the PO comments field
		List<PO_Line_Item__c> poLineItemUpdatedCommentsList = new List<PO_Line_Item__c>();
		for(PO_Line_Item__c poLineItem : (List<PO_Line_Item__c>)Trigger.new)
		{
			PO_Line_Item__c oldPoLineItem = (PO_Line_Item__c)Trigger.oldMap.get(poLineItem.Id);
			
			if(poLineItem.Customer_Comment__c != oldPoLineItem.Customer_Comment__c){
				poLineItemUpdatedCommentsList.add(poLineItem);
			}
		}
		if(!poLineItemUpdatedCommentsList.isEmpty())
		{
			poUpdateList = PoLineItemService.concatenateCommentsToPo(poLineItemUpdatedCommentsList);
			update poUpdateList;
		}
	}
    
}
public class customLookUpController {
    @AuraEnabled
    public static List < sObject > fetchLookUpValues(String searchKeyWord, String ObjectName, String fields, 
                                                     String conditions, String searchField) {
        system.debug('ObjectName-->' + ObjectName);
        String searchKey = '%' + searchKeyWord + '%';
        //String searchFilter = 'Item_Status__c = \'Active\'';
        
        List < sObject > returnList = new List < sObject > ();
      
        // Create a Dynamic SOQL Query For Fetch Record List with LIMIT 5


        String[] searchCriterias = searchField.split(';');
        String sQuery = '';

        if(searchCriterias.size() == 1){
            sQuery = 'select id, '+fields+' from ' +ObjectName + ' where '+conditions + searchCriterias[0]+' LIKE: searchKey order by createdDate DESC limit 10';
        } else {
            sQuery = 'select id, '+fields+' from ' +ObjectName + ' where '+conditions + searchCriterias[0]+' LIKE: searchKey' + ' OR ' + searchCriterias[1] +' LIKE: searchKey) order by Name ASC limit 10';
        }
         
        system.debug('JIV sQuery: ' + sQuery);
        List < sObject > lstOfRecords = Database.query(sQuery);
        
        for (sObject obj: lstOfRecords) {
            returnList.add(obj);
        }
        return returnList;
    }

    @AuraEnabled
    public static Account getAcc(){

        Account returnAccount = [SELECT Id, Name FROM Account LIMIT 1];

        return returnAccount;
    }
}
@isTest
private class TEST_ItemComponentController{

    static testMethod void myUnitTest() {
        Account acc = new Account();
        acc.Name = 'Test Account';
        acc.Type = 'Vendor';
        acc.Minimum_Order_Amount__c = '500';
        insert acc;
        Item__c item = new Item__c();
        item.Vendor__c = acc.id;
        item.Name = 'TestItem';
        item.Item__c = '90';
        item.Item_Status__c='Active';
        item.Type_of_display__c='Floor Display';
        item.Case_UPC__c='x-xxxxx-xxxxx-x';
        Id rt = Schema.Sobjecttype.Item__c.getRecordTypeInfosByName().get('Item').getRecordTypeId();
        item.RecordTypeId = rt;
        insert item;

        ApexPages.StandardController controller = new ApexPages.StandardController(item);
        ItemComponentController itemCompo = new ItemComponentController(controller);
        itemCompo.showItemComponents();
        itemCompo.itemList[0].Item__c = item.id;
        itemCompo.itemList[0].Quantity__c = 2;
        itemCompo.itemList[0].Unit_Cost__c = 3;
        itemCompo.doSave();
        itemCompo.itemList[0].Item_UPC__c = '1-11111-11111-1';
        itemCompo.doSaveAndNew();
    }
}
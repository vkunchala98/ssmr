public with sharing class UPO {
	
	public static void setCustomerVendorTerms(List<PO__c> records, Map<Id, PO__c> oldMap) {
		Set<Id> accIds = new Set<Id>();
		Map<Id, Account> accIdToAcc = new Map<Id, Account>();
		List<PO_RT__c> poRTs = PO_RT__c.getAll().values();
		String assortedRT = '';
		String directBillRT = '';
		String centralbillRT = '';
		
		for(PO__c p : records){
			accIds.add(p.Account__c);
			accIds.add(p.Bill_To__c);
		}

		List<Account> acc = [SELECT Id, Terms__c, Direct_Bill_Payment_Terms__c, Central_Bill_Payment_Terms__c FROM Account WHERE Id IN: accIds];
		for(Account a : acc){
			accIdToAcc.put(a.Id, a);
		}

		for(PO_RT__c po : poRTs){
			if(po.Name == 'Assorted'){
				assortedRT = po.Id__c;
			}else if(po.Name == 'Central Bill'){
				centralbillRT = po.Id__c;
			}else if(po.Name == 'Direct Bill'){
				directBillRT = po.Id__c;
			}
		}

		for(PO__c p : records){
			if(p.RecordTypeId == centralbillRT || p.RecordTypeId == assortedRT){
				if(accIdToAcc.get(p.Bill_To__c) != null){
					p.Customer_Terms__c = accIdToAcc.get(p.Bill_To__c).Terms__c;
				}
				if(accIdToAcc.get(p.Account__c) != null){
					p.Vendor_Terms__c = accIdToAcc.get(p.Account__c).Central_Bill_Payment_Terms__c;
				}
			}else if(p.RecordTypeId == directBillRT){
				if(accIdToAcc.get(p.Bill_To__c) != null){
					p.Customer_Terms__c = accIdToAcc.get(p.Bill_To__c).Terms__c;
				}
				if(accIdToAcc.get(p.Account__c) != null){
					p.Vendor_Terms__c = accIdToAcc.get(p.Account__c).Direct_Bill_Payment_Terms__c;
				}
			}
		}
	}
}
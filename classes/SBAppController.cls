public without sharing class SBAppController {
	
	public String shippingAccountListJSON {get; set;}
	public String billingAccountListJSON {get; set;}
	public String cartJSON {get; set;}
	
	public Boolean isShippingAddressSelected {get; set;}
	
	public SBAppController()
	{
		// get a list of shipping addresses to pass to the UI
		List<ShippingAccount> shippingAccountList = SBAppController.getShippingAccounts(UserInfo.getUserId());
		shippingAccountListJSON = JSON.serialize(shippingAccountList);
		
		// determine if the user selected shipping addresses
		List<Show_Cart_Shipping_Address__c> showCartShippingAddressList = [SELECT Id 
																		   FROM Show_Cart_Shipping_Address__c 
																		   WHERE Show_Cart__r.Owner__c =:UserInfo.getUserId() 
																		   AND Show_Cart__r.Status__c = 'Saved by Customer'];
		if(!showCartShippingAddressList.isEmpty()){
			isShippingAddressSelected = true;
		}
		else{
			isShippingAddressSelected = false;
		}
		
		// find the ID of any cart linked to this user with a status of "Saved by Customer"
		Show_Cart__c showCart = SBAppController.getCart(UserInfo.getUserId());
		cartJSON = showCart != null ? JSON.serialize(showCart) : '';
	}
	
	/**
	 * Get a list of submitted carts
	 */
	@RemoteAction
	public static List<Show_Cart__c> searchForCarts(Map<String, String> parameters)
	{
		// Add valid status to the parameters map.
		/*List<String> validStatuses = new List<String>();
		validStatuses.add('Submitted by Customer');
		validStatuses.add('Approved');
		validStatuses.add('Rejected');
		parameters.put('Status__c', JSON.serialize(validStatuses));*/

		List<String> criteriaStringList = new List<String>{'Owner__c =:currentUserId'};
		Id currentUserId = UserInfo.getUserId();
		List<String> statusValues = (parameters.containsKey('Status__c') ? (List<String>)JSON.deserializeUntyped(parameters.get('Status__c')) : null);
		
		// if statuses were provided, add the values to the query
		if(statusValues != null && !statusValues.isEmpty()){
			criteriaStringList.add('Status__c IN :statusValues');
		}
		
		System.debug(LoggingLevel.INFO, '#debug: called searchForCarts');
		// run the search
		List<Show_Cart__c> cartList = Database.query('SELECT Id, Name, CreatedDate, Status__c, Number_of_Show_Cart_Line_Items__c, Rejection_Reason__c, Total__c FROM Show_Cart__c' + (!criteriaStringList.isEmpty() ? ' WHERE ' + String.join(criteriaStringList, ' AND ') : '') + ' ORDER BY CreatedDate DESC');
		return cartList;
	}
	
	/**
	 * Perform a search on items, optionally filtered by search criteria
	 */
	@RemoteAction
	public static List<Item__c> searchForItems(Map<String, String> parameters)
	{
		List<String> itemCriteriaStringList = new List<String>{'Item_Status__c = \'Active\'', 'Display_in__c = true'};
		List<String> vendorCriteriaStringList = new List<String>();
		String boothNum = parameters.get('boothNum');
		String vendorName = parameters.get('vendorName');
		String itemNum = parameters.get('itemNum');
		String itemName = parameters.get('itemName');
		Set<Id> vendorIdSet = new Set<Id>();
		// only include items that are designated as part of the show
		itemCriteriaStringList.add('Display_in__c = true');
		
		// if a booth # was specified, add it to the query
		if(boothNum != null && boothNum != ''){
			vendorCriteriaStringList.add('Name =:boothNum');
		}
		
		// if a vendor name was specified, add it to the query
		if(vendorName != null && vendorName != '')
		{
			vendorName = '%' + vendorName + '%';
			vendorCriteriaStringList.add('Company_Name__r.Name LIKE :vendorName');
		}
		
		// if vendor criteria was provided then find the vendor IDs
		if(!vendorCriteriaStringList.isEmpty())
		{
			for(Event_Attendee__c eventAttendee : Database.query('SELECT Id, Company_Name__c FROM Event_Attendee__c WHERE ' + String.join(vendorCriteriaStringList, ' AND '))){
				vendorIdSet.add(eventAttendee.Company_Name__c);
			}
			if(!vendorIdSet.isEmpty()){
				itemCriteriaStringList.add('Vendor__c IN :vendorIdSet');
			}
			else{
				return new List<Item__c>();
			}
		}

		// if an item # was specified, add it to the query
		if(itemNum != null && itemNum != ''){
			itemCriteriaStringList.add('Item__c = :itemNum');
		}
		
		// if an item name was specified, add it to the query
		if(itemName != null && itemName != '')
		{
			itemName = '%' + itemName + '%';
			itemCriteriaStringList.add('Name LIKE :itemName');
		}
		
		// run the search
		List<Item__c> itemList = Database.query('SELECT Id, Name, Item__c, Vendor__r.Name, Vendor__r.Vendor_Code__c, Vendor__r.Minimum_Dropship_Order_Amount__c, Vendor__r.Minimum_Warehouse_Order_Amount__c, Vendor__r.Minimum_Dropship_Order_Value_case__c, Vendor__r.Minimum_Warehouse_Order_Value_case__c, Cost_Per_Deal__c, Cost_Per_Item__c, Pack_Per_Deal__c FROM Item__c' + (!itemCriteriaStringList.isEmpty() ? ' WHERE ' + String.join(itemCriteriaStringList, ' AND ') : '') + ' ORDER BY Item__c ASC');
		return itemList;
	}
	
	/**
	 * Create a cart
	 */
	@RemoteAction
	public static Show_Cart__c createCart()
	{
		Show_Cart__c showCart = SBAppController.getCart(UserInfo.getUserId());
		
		if(showCart == null)
		{
			// find the account ID associated with the current user
			User currentUser = [SELECT Id, Contact.AccountId, Contact.Account.Direct_or_Central_Bill__c FROM User WHERE Id =:UserInfo.getUserId()];
			
			// create the cart
			showCart = new Show_Cart__c(
				Owner__c = currentUser.Id,
				Bill_To__c = currentUser.Contact.AccountId,
				Bill_Type__c = currentUser.Contact.Account.Direct_or_Central_Bill__c
			);
			insert showCart;
			
			// default the available shipping addresses to the address of the current user's account and any accounts beneath it in the hierarchy
			List<Id> shippingAccountIdList = new List<Id>{currentUser.Contact.AccountId};
			
			Map<Id, Set<Id>> childAccountIdSetMap = SBAppController.getChildAccounts(new Set<Id>{currentUser.Contact.AccountId}, true);
			if(!childAccountIdSetMap.isEmpty()){
				shippingAccountIdList.addAll(childAccountIdSetMap.get(currentUser.Contact.AccountId));
			}
			
			SBAppController.setShippingAccounts(showCart.Id, shippingAccountIdList);
		}
		
		return showCart;
	}
	
	/**
	 * A recursive utility for finding the IDs of all accounts beneath the provided accounts
	 */
	public static Map<Id, Set<Id>> getChildAccounts(Set<Id> parentAccountIdSet, Boolean getAll)
	{
		Map<Id, Set<Id>> idSetMap = new Map<Id, Set<Id>>();
		
		// get all of the child accounts
		Map<Id, Id> childToParentAccountMap = new Map<Id, Id>();
		for(Account account : [SELECT Id, ParentId FROM Account WHERE ParentId IN :parentAccountIdSet])
		{
			Set<Id> childAccountIdSet = idSetMap.get(account.ParentId);
			if(childAccountIdSet == null){
				childAccountIdSet = new Set<Id>();
			}
			childAccountIdSet.add(account.Id);
			childToParentAccountMap.put(account.Id, account.ParentId);
			idSetMap.put(account.ParentId, childAccountIdSet);
		}
		
		// if children were found, call this function again to get their children and merge the results
		if(getAll && !childToParentAccountMap.isEmpty())
		{
			Map<Id, Set<Id>> grandchildIdSetMap = SBAppController.getChildAccounts(childToParentAccountMap.keySet(), true);
			for(Id childId : grandchildIdSetMap.keySet())
			{
				Id parentId = childToParentAccountMap.get(childId);
				Set<Id> childAccountIdSet = idSetMap.get(parentId);
				childAccountIdSet.addAll(grandchildIdSetMap.get(childId));
				idSetMap.put(parentId, childAccountIdSet);
			}
		}
		
		return idSetMap;
	}
	
	/**
	 * Get the active cart for the current user
	 */
	public static Show_Cart__c getCart(Id userId)
	{
		// find the account ID associated with the current user
		User currentUser = [SELECT Id, Contact.AccountId FROM User WHERE Id =:userId];
		
		List<Show_Cart__c> showCartList = [SELECT Id, Status__c, Total__c, Bill_To__c, 
												(SELECT Id, Arrival_Date__c, Discount_Dollar__c, Discount_Percent__c, Item__c, Quantity__c, Ship_To__c, Bill_To__c, Total__c, Customer_Comments__c, At_Once__c, 
													Item__r.Cost_Per_Deal__c, Item__r.Name, Item__r.Item__c, 
													Item__r.Vendor__r.Name, Item__r.Vendor__r.Minimum_Dropship_Order_Amount__c, Item__r.Vendor__r.Minimum_Warehouse_Order_Amount__c, Item__r.Vendor__r.Minimum_Dropship_Order_Value_case__c, Item__r.Vendor__r.Minimum_Warehouse_Order_Value_case__c 
												FROM Show_Cart_Line_Items__r 
												ORDER BY Item__r.Vendor__r.Name ASC) 
											FROM Show_Cart__c 
											WHERE Owner__c =:currentUser.Id 
												AND Bill_To__c =:currentUser.Contact.AccountId
												AND Status__c = 'Saved by Customer'];
		return (showCartList.isEmpty() ? null : showCartList[0]);
	}
	
	/**
	 * Add one or more items to a cart
	 */
	@RemoteAction
	public static List<Show_Cart_Line_Item__c> addItemsToCart(Id cartId, List<Map<String, String>> itemMapList)
	{
		List<Show_Cart_Line_Item__c> showCartLineItemList = new List<Show_Cart_Line_Item__c>();
		
		for(Map<String, String> itemMap : itemMapList)
		{
			List<String> datePartList = itemMap.get('Arrival_Date__c').split('-');
			Date arrivalDate = null;
			if(datePartList.size() == 3){
				arrivalDate = Date.newInstance(Integer.valueOf(datePartList[0]), Integer.valueOf(datePartList[1]), Integer.valueOf(datePartList[2]));
			}
			
			String quantityValue = itemMap.get('Quantity__c');
			String discountDollarValue = itemMap.get('Discount_Dollar__c');
			String discountPercentValue = itemMap.get('Discount_Percent__c');
			
			Show_Cart_Line_Item__c showCartLineItem = new Show_Cart_Line_Item__c(
				Id = (itemMap.get('Id') != '' ? itemMap.get('Id') : null),
				Show_Cart__c = itemMap.get('Show_Cart__c'),
				Quantity__c = (quantityValue != null ? Integer.valueOf(quantityValue) : null),
				At_Once__c = (itemMap.get('At_Once__c') == 'true' ? true : false),
    			Arrival_Date__c = (itemMap.get('At_Once__c') == 'true' ? null : arrivalDate),
    			Discount_Dollar__c = (discountDollarValue != null && discountDollarValue != '' ? Decimal.valueOf(discountDollarValue) : null),
    			Discount_Percent__c = (discountPercentValue != null && discountPercentValue != '' ? Integer.valueOf(discountPercentValue) : null),
    			Item__c = itemMap.get('Item__c'),
    			Ship_To__c = itemMap.get('Ship_To__c'),
    			Bill_To__c = itemMap.get('Bill_To__c'),
    			Customer_Comments__c = itemMap.get('Customer_Comments__c')
			);
			showCartLineItemList.add(showCartLineItem);
		}
		upsert showCartLineItemList Id;
		
		// get the new line items with some extra fields
		Set<Id> showCartLineItemIdSet = new Set<Id>();
		for(Show_Cart_Line_Item__c showCartLineItem : showCartLineItemList){
			showCartLineItemIdSet.add(showCartLineItem.Id);
		}
		showCartLineItemList = [SELECT Id, Arrival_Date__c, Discount_Dollar__c, Discount_Percent__c, Item__c, Quantity__c, Ship_To__c, Bill_To__c, Total__c, Customer_Comments__c, At_Once__c, 
									Item__r.Cost_Per_Deal__c, Item__r.Name, Item__r.Item__c, 
									Item__r.Vendor__r.Name, Item__r.Vendor__r.Minimum_Dropship_Order_Amount__c, Item__r.Vendor__r.Minimum_Warehouse_Order_Amount__c, Item__r.Vendor__r.Minimum_Dropship_Order_Value_case__c, Item__r.Vendor__r.Minimum_Warehouse_Order_Value_case__c 
								FROM Show_Cart_Line_Item__c
								WHERE Id IN :showCartLineItemIdSet];
		
		return showCartLineItemList;
	}
	
	/**
	 * Remove one or more items from a cart
	 */
	@RemoteAction
	public static Show_Cart__c removeItemsFromCart(List<Id> cartLineItemIdList)
	{
		// delete the show cart line items
		List<Show_Cart_Line_Item__c> showCartLineItemList = new List<Show_Cart_Line_Item__c>();
		
		for(Id cartLineItemId : cartLineItemIdList){
			showCartLineItemList.add(new Show_Cart_Line_Item__c(Id = cartLineItemId));
		}
		delete showCartLineItemList;
		
		return SBAppController.getCart(UserInfo.getUserId());
	}
	
	/**
	 * Submit the cart for approval
	 */
	@RemoteAction
	public static Show_Cart__c submitCart()
	{
		Show_Cart__c showCart = SBAppController.getCart(UserInfo.getUserId());
		showCart.Status__c = 'Submitted by Customer';
		update showCart;
		
		return showCart;
	}
	
	/**
	 * Set the available shipping accounts for the cart
	 */
	@RemoteAction
	public static List<ShippingAccount> setShippingAccounts(Id cartId, List<Id> accountIdList)
	{
		// find all current shipping addresses
		Map<Id, Show_Cart_Shipping_Address__c> showCartAddressMap = new Map<Id, Show_Cart_Shipping_Address__c>([SELECT Id, ID_Key__c FROM Show_Cart_Shipping_Address__c WHERE Show_Cart__c =:cartId]);
		
		// create/update the shipping addresses
		List<Show_Cart_Shipping_Address__c> showCartShippingAddressList = new List<Show_Cart_Shipping_Address__c>();
		for(Id accountId : accountIdList)
		{
			Show_Cart_Shipping_Address__c showCartShippingAddress = new Show_Cart_Shipping_Address__c(Show_Cart__c = cartId, ShipTo__c = accountId, ID_Key__c = (cartId + '-' + accountId));
			showCartShippingAddressList.add(showCartShippingAddress);
		}
		upsert showCartShippingAddressList ID_Key__c;
		
		// if any shipping addresses were deselected, remove the records
		for(Show_Cart_Shipping_Address__c showCartShippingAddress : showCartShippingAddressList){
			showCartAddressMap.remove(showCartShippingAddress.Id);
		}
		if(!showCartAddressMap.isEmpty()){
			delete showCartAddressMap.values();
		}
		
		return SBAppController.getShippingAccounts(UserInfo.getUserId());
	}
	
	/**
	 * Build a list of all shipping accounts for the specified user
	 */
	@RemoteAction
	public static List<ShippingAccount> getShippingAccounts(Id userId)
	{
		List<ShippingAccount> shippingAccountList = new List<ShippingAccount>();
		
		// find the account ID associated with the current user
		User currentUser = [SELECT Id, Contact.AccountId, Contact.Account.ParentId, Contact.Account.Name FROM User WHERE Id =:userId];
		// Hardcoded Values for exceptions.
		if(currentUser.Contact.Account.Name == 'Fresh Grocer'){
			ShippingAccount shippingAccount = new ShippingAccount();
			shippingAccount.accountId = currentUser.Contact.AccountId;
			shippingAccount.addressText = '800 W. 4th Street Wilmington, DE. 19801';
			shippingAccount.selected = true;
			shippingAccountList.add(shippingAccount);
		}else if(currentUser.Contact.Account.Name == 'Dave\'s'){
			ShippingAccount shippingAccount = new ShippingAccount();
			shippingAccount.accountId = currentUser.Contact.AccountId;
			shippingAccount.addressText = 'Placeholder';
			shippingAccount.selected = true;
			shippingAccountList.add(shippingAccount);
		}else{
			// get the current shipping address records
			Map<Id, Show_Cart_Shipping_Address__c> showCartShippingAddressMap = new Map<Id, Show_Cart_Shipping_Address__c>();
			for(Show_Cart_Shipping_Address__c showCartShippingAddress : [SELECT Id, Show_Cart__c, ShipTo__c FROM Show_Cart_Shipping_Address__c WHERE Show_Cart__r.Owner__c =:currentUser.Id AND Show_Cart__r.Status__c = 'Saved by Customer']){
				showCartShippingAddressMap.put(showCartShippingAddress.ShipTo__c, showCartShippingAddress);
			}
		
			// get the full account hierarchy and build a list of shipping addresses
			for(Account account : [SELECT Id, ShippingStreet, ShippingCity, ShippingState, ShippingPostalCode, ShippingCountry 
									FROM Account 
									WHERE Id =:currentUser.Contact.AccountId 
									OR Id =:currentUser.Contact.Account.ParentId 
									OR (ParentId != null AND ParentId =:currentUser.Contact.Account.ParentId)])
			{
				String addressText = '';
				List<String> addressPartList = new List<String>();
				if(account.ShippingStreet != null){
					addressPartList.add(account.ShippingStreet);
				}
				if(account.ShippingCity != null){
					addressPartList.add(account.ShippingCity);
				}
				if(account.ShippingState != null)
				{
					if(account.ShippingPostalCode != null){
						addressPartList.add(account.ShippingState + ' ' + account.ShippingPostalCode);
					}
					else{
						addressPartList.add(account.ShippingState);
					}
				}
				else{
					addressPartList.add(account.ShippingPostalCode);
				}
				if(account.ShippingCountry != null){
					addressPartList.add(account.ShippingCountry);
				}
				addressText = String.join(addressPartList, ', ');
			
				if(addressText != '')
				{
					ShippingAccount shippingAccount = new ShippingAccount();
					shippingAccount.accountId = account.Id;
					shippingAccount.addressText = addressText;
					shippingAccount.selected = showCartShippingAddressMap.containsKey(account.Id);
					shippingAccountList.add(shippingAccount);
				}
			}
		}
		return shippingAccountList;
	}
	
	public class ShippingAccount
	{
		Id accountId;
		String addressText;
		Boolean selected;
	}
    
}
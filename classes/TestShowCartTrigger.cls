/**
 * This class contains unit tests for validating the behavior of the ShowCartTrigger and related code
 */
@isTest
private class TestShowCartTrigger {
	
	@TestSetup
	public static void setup()
	{
		// populate custom settings
		TestUtilities.makeCustomSettings();
		
		// create customer accounts
        List<Account> customerAccountList = TestUtilities.generateCustomerAccounts(2);
        insert customerAccountList;
        
        // create vendor accounts
        List<Account> vendorAccountList = TestUtilities.generateVendorAccounts(10);
        insert vendorAccountList;
        
        // create items
        List<Item__c> itemList = TestUtilities.generateItems(vendorAccountList.size());
        for(Integer ii=0; ii < itemList.size(); ++ii){
        	itemList[ii].Vendor__c = vendorAccountList[Math.mod(ii, vendorAccountList.size())].Id;
        }
        insert itemList;
        
        // create item components
        List<Item_Component__c> itemComponentList = TestUtilities.generateItemComponents(itemList.size() * 2);
        for(Integer ii=0; ii < itemComponentList.size(); ++ii){
        	itemComponentList[ii].Item__c = itemList[Math.mod(ii, itemList.size())].Id;
        }
        insert itemComponentList;
	}

    static testMethod void convertShowCartToOrder(){
        TestShowCartTrigger.convertShowCartToOrderHelper(1);
    }

    static testMethod void convertShowCartToOrderBulk(){
        TestShowCartTrigger.convertShowCartToOrderHelper(200);
    }

    static void convertShowCartToOrderHelper(Integer quantity)
    {
    	// get customer accounts
    	List<Account> customerAccountList = [SELECT Id FROM Account WHERE RecordTypeId =:TestUtilities.getAccountRecordTypes().get('Customer').getRecordTypeId()];
    	
    	// get items
    	List<Item__c> itemList = [SELECT Id FROM Item__c];
    	
        // create show carts
        List<Show_Cart__c> showCartList = TestUtilities.generateShowCarts(quantity);
        for(Integer ii=0; ii < showCartList.size(); ++ii)
        {
        	showCartList[ii].Bill_To__c = customerAccountList[Math.mod(ii, customerAccountList.size())].Id;
        	showCartList[ii].Bill_Type__c = 'Direct Warehouse';
        }
        insert showCartList;
        for(Show_Cart__c showCart : showCartList){
        	showCart.Status__c = 'Approved';
        }
        
        // create show cart line items
        List<Show_Cart_Line_Item__c> showCartLineItemList = TestUtilities.generateShowCartLineItems(quantity * 2);
        for(Integer ii=0; ii < showCartLineItemList.size(); ++ii)
        {
        	showCartLineItemList[ii].Show_Cart__c = showCartList[Math.mod(ii, showCartList.size())].Id;
        	showCartLineItemList[ii].Ship_To__c = customerAccountList[Math.mod(ii, customerAccountList.size())].Id;
        	showCartLineItemList[ii].Item__c = itemList[Math.mod(ii, itemList.size())].Id;
        }
        insert showCartLineItemList;
        
        Test.startTest();
        
        update showCartList;
        
        Test.stopTest();
        
        // verify that orders were created
        List<Store_Order__c> storeOrderList = [SELECT Id FROM Store_Order__c];
        System.assertEquals(showCartList.size(), storeOrderList.size());
        
        // verify that order line items were created
        List<Order_Line_Items__c> storeOrderLineItemList = [SELECT Id FROM Order_Line_Items__c];
        System.assertEquals(showCartLineItemList.size(), storeOrderLineItemList.size());
        
        // verify that order line item components were created
        List<Order_Line_Item_Component__c> storeOrderLineItemComponentList = [SELECT Id FROM Order_Line_Item_Component__c];
        System.assertEquals(showCartLineItemList.size() * 2, storeOrderLineItemComponentList.size());
        
        // verify that order line item ship to records were created
        List<OrderLineItem_ShipTo__c> orderLineItemShipToList = [SELECT Id FROM OrderLineItem_ShipTo__c];
        System.assertEquals(showCartLineItemList.size(), orderLineItemShipToList.size());
        
        // verify that POs were created
        
        // verify that PO line items were created
        List<PO_Line_Item__c> poLineItemList = [SELECT Id FROM PO_Line_Item__c];
        System.assertEquals(storeOrderLineItemList.size(), poLineItemList.size());
        
        // verify that PO line item components were created
        List<PO_Line_Item_Component__c> poLineItemComponentList = [SELECT Id FROM PO_Line_Item_Component__c];
        System.assertEquals(storeOrderLineItemComponentList.size(), poLineItemComponentList.size());
        
    }
    
    
    static testMethod void setSalesRep(){
        TestShowCartTrigger.setSalesRepHelper(1);
    }

    static testMethod void setSalesRepBulk(){
        TestShowCartTrigger.setSalesRepHelper(200);
    }

    static void setSalesRepHelper(Integer quantity)
    {
    	// get customer accounts
    	List<Account> customerAccountList = [SELECT Id, OwnerId FROM Account WHERE RecordTypeId =:TestUtilities.getAccountRecordTypes().get('Customer').getRecordTypeId()];
    	
    	// get items
    	List<Item__c> itemList = [SELECT Id FROM Item__c];
    	
        // create show carts
        List<Show_Cart__c> showCartList = TestUtilities.generateShowCarts(quantity);
        for(Integer ii=0; ii < showCartList.size(); ++ii)
        {
        	showCartList[ii].Bill_To__c = customerAccountList[0].Id;
        	showCartList[ii].Bill_Type__c = 'Direct Warehouse';
        }
        
        
        Test.startTest();
        
        insert showCartList;
        
        Test.stopTest();
        
        
        // verify that the sales rep field was populated
        for(Show_Cart__c showCart : [SELECT Id, Bill_To__c, Sales_Rep__c FROM Show_Cart__c]){
        	System.assertEquals(customerAccountList[0].OwnerId, showCart.Sales_Rep__c);
        }
   
   /*     Update is not being checked because reparenting is not currently allowed
        // change the bill to account 
        for(Show_Cart__c showCart : showCartList){
        	showCart.Bill_To__c = customerAccountList[1].Id;
        }
        update showCartList;
        
        // verify that the sales rep field was updated
        for(Show_Cart__c showCart : [SELECT Id, Bill_To__c, Sales_Rep__c FROM Show_Cart__c]){
        	System.assertEquals(customerAccountList[1].OwnerId, showCart.Sales_Rep__c);
        }
    */    
    }
}
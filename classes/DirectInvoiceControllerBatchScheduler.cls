public Class DirectInvoiceControllerBatchScheduler implements Schedulable{

    public void execute(SchedulableContext schCon){
        string sch = '0 0 0 L * ?';
        DirectInvoiceControllerBatch dicb = new DirectInvoiceControllerBatch();
        //system.schedule('Vendor Invoice Per Month',sch,Database.executeBatch(vicb));
        Database.executeBatch(dicb);
    }
}
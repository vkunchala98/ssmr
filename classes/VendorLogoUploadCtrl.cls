public class VendorLogoUploadCtrl {
    
    public Id contactIds;
    public account acc{get;set;}
    public blob imageBody{get;set;}
    public List<Account> Accountlistinfo{get;set;}
    public Attachment attch{get;set;}
    
    public VendorLogoUploadCtrl(){
    attch=new Attachment();
        List<User> lstUser = [Select u.ContactId from User u where u.Id = : UserInfo.getUserId()];
        if(lstUser[0].contactId!=null){
            Id contactIds = lstUser[0].contactID;
            contact lstContactInfo = [select Id,AccountId from Contact where Id = : contactIds LIMIT 1];
        acc= [select Id,Name,Vender_Logo__c from Account where Id=:lstContactInfo.AccountId limit 1]; 
        }    
    }
    
    public pageReference UploadImage(){
    Attachment d= new Attachment();
 d.Name = attch.Name;
 d.body=attch.body; 
 d.ParentId=acc.Id; 
 insert d;
       acc.Vender_Logo__c  = '<img src="/servlet/servlet.FileDownload?file='+d.id+'" width="500" height="281"></img>'; //FirstExample is namespace & Comments__c is Rich Text Area field
       update acc;
        return null ;
    }
}
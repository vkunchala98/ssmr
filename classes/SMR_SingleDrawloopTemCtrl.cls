public class SMR_SingleDrawloopTemCtrl{
    private ApexPages.StandardSetController standardController;
    public String erroMsg {set;get;}
       public SMR_SingleDrawloopTemCtrl(ApexPages.StandardSetController standardController){
          this.standardController = standardController;
       }
       public PageReference doSubmit()
      { 
        List<Item__c> selectedItems= (List<Item__c>)standardController.getSelected();
        if(selectedItems.Size()>0){
          String DrawloopTempId=System.Label.SMR_SingleTemplate;
          String deliveryOp=System.Label.SMR_SingleTemplate_Delivery;
          String sfdcBaseURL = URL.getSalesforceBaseUrl().toExternalForm();
          System.debug('Base URL: ' + sfdcBaseURL );
          String RecIds='';
           for (Item__c ItemId : selectedItems) {
                  RecIds+=ItemId.id+',';
            }
          RecIds = RecIds.removeEnd(',');
          system.debug('>>>>'+RecIds);
            PageReference redirectPage = Page.loop__masslooplus;
            redirectPage.setRedirect(true);
            redirectPage.getParameters().put('retURL', sfdcBaseURL);
            redirectPage.getParameters().put('recordIds',RecIds);
            redirectPage.getParameters().put('sessionId',userInfo.getSessionId() );
            //redirectPage.getParameters().put('contactField','Customer_Email_Contact_ID__c');
            redirectPage.getParameters().put('hidecontact','true');
            redirectPage.getParameters().put('hideddp','true');
            redirectPage.getParameters().put('autorun','true');
            redirectPage.getParameters().put('attach','true');
            redirectPage.getParameters().put('ddpIds',DrawloopTempId);
            redirectPage.getParameters().put('deploy',deliveryOp);
            //contactField=Customer_Email_Contact_ID__c
            return redirectPage;

        }
        else{
            erroMsg ='error';
        }
        
         return null;
      }
}
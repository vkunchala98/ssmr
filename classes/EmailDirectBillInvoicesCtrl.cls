/** Class Name : EmailDirectBillInvoicesCtrl
*  Description  : Apex class for EmailDirectBillInvoices Lightning Button. 
*  Created By   : BTG 
*  Created On   : 21st Feb 2019
*
*  Modification Log:  
*  --------------------------------------------------------------------------------------------------------------------------------------
*   Developer                Date                   Modification ID      Description 
*  ---------------------------------------------------------------------------------------------------------------------------------------
*                                                                                    
**/


public class EmailDirectBillInvoicesCtrl{

     private ApexPages.StandardSetController standardController;
    public String erroMsg {set;get;}
    
    public EmailDirectBillInvoicesCtrl(ApexPages.StandardSetController standardController) {
        this.standardController = standardController;
        
    }
    
     public PageReference doSubmit()
          {       
        List<Invoice__c> selectedInvoice= (List<Invoice__c>) standardController.getSelected();
        system.debug('#####'+selectedInvoice);
        if(selectedInvoice.size()>0){
            //callout
            String DirectbillInvoicedrId=System.label.Direct_Customer_Invoice;
            String deliveryOp=System.Label.Direct_Customer_Invoice_Delivery;
            
             String RecIds='';
            for (Invoice__c sObjectId : selectedInvoice) {
            RecIds+=sObjectId.id+',';
            }
            RecIds = RecIds.removeEnd(',');
            system.debug('>>>>'+RecIds);
            
            String sfdcBaseURL = URL.getSalesforceBaseUrl().toExternalForm();
    System.debug('Base URL: ' + sfdcBaseURL );
    
   
    
            PageReference redirectPage = Page.loop__masslooplus;
            redirectPage.setRedirect(true);
            redirectPage.getParameters().put('retURL', sfdcBaseURL);
            redirectPage.getParameters().put('recordIds',RecIds);
            redirectPage.getParameters().put('sessionId',userInfo.getSessionId() );
            redirectPage.getParameters().put('contactField','Email_Contact_Id__c');
            redirectPage.getParameters().put('hidecontact','true');
            redirectPage.getParameters().put('hideddp','true');
            redirectPage.getParameters().put('autorun','true');
            redirectPage.getParameters().put('attach','true');
            redirectPage.getParameters().put('ddpIds',DirectbillInvoicedrId);
            redirectPage.getParameters().put('deploy',deliveryOp);
            
            //contactField=Customer_Email_Contact_ID__c
            return redirectPage;
           
            
        }else{
            erroMsg ='error';
        }
        return null;
    }
          
          
    
}
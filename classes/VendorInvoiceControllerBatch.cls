global Class VendorInvoiceControllerBatch implements Database.Batchable<Sobject>,Database.StateFul{

 
    List<PO_Line_Item__c> poLineItemList = new List<PO_Line_Item__c>();
    
    global String query;
    
    global VendorInvoiceControllerBatch() {
        query = 'SELECT Id, Name, Total__c, Store_Product__c,PO__c,PO__r.Ship_To__c,Store_Product__r.Vendor__c,Store_Product__r.Vendor__r.Terms__c,PO__r.Bill_To__c,Cost_per_Deal__c,Discount__c,Item_Number__c,Quantity__c,Type_of_Display__c,Vendor_Commission_Percentage_Owed__c,SMR_Vendor_Invoice__c,Registered_On__c,Original_Deal_Cost__c'; 
        query +=' FROM PO_Line_Item__c' ;
        query +=' WHERE Registered__c = true AND SMR_Vendor_Invoice__c = NULL AND (PO__r.RecordType__c=\'Direct_Bill\' OR PO__r.RecordType__c=\'Assorted\' )';

        system.debug('::query =>'+query );
    }
                      
    global Database.QueryLocator start(Database.BatchableContext BC){
        
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC,List<PO_Line_Item__c> poLineItemLists){
     
        if(poLineItemLists != NULL)   {
            poLineItemList.addAll(poLineItemLists);
        }
        
    }
    
    global void finish(Database.BatchableContext BC) {
        Map<Id,Invoice__c> vendorIdAndInvoiceMap = new Map<Id,Invoice__c>();
        Map<Id,List<PO_Line_Item__c>> vendorIdAndPoLineItemListMap = new Map<Id,List<PO_Line_Item__c>>();
        List<RecordType> invoiceRecTypeList = [SELECT Id FROM RecordType WHERE DeveloperName = 'Vendor_Invoice' AND  SobjectType = 'Invoice__c' limit 1];
        List<RecordType> invoiceLineItemRecTypeList = [SELECT Id FROM RecordType WHERE DeveloperName = 'Vendor' AND  SobjectType = 'Invoice_Line_Item__c' limit 1];
        string invoiceRecType = '';
        string invoiceLineItemRecType = '';
        if(invoiceLineItemRecTypeList.size() > 0 )invoiceLineItemRecType = invoiceLineItemRecTypeList[0].id;
        if(invoiceRecTypeList.size() > 0 )invoiceRecType = invoiceRecTypeList[0].id;
        List<Invoice_Line_Item__c> invoiceLIList = new List<Invoice_Line_Item__c>();  
        List<PO_Line_Item__c> updatePoLineItemList = new List<PO_Line_Item__c>();
        
        system.debug('::poLineItemList=>'+poLineItemList );
        if(poLineItemList != Null) {
            for(PO_Line_Item__c poItem : poLineItemList) {
                if(poItem.Store_Product__r.Vendor__c != null){
                    if(!vendorIdAndInvoiceMap.containsKey(poItem.Store_Product__r.Vendor__c)){
                        vendorIdAndInvoiceMap.put(poItem.Store_Product__r.Vendor__c, new Invoice__c());
                        vendorIdAndInvoiceMap.get(poItem.Store_Product__r.Vendor__c).Account__c = poItem.Store_Product__r.Vendor__c;
                        vendorIdAndInvoiceMap.get(poItem.Store_Product__r.Vendor__c).Terms__c = poItem.Store_Product__r.Vendor__r.Terms__c;
                        vendorIdAndInvoiceMap.get(poItem.Store_Product__r.Vendor__c).Invoice_Date__c = System.today();
                        vendorIdAndInvoiceMap.get(poItem.Store_Product__r.Vendor__c).RecordTypeId = invoiceRecType;
                        vendorIdAndInvoiceMap.get(poItem.Store_Product__r.Vendor__c).Invoice_Type__c = 'Vendor Commission';
                        vendorIdAndInvoiceMap.get(poItem.Store_Product__r.Vendor__c).Status__c = 'Not Yet Invoiced';
                        vendorIdAndInvoiceMap.get(poItem.Store_Product__r.Vendor__c).Amount__c = 0;
                    }
                    vendorIdAndInvoiceMap.get(poItem.Store_Product__r.Vendor__c).Amount__c += poItem.Vendor_Commission_Percentage_Owed__c;
                    if(!vendorIdAndPoLineItemListMap.containsKey(poItem.Store_Product__r.Vendor__c)){
                        vendorIdAndPoLineItemListMap.put(poItem.Store_Product__r.Vendor__c, new List<PO_Line_Item__c>());
                    }
                    vendorIdAndPoLineItemListMap.get(poItem.Store_Product__r.Vendor__c).add(poItem);
                }
            }
            system.debug('::vendorIdAndInvoiceMap=>'+vendorIdAndInvoiceMap);
            system.debug('::vendorIdAndPoLineItemListMap=>'+vendorIdAndPoLineItemListMap);
            
            if(vendorIdAndInvoiceMap.values().size() > 0) {
                insert vendorIdAndInvoiceMap.values();
            }
             system.debug('::invoiceeeeeeeeee=>'+vendorIdAndInvoiceMap.values());
             
            for(Id vendorId : vendorIdAndInvoiceMap.keySet()) {
                
                if(vendorIdAndPoLineItemListMap.get(vendorId) != Null) {
                    List<PO_Line_Item__c> tempPoLineItemList = vendorIdAndPoLineItemListMap.get(vendorId);
                    
                    for(PO_Line_Item__c oli : vendorIdAndPoLineItemListMap.get(vendorId)) {
                        
                        if(oli.Store_Product__r.Vendor__c == vendorIdAndInvoiceMap.get(vendorId).Account__c){
                            invoiceLIList.add(new Invoice_Line_Item__c(
                                Invoice__c = vendorIdAndInvoiceMap.get(oli.Store_Product__r.Vendor__c).Id,
                                Store_Product__c = oli.Store_Product__c,Cost_per_Deal__c=oli.Cost_per_Deal__c,
                                Bill_To__c=oli.PO__r.Bill_To__c,Item_Number__c=oli.Item_Number__c,Quantity__c=oli.Quantity__c,
                                Type_of_Display__c=oli.Type_of_Display__c,
                                PO_Line_Item_Registered_On__c=oli.Registered_On__c,PO_Line_Item__c=oli.Id,
                                Commission_Due__c = oli.Vendor_Commission_Percentage_Owed__c,
                                Ship_To__c = oli.PO__r.Ship_To__c,RecordTypeId=invoiceLineItemRecType
                                //,Record_Type__c = invoiceLineItemRecType,
                            ));
                            oli.SMR_Vendor_Invoice__c = vendorIdAndInvoiceMap.get(oli.Store_Product__r.Vendor__c).Id;
                            updatePoLineItemList.add(oli);
                        }
                    
                    }
                }
            }
            
        }
        if(invoiceLIList.size() > 0) {
            insert invoiceLIList;
            update updatePoLineItemList;
        }
    }
}
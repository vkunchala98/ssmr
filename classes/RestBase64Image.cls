@RestResource(urlMapping='/image/*')
global class RestBase64Image {
    @HttpGet
    global static Response getBase64Image() {
        
        RestRequest restReq = RestContext.request;
        String itemId = restReq.params.get('id');
        String base64Image = '';
        String itemAttachmentName = 'Product_Image_'+itemId;

        List<Attachment> attList = [Select Id, Body From Attachment Where ParentId = :itemId AND Name= :itemAttachmentName ];
        
        if(attList!= NULL && attList.size() > 0) {
            base64Image = EncodingUtil.base64Encode(attList[0].Body);
        }
        Response resp = new Response();
        resp.base64Text = base64Image;
        
        return resp;
    }
    
    global class Response {
        global String base64Text;
    }
}
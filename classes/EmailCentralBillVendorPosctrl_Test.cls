@isTest(SeeAllData=true)
public class EmailCentralBillVendorPosctrl_Test{

private static testmethod void positivescenario(){
    Id VendorRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Vendor').getRecordTypeId();
    String DirectRecordTypeId = Schema.SObjectType.PO__c.getRecordTypeInfosByName().get('Central Bill').getRecordTypeId();
    //Id cenrecordtypeid=[SELECT Id, DeveloperName FROM RecordType where DeveloperName='Central_Bill' limit 1].id;
  Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
      User u2 = new User(Alias = 'newUser', Email='newuser44444@testorg.com',
         EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
         LocaleSidKey='en_US', ProfileId = p.Id,
         TimeZoneSidKey='America/Los_Angeles', UserName='newuser1234444@testorg.com');
         system.runas(u2){
         String label=System.label.Central_Customer_PO;
     Loop__DDP__c ddp = new Loop__DDP__c(Name='My DDP');
        insert ddp;
        
        Loop__DDP_Integration_Option__c delivOpt = new Loop__DDP_Integration_Option__c(Name='Email', Loop__DDP__c=ddp.Id);
        insert delivOpt;
        
       
       Account acc= new Account();
       acc.Name='Test vendor';
       acc.RecordTypeid=VendorRecordTypeId;
       //acc.Vendor_Email_Contact__c=con.id;
       insert acc;
       contact con= new Contact();
       con.LastName='test Name';
       con.Email='test@gmail.com';
       con.phone='1234';
       con.AccountId=acc.id;
       insert con;
       acc.Vendor_Email_Contact__c=con.id;
       update acc;
       Account acc1= new Account();
       acc1.Name='Test billing';
       acc1.BillingStreet='test street';
       acc1.BillingCity='test city';
       acc1.BillingState='CA';
       acc1.BillingPostalcode='521235';
       acc1.BillingCountry='UAS';
       acc1.ShippingStreet='test street';
       acc1.ShippingCity='test city';
       acc1.ShippingState='CA';
       acc1.ShippingPostalcode='521235';
       acc1.ShippingCountry='UAS'; 
       acc1.Terms__c='Net 21'; 
       acc1.Email_Contact__c=con.id;     
       insert acc1;
       List<PO_RT__c> poRTs = PO_RT__c.getAll().values();
       id Centralbilling;
       for(PO_RT__c por:poRTs){
        if(por.Name=='central Bill'){
       Centralbilling=por.Id__c;
        }
       }
       List<Po__c> poList= new List<Po__c>();
       PO__c po = new PO__c();
       //po.Name='Test po';
       po.Account__c=acc.id;
       po.Bill_To__c=acc1.id;
       po.Ship_To__c=acc1.id;
       po.RecordTypeId =DirectRecordTypeId ;
       po.Order_Status__c='open';
       po.Customer_Terms__c='Net 21';
       poList.add(po);
       PO__c po2 = new PO__c();
       //po.Name='Test po';
       po2.Account__c=acc.id;
       po2.Bill_To__c=acc1.id;
       po2.Ship_To__c=acc1.id;
       po2.RecordTypeId=DirectRecordTypeId ;
       po.Order_Status__c='open';
       poList.add(po2);
      // insert poList;
       
       Test.startTest();
          Test.setCurrentPage(Page.EmailCentralBillVendorPOs);
          ApexPages.StandardSetController stdSetController = new ApexPages.StandardSetController(poList);
          stdSetController.setSelected(poList);
          EmailCentralBillVendorPosctrl ext = new EmailCentralBillVendorPosctrl(stdSetController);
          ext.doSubmit();
      Test.stopTest();


   }
   }

}
@isTest
global class TestRestBase64Image {

    static testmethod void method1() {
    
        Account acct = new Account();
        acct.Name = 'Test Account';
        insert acct;
    
        Attachment testAtt = new Attachment();
        testAtt.Name = 'Test Attachment';
        testAtt.Body = Blob.valueOf('test');
        testAtt.ParentId = acct.Id;
        insert testAtt;
        
        RestResponse res = new RestResponse();
        RestRequest req = new RestRequest();
        
        req.httpMethod = 'GET';
        req.addParameter('id',testAtt.id);
        RestContext.response = res;
        RestContext.request = req;
        
        RestBase64Image.Response resp = RestBase64Image.getBase64Image();         
    }
}
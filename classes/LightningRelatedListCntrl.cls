public class LightningRelatedListCntrl {
    @auraEnabled
    public Static LightningRelatedWrap userEvent(){
        
        LightningRelatedWrap myInitial = new LightningRelatedWrap();
        List<ContentDocument> DocumentList = new List<ContentDocument>();  
        String EventRecordid='';
        Event__c myEvent;
        Set<Id> documentIds = new Set<Id>();  //store file ids
        List<User> lstUser = [Select u.ContactId from User u where u.Id = : UserInfo.getUserId()];
        if(lstUser[0].contactID!=null){
            Id contactb = lstUser[0].contactID;
            list<contact> lstContactinfo = [select Id,AccountId from Contact where Id = :contactb LIMIT 1];
            List<Account> Accountlistinfo=[select id,Name,(select id,Name from Event__r) from Account where Id=:lstContactinfo[0].AccountId];
            if(Accountlistinfo[0].Event__r!=null){
                EventRecordid= Accountlistinfo[0].Event__r[0].id;
                system.debug('RTRRR'+EventRecordid);
                myEvent = [select id,(select id,Name,All_Products_Included__c,Customer_or_Partner_Incentive__c,Incentive_Amount__c from Incentives__r),(select id,Contact_Name__c,Contact_Name__r.Name,Contact_Email__c,Need_Hotel__c,Room_Type_Requested__c from Event_Attendees__r) from  Event__c where id=:EventRecordid]; 
                List<ContentDocumentLink> cdl=[select id,LinkedEntityId,ContentDocumentId from ContentDocumentLink where LinkedEntityId=:EventRecordid];  
                for(ContentDocumentLink cdLink:cdl){  
                    documentIds.add(cdLink.ContentDocumentId);  // Document ids
                }      
                DocumentList = [select Id,Title,FileType,ContentSize,Description from ContentDocument where id IN: documentIds];  
                
            }
            myInitial.EventId=EventRecordid;
            if(myEvent.Incentives__r.size()>0)
                myInitial.allInce=myEvent.Incentives__r;
            if(myEvent.Event_Attendees__r.size()>0)
                myInitial.allAttend=myEvent.Event_Attendees__r;
            if(DocumentList.size()>0)
                myInitial.allFiles=DocumentList;
            
            return myInitial;
            
        }
        return null;
    }
}
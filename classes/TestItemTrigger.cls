@isTest
public with sharing class TestItemTrigger {
	static testMethod void test(){
		Account a = TestUtilities.getVendorAccount();
		insert a;

		Item__c i1 = TestUtilities.getItem(a.Id);
		insert i1;
		i1.Item__c = 'test1';
		update i1;

		Item__c i2 = TestUtilities.getItem(a.Id);
		i2.Item__c = 'test2';
		insert i2;
		i2.Item__c = 'test4';
                i2.Available_in_2019_Show__c=true;

		update i2;
	}
}
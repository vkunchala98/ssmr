public Class VendorInvoiceControllerBatchScheduler implements Schedulable{

    public void execute(SchedulableContext schCon){
        string sch = '0 0 0 L * ?';
        VendorInvoiceControllerBatch vicb = new VendorInvoiceControllerBatch();
        //system.schedule('Vendor Invoice Per Month',sch,Database.executeBatch(vicb));
        Database.executeBatch(vicb);
    }
}
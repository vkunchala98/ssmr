global with sharing class ShowCartDTO {

	@AuraEnabled
	global 	String 	Id 						{get; set;}

	@AuraEnabled
	global 	String 	Name 					{get; set;}

	@AuraEnabled
	global Integer 	VendorsAdded			{get; set;}

	@AuraEnabled
	global Decimal 	TotalAmount 			{get; set;}

	@AuraEnabled
	global CustomerDTO 	CustomerInfo 		{get; set;}

	@AuraEnabled
	global List<VendorDTO> 	VendorsList{
		get{
            if (VendorsList == null){
                VendorsList = new List<VendorDTO>();
            }

            return VendorsList;
        }
        set;
	}

	public static ShowCartDTO buildCart(X2019_Show_Cart__c showCartSO){
		ShowCartDTO showCartDTO = new ShowCartDTO();

		showCartDTO.Id				= showCartSO.Id;
		showCartDTO.Name			= showCartSO.Name;
		showCartDTO.TotalAmount 	= showCartSO.Total_Amount__c;

		return showCartDTO;
	}
}
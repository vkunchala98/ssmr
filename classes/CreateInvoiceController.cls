global class CreateInvoiceController{

    public CreateInvoiceController(){
     
    }
    
    webservice static String CreateInvoice(String soId){
        try{
            Map<String, Invoice__c> billToInvoiceMap = new Map<String, Invoice__c>();
            List<Invoice_Line_Item__c> invoiceLIList = new List<Invoice_Line_Item__c>();
            
            List<Order_Line_Items__c> oliList = [SELECT Id, Name, Total__c, Store_Product__c, Store_Product__r.Vendor__r.Id,Store_Order__c,Bill_To__c,
                                                        Cost_per_Deal__c,Discount__c,Item__c,Item_Number__c,Quantity__c,Ship_To__c,
                                                        Type_of_Display__c
                                                 FROM Order_Line_Items__c 
                                                 WHERE Store_Order__c = :soId ];
            system.debug('-------oliList -------'+oliList );                                        
            for(Order_Line_Items__c oli : oliList){
                if(billToInvoiceMap.containsKey(oli.Bill_To__c)){
                    if(billToInvoiceMap.get(oli.Bill_To__c) != NULL)
                        billToInvoiceMap.put(oli.Bill_To__c, new Invoice__c(Account__c = oli.Bill_To__c, Amount__c = oli.Total__c+billToInvoiceMap.get(oli.Bill_To__c).Amount__c));
                } else {
                    billToInvoiceMap.put(oli.Bill_To__c, new Invoice__c(Account__c = oli.Bill_To__c, Amount__c = oli.Total__c));
                }
            }
            system.debug('-------billToInvoiceMap-------'+billToInvoiceMap);   
            if(billToInvoiceMap.size() > 0 ) Insert billToInvoiceMap.values();
            if(billToInvoiceMap.size() > 0){
                for(Order_Line_Items__c oli : oliList){
                    invoiceLIList.add(new Invoice_Line_Item__c(Invoice__c = billToInvoiceMap.get(oli.Bill_To__c).Id,Store_Product__c = oli.Store_Product__c,Cost_per_Deal__c=oli.Cost_per_Deal__c,
                                                     Discount__c=oli.Discount__c,Bill_To__c=oli.Bill_To__c,Item_Number__c=oli.Item_Number__c,Quantity__c=oli.Quantity__c,
                                                     Ship_To__c=oli.Ship_To__c,Type_of_Display__c=oli.Type_of_Display__c
                                                     ));
                }
                if(invoiceLIList.size() > 0) Insert invoiceLIList;
            }         
            return 'success';
        } catch(Exception e){
            return e.getMessage();
        }
        
        return 'success';
    }

}
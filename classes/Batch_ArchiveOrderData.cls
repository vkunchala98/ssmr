/**
 * This class identifies order data that is ready to be archived and moves it to an external storage service
 *
 * Archival criteria: For Purchase Order Line Items where "Registered On" is greater than 24 months ago, archive any associated PO, PO Line Item Components & Customer Order, Order Line Items, and Order Line Item Components
 */
global class Batch_ArchiveOrderData implements Database.Batchable<Sobject>, Database.AllowsCallouts, Database.Stateful{
	
	global String query;
	global Datetime cutOffDate;
	global Map<String, Set<String>> objectFieldSetMap;
	global I_DataArchiveService dataArchiveService;
	
	global Batch_ArchiveOrderData()
	{
    	// check a custom metadata record and dynamically instantiate the data archival class identified there
    	Data_Archival_Settings__mdt dataArchivalSettings = [SELECT Archival_Class__c, Minimum_Age__c FROM Data_Archival_Settings__mdt WHERE DeveloperName = 'Order_Data' LIMIT 1];
    	Type dataArchiveClassType = Type.forName(dataArchivalSettings.Archival_Class__c);
    	dataArchiveService = (I_DataArchiveService)dataArchiveClassType.newInstance();
		
		// get a list of fields on the objects that will be archived
		objectFieldSetMap = new Map<String, Set<String>>();
		Set<String> storeOrderFieldSet = TestUtilities.getFieldNameList('Store_Order__c');
		storeOrderFieldSet.add('Bill_To__r.Name');
		objectFieldSetMap.put('Store_Order__c', storeOrderFieldSet);
		Set<String> orderLineItemFieldSet = TestUtilities.getFieldNameList('Order_Line_Items__c');
		orderLineItemFieldSet.add('Store_Order__r.Bill_To__r.Name');
		objectFieldSetMap.put('Order_Line_Items__c', orderLineItemFieldSet);
		Set<String> orderLineItemComponentFieldSet = TestUtilities.getFieldNameList('Order_Line_Item_Component__c');
		orderLineItemComponentFieldSet.add('Order_Line_Item__r.Store_Order__r.Bill_To__r.Name');
		objectFieldSetMap.put('Order_Line_Item_Component__c', orderLineItemComponentFieldSet);
		Set<String> poFieldSet = TestUtilities.getFieldNameList('PO__c');
		poFieldSet.add('Ship_To__r.Name');
		objectFieldSetMap.put('PO__c', poFieldSet);
		Set<String> poLineItemFieldSet = TestUtilities.getFieldNameList('PO_Line_Item__c');
		poLineItemFieldSet.add('PO__r.Ship_To__r.Name');
		objectFieldSetMap.put('PO_Line_Item__c', poLineItemFieldSet);
		Set<String> poLineItemComponentFieldSet = TestUtilities.getFieldNameList('PO_Line_Item_Component__c');
		poLineItemComponentFieldSet.add('PO_Line_Item__r.PO__r.Ship_To__r.Name');
		objectFieldSetMap.put('PO_Line_Item_Component__c', poLineItemComponentFieldSet);
		
		// set the cutoff date for the query and use that in the filepath of the archive files
		cutOffDate = Datetime.now().addDays((Integer.valueOf(dataArchivalSettings.Minimum_Age__c * -1)));
    	dataArchiveService.setArchivePath('Order Archive (up to ' + cutOffDate.format('yyyy-MM-dd HH:mm:ss') + ')');
		system.debug('cutOffDate: ' + cutOffDate.format());
		
		query = 'SELECT Id, PO__c FROM PO_Line_Item__c WHERE (Registered__c = true OR Did_Not_Ship__c = true) AND Registered_On__c <= :cutOffDate AND IsDeleted = false AND PO__r.Order_Status__c IN (\'Completed\', \'Cancelled\') ORDER BY PO__r.Ship_To__r.Name ASC';
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC){
		system.debug('query: ' + query);
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<PO_Line_Item__c> batchPoLineItemList)
    {
    	/**
    	 * Get all of the data to be archived for this batch
    	 */
    	// find all of the PO__c record IDs
    	Set<Id> poIdSet = new Set<Id>();
    	for(PO_Line_Item__c poLineItem : batchPoLineItemList){
    		poIdSet.add(poLineItem.PO__c);
    	}
    	
    	// get all of the associated PO_Line_Item__c records and make sure we remove any PO IDs that have unregistered line items that were not marked as not shipped
    	List<PO_Line_Item__c> poLineItemFullList = Database.query('SELECT PO__r.Order_Status__c, ' + String.join(new List<String>(objectFieldSetMap.get('PO_Line_Item__c')), ', ') + ' FROM PO_Line_Item__c WHERE PO__c IN :poIdSet AND IsDeleted = false ORDER BY PO__r.Ship_To__r.Name ASC');
    	List<PO_Line_Item__c> poLineItemList = new List<PO_Line_Item__c>();
    	
    	// group the line items by PO
    	Map<Id, List<PO_Line_Item__c>> poLineItemByPoIdMap = new Map<Id, List<PO_Line_Item__c>>();
    	for(PO_Line_Item__c poLineItem : poLineItemFullList)
    	{
    		List<PO_Line_Item__c> tempPoLineItemList = poLineItemByPoIdMap.get(poLineItem.PO__c);
    		if(tempPoLineItemList == null){
    			tempPoLineItemList = new List<PO_Line_Item__c>();
    		}
    		tempPoLineItemList.add(poLineItem);
    		poLineItemByPoIdMap.put(poLineItem.PO__c, tempPoLineItemList);
    	}
    	
    	// remove any POs that should not get archived
    	for(PO_Line_Item__c poLineItem : poLineItemFullList)
    	{
    		// if the PO hasn't been fully registered or marked as not shipped then remove it from the archive list, along with it's parent store order
    		if(poLineItem.PO__r.Order_Status__c == 'Open' || (poLineItem.Registered__c == false && poLineItem.Did_Not_Ship__c == false)){
    			poLineItemByPoIdMap.remove(poLineItem.PO__c);
    		}
    	}
    	poIdSet = poLineItemByPoIdMap.keySet();
    	
    	// put together a new list of PO line items that need to be archived
    	for(List<PO_Line_Item__c> tempPoLineItemList : poLineItemByPoIdMap.values()){
    		poLineItemList.addAll(tempPoLineItemList);
    	}
    	
    	// get all of the associated PO__c records and find all of the related Store_Order__c IDs
    	List<PO__c> poList = Database.query('SELECT ' + String.join(new List<String>(objectFieldSetMap.get('PO__c')), ', ') + ' FROM PO__c WHERE Id IN :poIdSet AND IsDeleted = false ORDER BY Ship_To__r.Name ASC');
    	Set<Id> storeOrderIdSet = new Set<Id>();
    	for(PO__c po : poList){
    		storeOrderIdSet.add(po.Store_Order__c);
    	}
    	
    	// if any POs were not archived for any of the parent store orders then don't archive the store order
    	for(PO__c po : [SELECT Id, Store_Order__c FROM PO__c WHERE Store_Order__c IN :storeOrderIdSet])
    	{
    		if(!poIdSet.contains(po.Id)){
    			storeOrderIdSet.remove(po.Store_Order__c);
    		}
    	}
    	
    	// get all of the associated PO_Line_Item_Component__c records
    	List<PO_Line_Item_Component__c> poLineItemComponentList = Database.query('SELECT ' + String.join(new List<String>(objectFieldSetMap.get('PO_Line_Item_Component__c')), ', ') + ' FROM PO_Line_Item_Component__c WHERE PO_Line_Item__r.PO__c IN :poIdSet AND IsDeleted = false ORDER BY PO_Line_Item__r.PO__r.Ship_To__r.Name ASC');
    	
    	
    	// get all of the associated Store_Order__c records
    	List<Store_Order__c> storeOrderList = Database.query('SELECT ' + String.join(new List<String>(objectFieldSetMap.get('Store_Order__c')), ', ') + ' FROM Store_Order__c WHERE Id IN :storeOrderIdSet AND IsDeleted = false ORDER BY Bill_To__r.Name ASC');
    	
    	// get all of the associated Order_Line_Items__c records
    	List<Order_Line_Items__c> orderLineItemList = Database.query('SELECT ' + String.join(new List<String>(objectFieldSetMap.get('Order_Line_Items__c')), ', ') + ' FROM Order_Line_Items__c WHERE Store_Order__c IN :storeOrderIdSet AND IsDeleted = false ORDER BY Store_Order__r.Bill_To__r.Name ASC');
    	
    	// get all of the associated Order_Line_Item_Component__c records
    	List<Order_Line_Item_Component__c> orderLineItemComponentList = Database.query('SELECT ' + String.join(new List<String>(objectFieldSetMap.get('Order_Line_Item_Component__c')), ', ') + ' FROM Order_Line_Item_Component__c WHERE Order_Line_Item__r.Store_Order__c IN :storeOrderIdSet AND IsDeleted = false ORDER BY Order_Line_Item__r.Store_Order__r.Bill_To__r.Name ASC');
    	
    	
    	/**
    	 * Archive each list of objects starting at the bottom of the hierarchy but don't delete anything until all objects are archived successfully
    	 */
    	Boolean syncErrors = false;
    	
    	// archive the PO_Line_Item_Component__c records
    	List<String> shipToNameList = new List<String>();
    	for(PO_Line_Item_Component__c poLineItemComponent : poLineItemComponentList)
    	{
			Account shipToAccount = (Account)poLineItemComponent.PO_Line_Item__r.PO__r.Ship_To__r;
    		if(shipToAccount != null){
    			shipToNameList.add(shipToAccount.Name);
    		}
    		else{
    			shipToNameList.add(' ');
    		}
    	}
    	List<String> shipToNameRangeList = TestUtilities.findAlphaRange(shipToNameList);
    	Map<Id, Boolean> poLineItemComponentResultMap = dataArchiveService.archiveRecords('PO_Line_Item_Component__c (Ship To ' + String.join(shipToNameRangeList, '-') + ')', poLineItemComponentList, objectFieldSetMap.get('PO_Line_Item_Component__c'));
    	
    	// if any records failed to sync, set the syncErrors flag to stop the archiving process
    	syncErrors = !checkSyncStatus(poLineItemComponentResultMap);
    	
    	if(!syncErrors)
    	{
	    	// archive the PO_Line_Item__c records
	    	shipToNameList = new List<String>();
	    	for(PO_Line_Item__c poLineItem : poLineItemList)
	    	{
				Account shipToAccount = (Account)poLineItem.PO__r.Ship_To__r;
	    		if(shipToAccount != null){
	    			shipToNameList.add(shipToAccount.Name);
	    		}
	    		else{
	    			shipToNameList.add(' ');
	    		}
	    	}
	    	shipToNameRangeList = TestUtilities.findAlphaRange(shipToNameList);
	    	Map<Id, Boolean> poLineItemResultMap = dataArchiveService.archiveRecords('PO_Line_Item__c (Ship To ' + String.join(shipToNameRangeList, '-') + ')', poLineItemList, objectFieldSetMap.get('PO_Line_Item__c'));
	    	
	    	// if any records failed to sync, set the syncErrors flag to stop the archiving process
    		syncErrors = !checkSyncStatus(poLineItemResultMap);
	    	
	    	if(!syncErrors)
	    	{
		    	// archive the PO__c records
		    	shipToNameList = new List<String>();
		    	for(PO__c po : poList)
		    	{
					Account shipToAccount = (Account)po.Ship_To__r;
		    		if(shipToAccount != null){
		    			shipToNameList.add(shipToAccount.Name);
		    		}
		    		else{
		    			shipToNameList.add(' ');
		    		}
		    	}
		    	shipToNameRangeList = TestUtilities.findAlphaRange(shipToNameList);
		    	Map<Id, Boolean> poResultMap = dataArchiveService.archiveRecords('PO__c (Ship To ' + String.join(shipToNameRangeList, '-') + ')', poList, objectFieldSetMap.get('PO__c'));
		    	
		    	// if any records failed to sync, set the syncErrors flag to stop the archiving process
	    		syncErrors = !checkSyncStatus(poResultMap);
		    	
		    	if(!syncErrors)
		    	{
			    	// archive the Order_Line_Item_Component__c records
			    	List<String> billToNameList = new List<String>();
			    	for(Order_Line_Item_Component__c orderLineItemComponent : orderLineItemComponentList)
			    	{
						Account billToAccount = (Account)orderLineItemComponent.Order_Line_Item__r.Store_Order__r.Bill_To__r;
			    		if(billToAccount != null){
			    			billToNameList.add(billToAccount.Name);
			    		}
			    		else{
			    			billToNameList.add(' ');
			    		}
			    	}
			    	List<String> billToNameRangeList = TestUtilities.findAlphaRange(billToNameList);
			    	Map<Id, Boolean> orderLineItemComponentResultMap = dataArchiveService.archiveRecords('Order_Line_Item_Component__c (Bill To ' + String.join(billToNameRangeList, '-') + ')', orderLineItemComponentList, objectFieldSetMap.get('Order_Line_Item_Component__c'));
			    	
			    	// if any records failed to sync, set the syncErrors flag to stop the archiving process
		    		syncErrors = !checkSyncStatus(orderLineItemComponentResultMap);
			    	
			    	if(!syncErrors)
			    	{
				    	// archive the Order_Line_Items__c records
				    	billToNameList = new List<String>();
				    	for(Order_Line_Items__c orderLineItem : orderLineItemList)
				    	{
							Account billToAccount = (Account)orderLineItem.Store_Order__r.Bill_To__r;
				    		if(billToAccount != null){
				    			billToNameList.add(billToAccount.Name);
				    		}
				    		else{
				    			billToNameList.add(' ');
				    		}
				    	}
				    	billToNameRangeList = TestUtilities.findAlphaRange(billToNameList);
				    	Map<Id, Boolean> orderLineItemResultMap = dataArchiveService.archiveRecords('Order_Line_Items__c (Bill To ' + String.join(billToNameRangeList, '-') + ')', orderLineItemList, objectFieldSetMap.get('Order_Line_Items__c'));
				    	
				    	// if any records failed to sync, set the syncErrors flag to stop the archiving process
			    		syncErrors = !checkSyncStatus(orderLineItemResultMap);
				    	
				    	if(!syncErrors)
				    	{
					    	// archive the Store_Order__c records
					    	billToNameList = new List<String>();
					    	for(Store_Order__c storeOrder : storeOrderList)
					    	{
								Account billToAccount = (Account)storeOrder.Bill_To__r;
					    		if(billToAccount != null){
					    			billToNameList.add(billToAccount.Name);
					    		}
					    		else{
					    			billToNameList.add(' ');
					    		}
					    	}
					    	billToNameRangeList = TestUtilities.findAlphaRange(billToNameList);
					    	Map<Id, Boolean> orderResultMap = dataArchiveService.archiveRecords('Store_Order__c (Bill To ' + String.join(billToNameRangeList, '-') + ')', storeOrderList, objectFieldSetMap.get('Store_Order__c'));
					    	
					    	// if any records failed to sync, set the syncErrors flag to stop the archiving process
				    		syncErrors = !checkSyncStatus(orderResultMap);
					    	
					    	// if the archiving process succeeded then we can delete all of the records. Anything that did not sync will be left in place and retried on the next run
					    	if(!syncErrors)
					    	{
					    		delete poLineItemComponentList;
					    		delete poLineItemList;
					    		delete poList;
					    		delete orderLineItemComponentList;
					    		delete orderLineItemList;
					    		delete storeOrderList;
					    	}
				    	}
			    	}
		    	}
	    	}
    	}
    	
    	// allow any deferred DML to be processed
    	dataArchiveService.deferredDML();
    }
    
    global void finish(Database.BatchableContext BC)
    {
    	
    }
    
    // traverse the sync result map and determine if the sync was 100% successful (true) or contained failures (false)
    public Boolean checkSyncStatus(Map<Id, Boolean> syncStatusMap)
    {
    	for(Boolean syncResult : syncStatusMap.values())
    	{
    		if(syncResult == false){
    			return false;
    		}
    	}
    	
    	return true;
    }
    
}
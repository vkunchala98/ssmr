/** Class Name : SMR_EventItemCntrl
*  Description  : Apex class for List Button. 
*  Created By   : BTG 
*  Created On   : 07th May 2019
*
*  Modification Log:  
*  --------------------------------------------------------------------------------------------------------------------------------------
*   Developer                Date                   Modification ID      Description 
*  ---------------------------------------------------------------------------------------------------------------------------------------
*                                                                                    
**/
public class SMR_EventItemCntrl{
    private ApexPages.StandardSetController standardController;
    public String erroMsg {set;get;}
    public SMR_EventItemCntrl(ApexPages.StandardSetController standardController) {
        this.standardController = standardController;
    }
     
     public PageReference doSubmit()
      {       
        List<Event__c> selectedEvents= (List<Event__c>)standardController.getSelected();
        if(selectedEvents.size()>0){
            //callout
            String centralcusPodrId=System.Label.Smr_EventItems;
            String deliveryOp=System.Label.Smr_EventItems_Delivery;
            String sfdcBaseURL = URL.getSalesforceBaseUrl().toExternalForm();
            System.debug('Base URL: ' + sfdcBaseURL );
            String RecIds='';
            for (Event__c sObjectId : selectedEvents) {
                  RecIds+=sObjectId.id+',';
            }
            RecIds = RecIds.removeEnd(',');
            system.debug('>>>>'+RecIds);
            PageReference redirectPage = Page.loop__masslooplus;
            redirectPage.setRedirect(true);
            redirectPage.getParameters().put('retURL', sfdcBaseURL);
            redirectPage.getParameters().put('recordIds',RecIds);
            redirectPage.getParameters().put('sessionId',userInfo.getSessionId() );
            //redirectPage.getParameters().put('contactField','Customer_Email_Contact_ID__c');
            redirectPage.getParameters().put('hidecontact','true');
            redirectPage.getParameters().put('hideddp','true');
            redirectPage.getParameters().put('autorun','true');
            redirectPage.getParameters().put('attach','true');
            redirectPage.getParameters().put('ddpIds',centralcusPodrId);
            redirectPage.getParameters().put('deploy',deliveryOp);
            //contactField=Customer_Email_Contact_ID__c
            return redirectPage;
        }else{
            erroMsg ='error';
        }
        return null;
    }     
}
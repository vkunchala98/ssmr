/**
 * This class contains logic to archive data to Box.com using the Box.com Salesforce SDK
 */
public without sharing class BoxArchiveService implements I_DataArchiveService {
	
	private BoxApiConnectionExt api;
	private BoxFolder.Info archiveFolderInfo;		// holds a reference to the folder where archive files should be created
	private List<HTTP_Request_Log__c> logEntryList;	// this is used as a log entry queue so DML can be deferred until after all callouts are made
	private String archivePath;						// this is the file path to use for storing archive files
	
	public BoxArchiveService()
	{
		// get the Box.com API settings
		Box_com_Integration__c boxSettings = Box_com_Integration__c.getOrgDefaults();
		
		// start the connection to Box.com
		api = new BoxApiConnectionExt(boxSettings.Client_ID__c, boxSettings.Client_Secret__c, boxSettings.Access_Token__c, boxSettings.Refresh_Token__c);
		
		// make sure HRLentryList is initialized
		if(logEntryList == null){
			logEntryList = new List<HTTP_Request_Log__c>();
		}
		
		// create a default archive path
		this.archivePath = 'Archive - ' + Datetime.now().format('yyyy-MM-dd HHmmss');
	}
	
	public void setArchivePath(String path){
		this.archivePath = path;
	}
	
	/**
	 * This method will take the provided records, convert them to CSV data, and create a file in the Box.com account
	 */
	public BoxFolder getArchiveFolder()
	{
		// see if there is a folder already with the name we're about to use
		if(archiveFolderInfo == null)
		{
			String archiveFolderName = archivePath;
			String endpoint = 'https://api.box.com/2.0/search?type=folder&query=' + EncodingUtil.urlEncode(archiveFolderName, 'UTF-8');
			
			BoxApiRequest boxRequest = new BoxApiRequest(this.api, endpoint, BoxApiRequest.METHOD_GET);
			boxRequest.setTimeout(this.api.getTimeout());
			HttpResponse boxSearchResponse;
			try
			{
				boxSearchResponse = boxRequest.send();
				
				// if there was an error due to authentication then try refreshing the access token and running the search again
				if(boxSearchResponse.getStatusCode() == 401)
				{
					this.api.refresh();
					boxSearchResponse = boxRequest.send();
				}
				
				// if there is still an error that didn't throw an exception, log it
				if(boxSearchResponse.getStatusCode() >= 400)
				{
					// log the error
					HttpRequestLogService.HRL_Entry searchRequestLogEntry = new HttpRequestLogService.HRL_Entry('Outbound');
					searchRequestLogEntry.setRequestMethod('GET');
					searchRequestLogEntry.setRequestEndpoint(endpoint);
					searchRequestLogEntry.addResponseData(boxSearchResponse);
					logEntryList.add(searchRequestLogEntry.getRawLog());
				}
				// otherwise, process the results
				else
				{
					// find the folder in the search results
					Map<String, Object> resultMap = (Map<String, Object>)JSON.deserializeUntyped(boxSearchResponse.getBody());
					Integer totalResults = Integer.valueOf(resultMap.get('total_count'));
					if(totalResults > 0)
					{
						List<Object> resultList = (List<Object>)resultMap.get('entries');
						for(Object result : resultList)
						{
							Map<String, Object> recordMap = (Map<String, Object>)result;
							if(recordMap.get('name') == archiveFolderName){
								archiveFolderInfo = new BoxFolder.Info(JSON.serialize(recordMap));
							}
						}
					}
				}
			}
			catch(BoxApiRequest.BoxApiRequestException e)
			{
				// log the error unless it was a 404 since it's ok if the folder wasn't found
				if(boxSearchResponse != null && boxSearchResponse.getStatusCode() != 404)
				{
					HttpRequestLogService.HRL_Entry searchRequestLogEntry = new HttpRequestLogService.HRL_Entry('Outbound');
					searchRequestLogEntry.setRequestMethod('GET');
					searchRequestLogEntry.setRequestEndpoint(endpoint);
					BoxApiConnectionExt.logHttpError(e, searchRequestLogEntry);
					logEntryList.add(searchRequestLogEntry.getRawLog());
				}
			}
			
			// if the folder still wasn't set then we need to create it
			if(archiveFolderInfo == null)
			{
				// create the folder
				BoxFolder rootFolder = BoxFolder.getRootFolder(this.api);
				try{
					archiveFolderInfo = rootFolder.createFolder(archiveFolderName);
				}
				catch(BoxApiRequest.BoxApiRequestException createFolderException)
				{
					HttpRequestLogService.HRL_Entry createFolderRequestLogEntry = new HttpRequestLogService.HRL_Entry('Outbound');
					createFolderRequestLogEntry.setRequestMethod('POST');
					createFolderRequestLogEntry.setRequestEndpoint('https://api.box.com/2.0/folders');
					BoxApiConnectionExt.logHttpError(createFolderException, createFolderRequestLogEntry);
					logEntryList.add(createFolderRequestLogEntry.getRawLog());
				}
			}
		}
		
		return (archiveFolderInfo != null ? new BoxFolder(this.api, archiveFolderInfo.Id) : null);
	}
	
	/**
	 * This method will take the provided records, convert them to CSV data, and create a file in the Box.com account
	 */
	public Map<Id, Boolean> archiveRecords(String archiveName, List<SObject> sobjectList, Set<String> objectFieldSet)
	{
		Map<Id, Boolean> statusMap = new Map<Id, Boolean>();
		for(SObject sob : sobjectList){
			statusMap.put(sob.Id, false);
		}
		
		// if no records were provided or the refresh token had to be used then don't bother trying to archive anything
		if(sobjectList.isEmpty()){
			return statusMap;
		}
		
		// get the archive folder we'll be creating files in
		BoxFolder archiveFolder = this.getArchiveFolder();
		
		// convert the records to CSV data
		String csvData = TestUtilities.convertToCSV((new List<String>(objectFieldSet)), sobjectList);
		
		// find out if the requested file name is already in use and if so, find out where we are in the sequence
		List<BoxItem.Info> archiveFileList = archiveFolder.getChildren(null, 1000);
		Integer fileSequenceNumber = 0;
		for(BoxItem.Info boxItemInfo : archiveFileList)
		{
			if(boxItemInfo.type == 'file' && boxItemInfo.getName().startsWith(archiveName))
			{
				List<String> filenamePartsList = boxItemInfo.getName().replace('.csv', '').split('-');
				Integer lastFilenamePartIndex = filenamePartsList.size()-1;
				
				if(filenamePartsList.size() > 1)
				{
					try
					{
						if(Integer.valueOf(filenamePartsList[lastFilenamePartIndex]) > fileSequenceNumber){
							fileSequenceNumber = Integer.valueOf(filenamePartsList[lastFilenamePartIndex]);
						}
					}
					catch(System.TypeException e)
					{
						// if the last filename part is not a number character then this exception will be thrown so we need to catch it and treat it as if no number was found
						fileSequenceNumber = 1;
					}
				}
				else if(fileSequenceNumber == 0){
					fileSequenceNumber = 1;
				}
			}
		}
		
		// make sure the archive name includes the file extension
		if(archiveName.endsWith('.csv')){
			archiveName = archiveName.replace('.csv', '');
		}
		
		try
		{
			BoxFile.Info newFileInfo = archiveFolder.uploadFile(Blob.valueOf(csvData), archiveName + (fileSequenceNumber == 0 ? '' : '-' + (fileSequenceNumber + 1)) + '.csv');
			
			// update the status map if the file was created
			if(newFileInfo != null)
			{
				for(SObject sob : sobjectList){
					statusMap.put(sob.Id, true);
				}
			}
		}
		catch(BoxApiRequest.BoxApiRequestException e)
		{
			// log the failure
			HttpRequestLogService.HRL_Entry fileUploadRequestLogEntry = new HttpRequestLogService.HRL_Entry('Outbound');
			fileUploadRequestLogEntry.setRequestMethod('POST');
			fileUploadRequestLogEntry.setRequestEndpoint('files/content?parent_id=' + archiveFolderInfo.Id);
			BoxApiConnectionExt.logHttpError(e, fileUploadRequestLogEntry);
			logEntryList.add(fileUploadRequestLogEntry.getRawLog());
		}
		
		return statusMap;
	}
	
	/**
	 * This method should be used when the calling code is done with this instance (helps avoid hitting a platform restriction regarding DML before callouts)
	 */
	public void deferredDML()
	{
		// save the HTTP request logs generated over the life of this instance
		insert logEntryList;
		logEntryList.clear();
		
		// if the tokens were refreshed and the custom setting was updated in memory then save the new tokens
		if(this.api.tokensRefreshed)
		{
        	this.api.saveCustomSetting();
        	this.api.tokensRefreshed = false;
        }
	}
    
}
/**
 * This class allows you to schedule the execution of the Batch_ArchiveOrderData class
 */
public class Sched_ArchiveOrderData implements Schedulable {
	
	public void execute(SchedulableContext schCon)
	{
        Batch_ArchiveOrderData bc = new Batch_ArchiveOrderData();
        Database.executeBatch(bc);
	}
    
}
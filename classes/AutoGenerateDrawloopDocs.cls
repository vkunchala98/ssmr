/**
 * Batch class to generate the drawloop documents 
 *
 * @version     2019-01-17  BTG      first version
 *              
 */ 
global class AutoGenerateDrawloopDocs implements Database.Batchable<sObject>, Database.AllowsCallouts{
    public String query;
    
    global Database.QueryLocator start(Database.BatchableContext BC){
     /*   //String recordInfo ='a04U0000014mACB';
        Set<ID> ids = new Set<ID>{'a040B00001wnJnC','a04U000001JfLiI'}; */
        String myStatus='Review Completed';

        query='SELECT Id, Type_of_display__c, Status__c IsProcessed__c FROM Item__c WHERE Id IN:ids AND IsProcessed__c!=true AND Status__c=:myStatus';
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<sObject> scope){
        List<Item__c> myNewLst =[SELECT Id, Type_of_display__c, IsProcessed__c,(select id, Type__c from Upload_Pictures__r) FROM Item__c where id in:scope];
        
        system.debug('$$$scope$$$'+scope);
        List<Loop__DDP__c> ddps = [SELECT Id, Name, (SELECT Id,name FROM Loop__Custom_Integration_Options__r ) FROM Loop__DDP__c];
        Map<String,Loop__DDP__c> myDdpMap = new Map<String,Loop__DDP__c>();
        for(Loop__DDP__c eachDDP :ddps){
         myDdpMap.put(eachDDP.name,eachDDP);
        }
        if (ddps == null || ddps.size() < 1  ) {
           // throw new NoDDPInfoException('The DDP or Delivery Option specified was not found.');
            system.debug('ERROR no drawloops >>>');
        }else{
                Loop.loopMessage lm = new Loop.loopMessage();
                // Session id
                lm.sessionId = userInfo.getSessionId();
                for (Item__c sObjectId : myNewLst) {
                    sObject mySObject = sObjectId;
                    Loop__DDP__c dynamicDP= new Loop__DDP__c();
                    if(sObjectId.Upload_Pictures__r.size()>0){
                      //single item component
                      if(sObjectId.Upload_Pictures__r.size()==1){
                        dynamicDP=myDdpMap.get('template1');
                        }else if( sObjectId.Upload_Pictures__r.size()>1 && sObjectId.Upload_Pictures__r.size()<=6){
                        dynamicDP=myDdpMap.get('template2');
                       }else if(sObjectId.Upload_Pictures__r.size()>1 && sObjectId.Upload_Pictures__r[0].Type__c=='Grid'){
                        dynamicDP=myDdpMap.get('template3');
                       }else if(sObjectId.Upload_Pictures__r.size()>6){
                        dynamicDP=myDdpMap.get('template4');
                        }else{
                         dynamicDP=myDdpMap.get('displayImg');
                        }
                    }else{
                        dynamicDP=myDdpMap.get('SingleItems');
                    }
                    
                    // ADD A DDP RUN REQUEST
                    lm.requests.add(new Loop.loopMessage.loopMessageRequest(
                        sObjectId.Id, 
                        dynamicDP.Id,
                        new Map<string, string>{
                            'deploy' => dynamicDP.Loop__Custom_Integration_Options__r[0].Id
                                }
                    ));
                }
                // SEND ALL DDP RUN REQUESTS IN A SINGLE CALL OUT
                lm.sendAllRequests();
        } 
    }
    
    global void finish(Database.BatchableContext BC){
        
        System.debug('Batch class is finished>>>');
        
        
    }
}
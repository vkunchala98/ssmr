@isTest

public class TestDirectInvoiceControlleBatchScheduler {

    public static TestMethod void DirectInvoiceControllerBatchScheduler () {

        TestUtilities.makeCustomSettings();
    
        RecordType cusRecType = [SELECT Id FROM RecordType WHERE DeveloperName = 'Customer' AND SobjectType ='Account'];
        RecordType vendorRecType = [SELECT Id FROM RecordType WHERE DeveloperName = 'Vendor' AND SobjectType ='Account'];
        RecordType poRecType = [SELECT Id FROM RecordType WHERE DeveloperName = 'Direct_Bill' AND SobjectType = 'PO__c'];
        
        List<Account> accList = new List<Account>{
                                                     new Account(Name = 'Test Account1',RecordTypeId = cusRecType.Id),
                                                     new Account(Name = 'Test Account',RecordTypeId = vendorRecType.Id) 
                                                  };
        Insert accList;
        
        List<Item__c> itemList = new List<Item__c>{
                                                    new Item__c(Vendor__c = accList[1].Id,Item_Status__c = 'Active',Item_UPC__c = 'x-xxxxx-xxxxx-x',Item__c = 'FLEECEFD',
                                                                Name = 'testItem',Cost_Per_Deal__c = 23, Case_UPC__c='x-xxxxx-xxxxx-x')
                                                
                                                };
        Insert itemList ;
        
        List<Store_Order__c> stOrdList = new List<Store_Order__c>{
                                                                      new Store_Order__c(Account__c = accList[0].Id,Bill_Types__c='Direct Bill',Bill_To__c=accList[0].Id)
                                                                 };
        Insert stOrdList;
        
        List<Order_Line_Items__c > oliItemList = new List<Order_Line_Items__c >{
                                                                                new Order_Line_Items__c(Store_Order__c = stOrdList[0].Id,Store_Product__c = itemList[0].Id),
                                                                                new Order_Line_Items__c(Store_Order__c = stOrdList[0].Id,Store_Product__c = itemList[0].Id)                                                                       
                                                                            };
        Insert oliItemList;
        
        List<PO__c> poList = new List<PO__c>{
                                                new PO__c(Account__c = accList[1].Id,Store_Order__c = stOrdList[0].Id, Order_Status__c = 'Open',RecordTypeId=poRecType.Id,Bill_To__c = accList[0].Id)
                                            };
        Insert poList;
        
        List<PO_Line_Item__c> poliList = new List<PO_Line_Item__c>{
                                                                       new PO_Line_Item__c(Store_Order__c = stOrdList[0].Id,PO__c=poList[0].Id,Registered__c= true,Order_Line_Item__c=oliItemList[0].Id),
                                                                       new PO_Line_Item__c(Store_Order__c = stOrdList[0].Id,PO__c=poList[0].Id,Registered__c= true,Order_Line_Item__c=oliItemList[1].Id)
        
                                                                   };
        Insert poliList;
        
        string sch = '0 0 0 L * ?';
        DirectInvoiceControllerBatchScheduler dic  = new DirectInvoiceControllerBatchScheduler();
        system.schedule('Direct Invoice Per Month',sch,dic);
        DirectInvoiceControllerBatch dicb = new DirectInvoiceControllerBatch();
        Database.executeBatch(dicb);
    
    }


}
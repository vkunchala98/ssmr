/**
 * This class contains unit tests for validating the behavior of the SBAppController class and related code
 */
@isTest
private class TestSBAppController {
	
	@TestSetup
	public static void setup()
	{
		// populate custom settings 
		
		// create bill to account
		
		// create vendor accounts
		
		// create items
		
		// create community user
		
	}

    static testMethod void myUnitTest() 
    {
        SBAppController con = new SBAppController();
        
    /*    // test getting the show cart
        Show_Cart__c showCart = TestUtilities.generateShowCarts(1)[0];
        showCart.Bill_To__c = ;
        
        insert showCart;
    */    
        // test the item search
        Map<String, String> itemSearchParamMap = new Map<String, String>();
        SBAppController.searchForItems(itemSearchParamMap);
        
        // test adding items to the cart
        List<Map<String, String>> itemMapList = new List<Map<String, String>>();
   //     SBAppController.addItemsToCart(showCart.Id, itemMapList);
        
        // test removing items from the cart
        List<Id> cartLineItemIdList = new List<Id>();
        SBAppController.removeItemsFromCart(cartLineItemIdList);
        
        // test submitting the cart
   //     SBAppController.submitCart();
        
        // test setting the shipping accounts
        List<Id> accountIdList = new List<Id>();
    //    SBAppController.setShippingAccounts(showCart.Id, accountIdList);
    }
}
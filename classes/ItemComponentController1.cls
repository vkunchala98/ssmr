public without sharing class ItemComponentController1{
    
    public Item__c itemObj {get; set;}
    public List<Item_Component__c> itemList {get; set;}
    public String successMsg {get; set;}
    public Id itemId;
    public User vendorUser {get;set;}
    public Attachment attachment;
    public Boolean newItem {get;set;}
    public Id rt;
    public Boolean isInsert {get;set;}
    
    
    public ItemComponentController1(){
       itemList = new List<Item_Component__c>();

        successMsg = '';
      rt = Schema.Sobjecttype.Item__c.getRecordTypeInfosByName().get('Item').getRecordTypeId();
            isInsert = true;
            itemList = new List<Item_Component__c>();
            Item_Component__c ic = new Item_Component__c();
            itemList.add(ic);
    

        vendorUser = [SELECT AccountId FROM User WHERE Id = :UserInfo.getUserId() LIMIT 1];
        if(vendorUser.AccountId != null){
            itemObj.Vendor__c = vendorUser.AccountId;
                    itemObj.RecordTypeId = rt;

        }
        //Have to set the itemObj record type to be multi-item
    }

    public void showItemComponents(){
        addItemComponents();
    }

    public void addItemComponents(){
        system.debug('--------itemList-------'+itemList.size());
        Item_Component__c newItemComponent = new Item_Component__c(Item__c = itemObj.Id);
        system.debug('--------newItemComponent -------'+newItemComponent);
        itemList.add(newItemComponent);
        system.debug('--------itemList-------'+itemList);
    }

    public PageReference doSaveAndNew(){
        Savepoint sp = Database.setSavepoint();
        try {
            if(itemObj.Item_Status__c == null){
                itemObj.Item_Status__c = 'Active';
            }
            if(isInsert) itemObj.Id = null;
            upsert itemObj;
            for(Item_Component__c ic : itemList){
                ic.Item__c = itemObj.Id;
            }
            upsert itemList;
            PageReference pr = Page.ItemComponent;
            pr.setRedirect(true);
            return pr;  
        } catch (exception e) {
            System.debug(System.Logginglevel.INFO, 'BRAND2: ' + e.getMessage());      
            apexPages.addMessages(e);
            Database.rollback(sp);
            return null;
        }

        
    }

    public PageReference doSave(){
        Savepoint sp = Database.setSavepoint();
        try {
            if(itemObj.Item_Status__c == null){
                itemObj.Item_Status__c = 'Active';
            }
            if(isInsert) itemObj.Id = null;
            upsert itemObj;
            for(Item_Component__c ic : itemList){
                ic.Item__c = itemObj.Id;
            }
            upsert itemList;
            String recordURL = '/';
            if(vendorUser.AccountId != null) recordURL += 'vendors/';
            recordURL += itemObj.Id;
            PageReference pr = new PageReference(recordURL);
            return pr;
        } catch (exception e) {
            System.debug(System.Logginglevel.INFO, 'BRAND2: ' + e.getMessage());      
            apexPages.addMessages(e);
            Database.rollback(sp);
            return null;
        }
    }
}
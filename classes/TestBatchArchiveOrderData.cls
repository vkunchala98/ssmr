/**
 * This class contains unit tests for validating the behavior of the Batch_ArchiveOrderData class and any related code
 */
@isTest
private class TestBatchArchiveOrderData {
	
	@TestSetup
	static void setupData()
	{
		// create custom settings
		TestUtilities.makeCustomSettings();
		
		// create customer and vendor accounts
		List<Account> accountList = TestUtilities.generateCustomerAccounts(1);
		List<Account> vendorAccountList = TestUtilities.generateVendorAccounts(1);
		accountList.add(vendorAccountList[0]);
		insert accountList;
		
		// create items
		List<Item__c> itemList = TestUtilities.generateItems(1);
		itemList[0].Vendor__c = vendorAccountList[0].Id;
		insert itemList;
	}

    static testMethod void testOrderDataArchival(){
    	TestBatchArchiveOrderData.testOrderDataArchivalHelper(1);
    }

    static testMethod void testOrderDataArchivalBulk(){
    	TestBatchArchiveOrderData.testOrderDataArchivalHelper(200);
    }

    static void testOrderDataArchivalHelper(Integer quantity)
    {
    	// get customer and vendor accounts
    	Map<String, Schema.RecordTypeInfo> accountRecordTypeMap = TestUtilities.getAccountRecordTypes();
    	List<Account> customerAccountList = [SELECT Id FROM Account WHERE RecordTypeId = :accountRecordTypeMap.get('Customer').getRecordTypeId()];
    	List<Account> vendorAccountList = [SELECT Id FROM Account WHERE RecordTypeId = :accountRecordTypeMap.get('Vendor').getRecordTypeId()];
    	
        // create store orders
        List<Store_Order__c> storeOrderList = TestUtilities.generateStoreOrders(quantity);
        for(Store_Order__c storeOrder : storeOrderList){
        	storeOrder.Account__c = customerAccountList[0].Id;
        }
        insert storeOrderList;
        
        // create order line items
        List<Order_Line_Items__c> orderLineItemList = TestUtilities.generateOrderLineItems(quantity);
        for(Integer ii=0; ii < orderLineItemList.size(); ++ii)
        {
        	Order_Line_Items__c orderLineItem = orderLineItemList[ii];
        	orderLineItem.Store_Order__c = storeOrderList[ii].Id;
        }
        insert orderLineItemList;
        
        // create order line item components
        List<Order_Line_Item_Component__c> orderLineItemComponentList = TestUtilities.generateOrderLineItemComponents(quantity);
        for(Integer ii=0; ii < orderLineItemComponentList.size(); ++ii)
        {
        	Order_Line_Item_Component__c orderLineItemComponent = orderLineItemComponentList[ii];
        	orderLineItemComponent.Order_Line_Item__c = orderLineItemList[ii].Id;
        }
        insert orderLineItemComponentList;
        
        // create POs
        List<PO__c> poList = TestUtilities.generatePOs(quantity);
        for(Integer ii=0; ii < poList.size(); ++ii)
        {
        	PO__c po = poList[ii];
        	po.Store_Order__c = storeOrderList[ii].Id;
        	po.Account__c = vendorAccountList[Math.mod(ii, vendorAccountList.size())].Id;
        	po.Order_Status__c = 'Completed';
        	po.Ship_To__c = customerAccountList[0].Id;
        }
        insert poList;
        
        // create PO line items
        Datetime twoYearsAgo = Datetime.now().addYears(-2);
        List<PO_Line_Item__c> poLineItemList = TestUtilities.generatePOlineItems(quantity);
        for(Integer ii=0; ii < poLineItemList.size(); ++ii)
        {
        	PO_Line_Item__c poLineItem = poLineItemList[ii];
        	poLineItem.PO__c = poList[ii].Id;
        	poLineItem.Registered_On__c = twoYearsAgo;
        	poLineItem.Registered__c = true;
        }
        insert poLineItemList;
        update poLineItemList;	// this is to override the automation that sets the registered date to the date that the registered checkbox was checked
        
        // create PO line item components
        List<PO_Line_Item_Component__c> poLineItemComponentList = TestUtilities.generatePOlineItemComponents(quantity);
        for(Integer ii=0; ii < poLineItemComponentList.size(); ++ii)
        {
        	PO_Line_Item_Component__c poLineItemComponent = poLineItemComponentList[ii];
        	poLineItemComponent.PO_Line_Item__c = poLineItemList[ii].Id;
        }
        insert poLineItemComponentList;
        
        // set up the callout mock class
        Test.setMock(HttpCalloutMock.class, new TestMultiMockCallout());
        
        // run the scheduler for the data archival service
        Test.startTest();
        
        String sch = '0 0 0 L * ?';
//        system.schedule('Test schedule archival', sch, new Sched_ArchiveOrderData());
        
        // queue up the job directly so it actually executes
        Batch_ArchiveOrderData bc = new Batch_ArchiveOrderData();
        Database.executeBatch(bc);
        
        Test.stopTest();
        
        // verify that the records were all cleaned up
        storeOrderList = [SELECT Id FROM Store_Order__c];
        System.assertEquals(0, storeOrderList.size());
        orderLineItemList = [SELECT Id FROM Order_Line_Items__c];
        System.assertEquals(0, orderLineItemList.size());
        orderLineItemComponentList = [SELECT Id FROM Order_Line_Item_Component__c];
        System.assertEquals(0, orderLineItemComponentList.size());
        poList = [SELECT Id FROM PO__c];
        System.assertEquals(0, poList.size());
        poLineItemList = [SELECT Id FROM PO_Line_Item__c];
        System.assertEquals(0, poLineItemList.size());
        poLineItemComponentList = [SELECT Id FROM PO_Line_Item_Component__c];
        System.assertEquals(0, poLineItemComponentList.size());
    }
}
@isTest
private class TestPOTrigger {
	
	@isTest static void test_method_one() {

		TestUtilities.makeCustomSettings();

		Account a = new Account();
		a.Name = 'test';
		a.Type = 'Vendor';
		insert a;

		Account a2 = new Account();
		a2.Name = 'test';
		a2.Type = 'Customer';
		insert a2;

		Account a3 = new Account();
		a3.Name = 'test';
		a3.Type = 'Customer';
		insert a3;

		Item__c i1 = new Item__c();
		i1.Vendor__c = a.Id;
		i1.Item__c = 'test';
		i1.Type_of_display__c = 'Case Good';
		i1.Name = 'test';
		i1.Cost_Per_Deal__c = 0;
		i1.Pack_Per_Deal__c = 0;
		i1.Case_Weight__c = 0;
		i1.Case_Cube__c = 0;
		i1.Case_Dimension_LxWxH__c = '0';
		i1.Ti_Hi__c = '0';
		insert i1;
		i1.Item__c = 'test1';
		update i1;

		PO__c po = new PO__c();
		po.RecordTypeId = [SELECT Id FROM RecordType WHERE Name = 'Assorted' LIMIT 1].Id;
		po.Account__c = a.Id;
		po.Item__c = i1.Id;
		po.Vendor_Invoice_Num__c = '1234';
		po.Vendor_Invoice_Date__c = Date.today();
		po.PO_Type__c = 'Central Bill';
		//po.Amount_Assorted__c =  '$500';
		po.Bill_To__c = a2.Id;
		po.Ship_To__c = a3.Id;
		insert po;

		po.Register_ALL_Items__c = true;
		update po;
	}	
}
/** Class Name : EmailDirectBillCustomerPOsCtrlTest
*  Description  : Test class for EmailDirectBillCustomerPOsCtrl
*  Created By   : BTG vk
*  Created On   : 21st Feb 2019
*
*  Modification Log:  
*  --------------------------------------------------------------------------------------------------------------------------------------
*   Developer                Date                   Modification ID      Description 
*  ---------------------------------------------------------------------------------------------------------------------------------------
*                                                                                    
**/
@isTest(seeAllData=true)
public class EmailDirectBillCustomerPOsCtrlTest{
private static testmethod void positivescenario(){
 Id VendorRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Customer').getRecordTypeId();
    Id DirectRecordTypeId = Schema.SObjectType.PO__c.getRecordTypeInfosByName().get('Direct Bill').getRecordTypeId();
 String myFinl= String.valueOf(DirectRecordTypeId).substring(0, 15);
 
 
   Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
      User u2 = new User(Alias = 'newUser', Email='newuser44444@testorg.com',
         EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
         LocaleSidKey='en_US', ProfileId = p.Id,
         TimeZoneSidKey='America/Los_Angeles', UserName='newuser1234444@testorg.com');
      system.runas(u2){
         String label=System.label.Central_Customer_PO;
     Loop__DDP__c ddp = new Loop__DDP__c(Name='My DDP');
        insert ddp;
        
        Loop__DDP_Integration_Option__c delivOpt = new Loop__DDP_Integration_Option__c(Name='Email', Loop__DDP__c=ddp.Id);
        insert delivOpt;
        
       Account acc= new Account();
       acc.Name='Test vendor';
       acc.RecordTypeid=VendorRecordTypeId;
       insert acc;
       Account acc1= new Account();
       acc1.Name='Test billing';
       acc1.BillingStreet='test street';
       acc1.BillingCity='test city';
       acc1.BillingState='CA';
       acc1.BillingPostalcode='521235';
       acc1.BillingCountry='UAS';
       acc1.ShippingStreet='test street';
       acc1.ShippingCity='test city';
       acc1.ShippingState='CA';
       acc1.ShippingPostalcode='521235';
       acc1.ShippingCountry='UAS'; 
       acc1.Terms__c='Net 21';
       acc1.Central_Bill_Payment_Terms__c='Net 21'; 
       acc1.Direct_Bill_Payment_Terms__c='Net 21';  
       //acc1.Email_Contact__c=  
       insert acc1;
       
       Contact mycon= new Contact();
       mycon.firstname='Test Firstname';
       mycon.lastname='Test Lastname';
       mycon.email='btgcorp@btgcorp.com';
       mycon.AccountId=acc1.id;
       insert mycon;
       
       acc1.Email_Contact__c=mycon.id;
       update acc1;
      
       List<Po__c> poList= new List<Po__c>();
       PO__c po = new PO__c();
       //po.Name='Test po';
       po.Account__c=acc.id;
       po.Bill_To__c=acc1.id;
       po.Ship_To__c=acc1.id;
       po.RecordTypeId =myFinl;
       po.Order_Status__c='open';
       poList.add(po);
       PO__c po2 = new PO__c();
       //po.Name='Test po';
       po2.Account__c=acc.id;
       po2.Bill_To__c=acc1.id;
       po2.Ship_To__c=acc1.id;
       po2.RecordTypeId=myFinl;
       po2.Order_Status__c='open';
       poList.add(po2);
      // insert poList;
       
       
       
       Test.startTest();
          Test.setCurrentPage(Page.EmailDirectBillCustomerPOs);
          ApexPages.StandardSetController stdSetController = new ApexPages.StandardSetController(poList);
          stdSetController.setSelected(poList);
          EmailDirectBillCustomerPOsCtrl ext = new EmailDirectBillCustomerPOsCtrl(stdSetController);
          ext.doSubmit();
      Test.stopTest();
}
}
}
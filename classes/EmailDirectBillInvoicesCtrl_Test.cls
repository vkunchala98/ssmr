/** Class Name : EmailDirectBillInvoicesCtrl_Test
*  Description  : Apex class for EmailDirectBillInvoicesCtrl_Test. 
*  Created By   : BTG 
*  Created On   : 21st Feb 2019
*
*  Modification Log:  
*  --------------------------------------------------------------------------------------------------------------------------------------
*   Developer                Date                   Modification ID      Description 
*  ---------------------------------------------------------------------------------------------------------------------------------------
*                                                                                    
**/

@isTest(SeeAllData=true)
public class EmailDirectBillInvoicesCtrl_Test{
  private static testmethod void positivescenario(){
    //Id VendorRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Vendor').getRecordTypeId();
   // String DirectRecordTypeId = Schema.SObjectType.PO__c.getRecordTypeInfosByName().get('Central Bill').getRecordTypeId();
    //Id cenrecordtypeid=[SELECT Id, DeveloperName FROM RecordType where DeveloperName='Central_Bill' limit 1].id;
  Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
      User u2 = new User(Alias = 'newUser', Email='newuser44444@testorg.com',
         EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
         LocaleSidKey='en_US', ProfileId = p.Id,
         TimeZoneSidKey='America/Los_Angeles', UserName='newuser1234444@testorg.com');
         system.runas(u2){
         String label=System.label.Central_Customer_PO;
     Loop__DDP__c ddp = new Loop__DDP__c(Name='My DDP');
        insert ddp;
        
        Loop__DDP_Integration_Option__c delivOpt = new Loop__DDP_Integration_Option__c(Name='Email', Loop__DDP__c=ddp.Id);
        insert delivOpt;
        
       
       Account acc= new Account();
       acc.Name='Test vendor';
      // acc.RecordTypeid=VendorRecordTypeId;
       insert acc;
       
       contact con= new Contact();
       con.LastName='test Name';
       con.Email='test@gmail.com';
       con.phone='1234';
       con.AccountId=acc.id;
       insert con;
       
       acc.AP_Email_Contact__c=con.id;
       update acc;
       
      
       List<Invoice__c > InvList= new List<Invoice__c >();
       Invoice__c inv = new Invoice__c();
       inv.Account__c=acc.id;
       InvList.add(inv);
       Invoice__c inv2 = new Invoice__c();       
       inv2.Account__c=acc.id;
       InvList.add(inv2);
       
       Test.startTest();
          Test.setCurrentPage(Page.EmailDirectBillInvoices);
          ApexPages.StandardSetController stdSetController = new ApexPages.StandardSetController(InvList);
          stdSetController.setSelected(InvList);
          EmailDirectBillInvoicesCtrl ext = new EmailDirectBillInvoicesCtrl(stdSetController);
          ext.doSubmit();
      Test.stopTest();


   }
   }
}
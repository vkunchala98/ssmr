global with sharing class ShowCartDAO {
	
	global static List<X2019_Show_Cart__c> getCustomerShowCart(String customerId){
		return 	[SELECT	Id,
						Name,
						Account__c,
						Account__r.Id,
						Account__r.Name,
						Total_Amount__c,
						(SELECT 	Id,
									Store_Product__c,
									Store_Product__r.Name,
									Store_Product__r.Item__c,
									Store_Product__r.Pack_Per_Deal__c,
									Store_Product__r.Cost_Per_Item__c,
									Store_Product__r.Cost_Per_Deal__c,
									Store_Product__r.X2019_Seasonal_Section__c,
									Store_Product__r.Vendor__c,
									Store_Product__r.Vendor__r.Name,
									Quantity__c,
									Arrival_Date__c,
									Ship_To__c,
									Notes__c
							FROM X2019_Show_Cart_Line_Items__r),
						(SELECT 	Id,
									Vendor__c
							FROM X2019_Show_Visited_Vendors__r)
					FROM 	X2019_Show_Cart__c
					WHERE 	Account__c = :customerId
					LIMIT 	1];

	}

	global static List<Account> getVendorList(){
		return [SELECT 	Id,
						Name
					FROM Account
					WHERE RecordType.DeveloperName = 'Vendor'];
	}

	global static List<Account> getVendorListFilterName(String nameSearch){
		String nameFilter = '%' + nameSearch + '%';
		return [SELECT 	Id,
						Name
					FROM Account
					WHERE RecordType.DeveloperName = 'Vendor'
						AND Name LIKE :nameFilter ];
	}
}
public with sharing class redirectToItemListViewController {
	public PageReference redirectToList() {
    Schema.DescribeSObjectResult result = Item__c.SObjectType.getDescribe();

    PageReference pageRef = new PageReference('/' + result.getKeyPrefix());
    pageRef.setRedirect(true);
    return pageRef;
}
}
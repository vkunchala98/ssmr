public with sharing class PoLineItemService {
	
	/**
	 * Check if all PO line items have been registered/shipped and update the PO order status to complete if they have been
	 */
	public static List<PO__c> setCompletedPoOrderStatus(List<PO_Line_Item__c> poLineItemList)
	{
		Set<Id> poIds = new Set<Id>();
		for(PO_Line_Item__c poli : poLineItemList){
			poIds.add(poli.PO__c);
		}
	
		List<PO__c> posAndItems = [SELECT Id,Order_Status__c, (SELECT Id, Registered__c, Did_Not_Ship__c FROM PO_Line_Items__r) FROM PO__c WHERE Id IN :poIds];
		List<PO__c> updateThese = new List<PO__c>();
		for(PO__c po : posAndItems)
		{
			if(po.Order_Status__c =='Open')
			{
				Boolean changeOrderStatus = TRUE;
				for(PO_Line_Item__c poli : po.Po_Line_Items__r)
				{ 
					if(poli.Registered__c == FALSE && poli.Did_Not_Ship__c == FALSE){
						changeOrderStatus = FALSE;
					}
				}
				if(changeOrderStatus)
				{
					po.Order_Status__c = 'Completed';
					updateThese.add(po);
				}
			}
		}
		
		return updateThese;
	}
	
	/**
	 * Concatenate comments from PO line items onto the parent PO
	 */
	public static List<PO__c> concatenateCommentsToPo(List<PO_Line_Item__c> poLineItemList)
	{
		Set<Id> poIdSet = new Set<Id>();
		for(PO_Line_Item__c poli : poLineItemList){
			poIdSet.add(poli.PO__c);
		}
		
		List<PO__c> poList = new List<PO__c>();
		for(PO__c po : [SELECT Id, (SELECT Id, Customer_Comment__c FROM PO_Line_Items__r) FROM PO__c WHERE Id IN :poIdSet])
		{
			List<String> commentList = new List<String>();
			for(PO_Line_Item__c poli : po.Po_Line_Items__r)
			{ 
				if(poli.Customer_Comment__c != null && poli.Customer_Comment__c != ''){
					commentList.add(poli.Customer_Comment__c);
				}
			}
			po.Comments__c = String.join(commentList, '; ');
			poList.add(po);
		}
		
		return poList;
	}
    
}
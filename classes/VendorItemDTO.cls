global with sharing class VendorItemDTO {
    
	@AuraEnabled
	global 	String  	Id 						{get; set;}

	@AuraEnabled
	global 	String 	    Name 					{get; set;}

	@AuraEnabled
	global 	String 	    ItemNumber				{get; set;}

	@AuraEnabled
	global 	Decimal 	DealPack				{get; set;}

	@AuraEnabled
	global 	Decimal 	EachCost				{get; set;}

	@AuraEnabled
	global 	Decimal 	DealCost				{get; set;}

	@AuraEnabled
	global 	Integer	    QuantityOrder 			{get; set;}

	@AuraEnabled
	global 	Date 	    DeliveryDate			{get; set;}

	@AuraEnabled
	global 	String 	    ShipTo 					{get; set;}

	@AuraEnabled
	global 	String 	    Notes 					{get; set;}

	@AuraEnabled
	global 	Boolean 	Favorite				{get; set;}
}
public class SMR_CommunityEventDetailCtrl {

    @AuraEnabled
    public static String getAccountEvent() {
        try{
            List<User> lstUser = [Select u.ContactId from User u where u.Id = : UserInfo.getUserId()];
            if(lstUser[0].contactID!=null){
                Id contactb = lstUser[0].contactID;
                list<contact> lstContactinfo = [select Id,AccountId from Contact where Id = :contactb LIMIT 1];
                 List<Account> Accountlistinfo=[select id,Name,(select id,Name from Event__r) from Account where Id=:lstContactinfo[0].AccountId];
                if(Accountlistinfo[0].Event__r!=null){
                    Id EventRecordid=Accountlistinfo[0].Event__r[0].id;
                    system.debug('RTRRR'+EventRecordid);
                    return EventRecordid;
                }
                return null;
            }
            return null;
        }catch(DmlException ex){
            // throw new AuraHandledException('User-defined error');
            throw new AuraHandledException(ex.getMessage());
        }
    }
}
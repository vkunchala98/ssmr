/** Class Name : EmailCentralBillCustomerPOsCtrl
*  Description  : Apex class for EmailCentralBillCustomerPOs_Lightning Button. 
*  Created By   : BTG 
*  Created On   : 21st Feb 2019
*
*  Modification Log:  
*  --------------------------------------------------------------------------------------------------------------------------------------
*   Developer                Date                   Modification ID      Description 
*  ---------------------------------------------------------------------------------------------------------------------------------------
*                                                                                    
**/


public class EmailCentralBillCustomerPOsCtrl{

     private ApexPages.StandardSetController standardController;
    
    public String erroMsg {set;get;}
    
    public EmailCentralBillCustomerPOsCtrl(ApexPages.StandardSetController standardController) {
        this.standardController = standardController;
        
    }
          public PageReference doSubmit()
          {       
        List<PO__c> selectedPO= (List<PO__c>) standardController.getSelected();
        if(selectedPO.size()>0){
            //callout
            
            String centralcusPodrId=System.Label.Central_Customer_PO;
            String deliveryOp=System.Label.Central_Customer_PO_Delivery;
            String sfdcBaseURL = URL.getSalesforceBaseUrl().toExternalForm();
            System.debug('Base URL: ' + sfdcBaseURL );
         
            String RecIds='';
            for (PO__c sObjectId : selectedPO) {
                         RecIds+=sObjectId.id+',';
            }
            RecIds = RecIds.removeEnd(',');
            system.debug('>>>>'+RecIds);
            PageReference redirectPage = Page.loop__masslooplus;
            redirectPage.setRedirect(true);
            redirectPage.getParameters().put('retURL', sfdcBaseURL);
            redirectPage.getParameters().put('recordIds',RecIds);
            redirectPage.getParameters().put('sessionId',userInfo.getSessionId() );
            redirectPage.getParameters().put('contactField','Customer_Email_Contact_ID__c');
            redirectPage.getParameters().put('hidecontact','true');
            redirectPage.getParameters().put('hideddp','true');
            redirectPage.getParameters().put('autorun','true');
            redirectPage.getParameters().put('attach','true');
            redirectPage.getParameters().put('ddpIds',centralcusPodrId);
            redirectPage.getParameters().put('deploy',deliveryOp);
          
            //contactField=Customer_Email_Contact_ID__c
            return redirectPage;
            
            
        }else{
            erroMsg ='error';
        }
        return null;
    }
          
}
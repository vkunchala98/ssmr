public class StoreOrderController{

    public List<Order_Line_Items__c> orderLineItemList {get;set;}
    public List<Store_Order__c> storeOrderList {get;set;}
    public List<Account> accList {get;set;}
    public List<OrderLineItem_ShipTo__c> orderlineItemShipToList {get;set;}
    public Id storeOrderId {get;set;}
    public String accId {get;set;}
    public String accName {get;set;} 
    public String accHeadingName {get;set;}
    public String billType {get;set;}
    public Store_Order__c storeOrder {get;set;}
    public String billingAddress {get;set;}
    public String shippingAddress {get;set;}
    public Boolean multipleShipTo {get;set;}
    public Boolean multipleBillTo {get;set;}
    
     public class jsonResultCls{
    
        public string Id {get;set;}                              
        public String successMsg {get;set;} 
    }
    
    public StoreOrderController() {
    
        multipleShipTo = false;
        multipleBillTo = false;
        accList = new  List<Account>();
        orderLineItemList = new List<Order_Line_Items__c>();
        storeOrderList = new List<Store_Order__c>();
        orderlineItemShipToList  = new List<OrderLineItem_ShipTo__c>();
        accId = Apexpages.currentpage().getparameters().get('Id');
        Set<Id> orderlineItemIdSet = new Set<Id>();
        if (accId.startsWith('001')) {
            accList = new  List<Account>();
            accList = [SELECT Id,Name,Direct_or_Central_Bill__c,Email_Contact__c
                       FROM Account
                       WHERE Id = :accId];
            if(accList[0].Direct_or_Central_Bill__c != Null || accList[0].Direct_or_Central_Bill__c != '') 
                billType = accList[0].Direct_or_Central_Bill__c ;
            else 
                billType = '';
            accHeadingName = accList[0].Name;
            accName = String.escapeSingleQuotes(accList[0].Name);
            
        } else {
           
            storeOrderId = Apexpages.currentpage().getparameters().get('Id');
            for(Order_Line_Items__c st: [SELECT Id,Name,Item_Number__c,Cost_per_Deal__c,Quantity__c,Bill_To__c,Ship_To__c,Store_Product__c,
                                                Store_Product__r.Vendor__r.Name,Store_Product__r.Vendor__c,Bill_To__r.Name,Ship_To__r.Name,
                                                Vendor__c,Total__c,Discount__c,Store_Product__r.Name,Store_Order__c, 
                                                Shipping_Date__c,Store_Product__r.Item__c,Arrival_Date__c,Attention__c  
                                         FROM Order_Line_Items__c
                                         WHERE Store_Order__c =:storeOrderId ]) {
                  
                  st.Store_Product__r.Name =String.escapeSingleQuotes(st.Store_Product__r.Name) ; 
                  st.Store_Product__r.Name =st.Store_Product__r.Name.replaceAll('\"','&quot;') ;  
                  st.Store_Product__r.Item__c =String.escapeSingleQuotes(st.Store_Product__r.Item__c) ;  
                  st.Store_Product__r.Item__c =st.Store_Product__r.Item__c.replaceAll('\"','&quot;') ;
                  st.Store_Product__r.Vendor__r.Name=String.escapeSingleQuotes(st.Store_Product__r.Vendor__r.Name) ;  
                  st.Store_Product__r.Vendor__r.Name =st.Store_Product__r.Vendor__r.Name.replaceAll('\"','&quot;') ; 
                  if(st.Ship_To__c != null) {
                      st.Ship_To__r.Name =String.escapeSingleQuotes(st.Ship_To__r.Name) ;  
                      st.Ship_To__r.Name =st.Ship_To__r.Name.replaceAll('\"','&quot;') ; 
                  } 
                  if(st.Bill_To__c != null) {
                      st.Bill_To__r.Name =String.escapeSingleQuotes(st.Bill_To__r.Name) ;
                      st.Bill_To__r.Name =st.Bill_To__r.Name.replaceAll('\"','&quot;') ;  

                  } 
                  if(st.Attention__c  != null) {
                      st.Attention__c   =String.escapeSingleQuotes(st.Attention__c  ) ;
                      st.Attention__c   =st.Attention__c.replaceAll('\"','&quot;') ;  

                  } 
                  if(st.Quantity__c == null)
                      st.Quantity__c = 1;
                  orderLineItemList.add(st);      
                  orderlineItemIdSet.add(st.Id);
                                 
            }
            if(orderlineItemIdSet.size() > 0) {
                for(OrderLineItem_ShipTo__c oShip: [SELECT Id,Account__c,Account__r.Name,Order_Line_Item__c 
                                                    FROM OrderLineItem_ShipTo__c
                                                    WHERE Order_Line_Item__c IN :orderlineItemIdSet] ){
                    oShip.Account__r.Name = String.escapeSingleQuotes(oShip.Account__r.Name);
                    orderlineItemShipToList.add(oShip);
                }
            }
            
            storeOrderList = [SELECT ID,Name,Account__c,Account__r.Name,Account__r.Direct_or_Central_Bill__c,Multiple_Bill_To__c,Multiple_Ship_To__c,Bill_Types__c
                              FROM Store_Order__c 
                              WHERE Id= :storeOrderId ];
            if(storeOrderList[0].Multiple_Bill_To__c != null) 
                multipleBillTo = storeOrderList[0].Multiple_Bill_To__c;
            else 
                multipleBillTo = false;
            if(storeOrderList[0].Multiple_Ship_To__c!= null)
                multipleShipTo = storeOrderList[0].Multiple_Ship_To__c;
            else 
                multipleShipTo = false;
            billType = storeOrderList[0].Bill_Types__c;
            accHeadingName = storeOrderList[0].Account__r.Name;
            accName = String.escapeSingleQuotes(storeOrderList[0].Account__r.Name) ;
            accName = accName.replaceAll('&quot;','"') ;
            accId = storeOrderList[0].Account__c;
        }
    }
    @RemoteAction
    public static String vendorList() {
    
        List<Account> vendorList = [SELECT id,Name 
                                        FROM Account
                                        WHERE RecordTypeId IN (SELECT Id FROM RecordType WHERE DeveloperName = 'Vendor' AND SobjectType ='Account')
                                        AND Active__c = 'Yes'
                                    ]; 
        String vendorName = JSON.serialize(new List<Account>());
        if(vendorList.size()>0){
            for(Account acc : vendorList)
                acc.name = String.escapeSingleQuotes(acc.Name);
            vendorName = JSON.serialize(vendorList);
            system.debug('-----vendorName--------'+vendorName);
        }
        return vendorName;
    }
    
    @RemoteAction
    public static String itemList(String accountId) {
        List<RecordType> recordTypeList = [SELECT Id FROM RecordType WHERE DeveloperName = 'Vendor' AND SobjectType ='Account'];
        Id vendorRecordTypeId;
        if(recordTypeList.size() >0)vendorRecordTypeId = recordTypeList[0].id;
        List<Item__c> itemList = [SELECT Id,Name,Item__c,Cost_per_Deal__c,Suggested_Retail_Price__c,
            Vendor__c,Vendor__r.Name,Item_Status__c 
            FROM Item__c 
            WHERE Vendor__c IN (SELECT id FROM Account WHERE RecordTypeId =:vendorRecordTypeId)
            AND  Vendor__r.Active__c = 'Yes' AND Item_Status__c = 'Active'                 
        ];
        //Item_Status__c = 'Active' AND
        String itemDetails = JSON.serialize(new List<Item__c>());
        if(itemList.size() > 0){
            for(Item__c item : itemList){
                item.Name = String.escapeSingleQuotes(item.Name);
                item.Item__c = String.escapeSingleQuotes(item.Item__c);
            }
            itemDetails = JSON.serialize(itemList);
            system.debug('-----itemDetails--------'+itemDetails);
        }
        return itemDetails;
    }
    
    @RemoteAction
    public static String itemListPage(Integer pageNumber)
    {
        List<RecordType> recordTypeList = [SELECT Id FROM RecordType WHERE DeveloperName = 'Vendor' AND SobjectType ='Account'];
        Id vendorRecordTypeId;
        if(recordTypeList.size() >0){
        	vendorRecordTypeId = recordTypeList[0].Id;
        }
        
        Integer pageSize = 5000;
        Integer startNum = pageNumber * pageSize;
        List<Item__c> itemList = [SELECT Id, Name, Item__c, Cost_per_Deal__c, Suggested_Retail_Price__c,
						            	Vendor__c, Vendor__r.Name, Item_Status__c 
						            FROM Item__c 
						            WHERE Vendor__c IN (SELECT Id 
						            					FROM Account 
						            					WHERE RecordTypeId =:vendorRecordTypeId)
						            AND  Vendor__r.Active__c = 'Yes' 
						            AND Item_Status__c = 'Active'
						            ORDER BY Id
						        ];
        
        List<Item__c> itemPageList = new List<Item__c>();
        for(Integer ii=startNum; ii < itemList.size() && ii < (startNum + pageSize); ++ii)
        {
        	Item__c item = itemList[ii];
            item.Name = String.escapeSingleQuotes(item.Name);
            item.Item__c = String.escapeSingleQuotes(item.Item__c);
        	itemPageList.add(item);
        }
        
        String itemDetails = JSON.serialize(itemPageList);
        
        return itemDetails;
    }
    
   

    @RemoteAction
    public static String deleteOrderLineItem(String OrderLineItemId) {
        String result ='';
        try{
            Delete [SELECT ID FROM Order_Line_Items__c WHERE Id = :OrderLineItemId];
            result = 'Success';
        } catch(Exception e) {
            result = 'Fail';
        }
        return result;
    
    }
    
    @RemoteAction
    public static String customerList(String accIds) {
        
        List<Account> customerChildList = [SELECT id,Name 
                                           FROM Account
                                           WHERE ParentId =:accIds AND 
                                                 RecordTypeId IN (SELECT Id FROM RecordType WHERE DeveloperName = 'Customer' AND SobjectType ='Account')
                                          ];
        List<Account> currentCustomerList = [SELECT Id,Name 
                                             FROM Account
                                             WHERE RecordTypeId IN (SELECT Id FROM RecordType WHERE DeveloperName = 'Customer' AND SobjectType ='Account')
                                                 AND Id =:accIds ];
        if(customerChildList.size() > 0) currentCustomerList.addAll(customerChildList);
            
        String customerName = JSON.serialize(new List<Account>()); 
         
        if(currentCustomerList.size()>0){
            for(Account acc : currentCustomerList)
                acc.Name = String.escapeSingleQuotes(acc.Name);
            customerName = JSON.serialize(currentCustomerList);
        }
        system.debug('-----customerName--------'+customerName);
        
        return customerName;
    
    }
    
    @RemoteAction
    public static string createUpdateStoreOrderAndLineItems(String lineItems) {
    
        Savepoint sp = Database.setSavepoint();
        
        Map<String,Object> deserialisedStoreOrderAndLineItems = (Map<String, Object>)JSON.deserializeUntyped(lineItems);
        List<Object> storeOrderList = (List<Object>)deserialisedStoreOrderAndLineItems.get('StoreOrder');
        List<OrderLineItem_ShipTo__c> OrderlineShipList = new List<OrderLineItem_ShipTo__c>();
        Map<String,Object> storeOrderListValue = (Map<String,Object>)storeOrderList[0];
        Map<Integer,String> rowIndexAndShipToIdMap = new Map<Integer,String>();
        Map<Integer,Order_Line_Items__c> rowIndexAndOrderLineItemMap = new Map<Integer,Order_Line_Items__c>();
        Map<Id,List<OrderLineItem_ShipTo__c>>shipToOrderLineItemsIdsMap = new  Map<Id,List<OrderLineItem_ShipTo__c>>();
        Map<string,List<Order_Line_Items__c>>groupingOLIMapList = new Map<string,List<Order_Line_Items__c>>();

        List<OrderLineItem_ShipTo__c> OLIShipToList = new List<OrderLineItem_ShipTo__c>();
        List<PO_Line_Item__c> poLineItemList = new List<PO_Line_Item__c>();
        List<PO__c> poList = new List<PO__c>();
        Map<string,string> shipToIds = new Map<string,string>();
        
        Set<Id> orderLineItemIdExistingSet = new Set<Id>();
        Integer totalRow = 0;
        List<jsonResultCls> jsonResultClsArray = new List<jsonResultCls>();
        jsonResultCls createdNewJson = new jsonResultCls();
        Id storeId ;
        String storesID;
        Boolean updateStoreOrderFlag = false;
        Store_Order__c storeOrder = new Store_Order__c();
        string jsonResult = '';
        
        List<Account>billToList = new List<Account>();
        
        try                                                      
        {               
            if(storeOrderListValue.get('Account__c') != Null && storeOrderListValue.get('Account__c') != '') {
                
                storeOrder.Account__c = String.valueOf(storeOrderListValue.get('Account__c'));
                storeOrder.Multiple_Ship_To__c = Boolean.valueOf(String.valueOf(storeOrderListValue.get('Multiple_Ship_To__c')));
                storeOrder.Multiple_Bill_To__c = Boolean.valueOf(String.valueOf(storeOrderListValue.get('Multiple_Bill_To__c')));
                storeOrder.Bill_To__c = String.valueOf(storeOrderListValue.get('Bill_To__c'));
                if(storeOrder.Bill_To__c != null){
                    billToList = [SELECT Id,Name,Email_Contact__c,OwnerId FROM Account WHERE Id = :storeOrder.Bill_To__c];
                    if(billToList != null && billToList.size()>0)storeOrder.Customer_Contact__c = billToList[0].Email_Contact__c;
                }
                if(storeOrderListValue.get('StoreId') != Null && storeOrderListValue.get('StoreId') != '') {
                    storeOrder.Id = String.valueOf(storeOrderListValue.get('StoreId'));
                    updateStoreOrderFlag = true;
                }
                //storeOrder.Sales_Rep__c = UserInfo.getUserId();
                if(billToList != null && billToList.size()>0)storeOrder.Sales_Rep__c = billToList[0].OwnerId;
                if(storeOrderListValue.get('Bill_Type__c') != Null && storeOrderListValue.get('Bill_Type__c') != '')
                    storeOrder.Bill_Types__c= String.valueOf(storeOrderListValue.get('Bill_Type__c'));
                else 
                    storeOrder.Bill_Types__c = 'Direct Bill';
                upsert storeOrder;
                storeId = storeOrder.Id;
            } 
            List<Order_Line_Items__c> OLIList = new List<Order_Line_Items__c>();
            List<Object> OLILists = (List<Object>)deserialisedStoreOrderAndLineItems.get('OrderLineItem');
            Map<string,Account> accountIdsWithMaps = new Map<string,Account>();
            Set<string> accountIds = new Set<string>();
            Map<string,OrderLineItem_ShipTo__c> shipToMap = new Map<string,OrderLineItem_ShipTo__c>();
            Map<Id,List<Order_Line_Item_Component__c>> OLILineItemComMap = new Map<Id,List<Order_Line_Item_Component__c>>();
            
            Set<Id> itemIds = new Set<Id>();
            for(Integer i=0;i<OLILists.size();i++) {
                
                Map<String,Object> OLIListValue = (Map<String,Object>)OLILists[i];
                
                Order_Line_Items__c OLI = new Order_Line_Items__c();
                if(OLIListValue.get('Id') != null && OLIListValue.get('Id') != '') {
                    OLI.Id = String.valueOf(OLIListValue.get('Id'));
                    orderLineItemIdExistingSet.add(String.valueOf(OLIListValue.get('Id')));
                }
                if(String.valueOf(OLIListValue.get('At_Once__c')) != null) OLI.At_Once__c = Boolean.valueOf(String.valueOf(OLIListValue.get('At_Once__c')));
                
                if(String.valueOf(OLIListValue.get('Bill_To__c')) != null) OLI.Bill_To__c = String.valueOf(OLIListValue.get('Bill_To__c'));
                //if(String.valueOf(OLIListValue.get('Ship_To__c')) != '')OLI.Ship_To__c = String.valueOf(OLIListValue.get('Ship_To__c'));
                if(String.valueOf(OLIListValue.get('Cost_per_Deal__c')) != null) OLI.Cost_per_Deal__c = Decimal.valueOf(String.valueOf(OLIListValue.get('Cost_per_Deal__c')));
                if(OLIListValue.get('Total_Deal_Cost__c') != null && OLIListValue.get('Total_Deal_Cost__c') != '')OLI.Total_Deal_Cost__c = Decimal.valueOf(String.valueOf(OLIListValue.get('Total_Deal_Cost__c')));
                if(OLIListValue.get('Discount__c') != null && OLIListValue.get('Discount__c') != '') 
                    OLI.Discount__c = Decimal.valueOf(String.valueOf(OLIListValue.get('Discount__c')));
                else                                  
                    OLI.Discount__c = null;
               
                if(String.valueOf(OLIListValue.get('Item__c')) != null)OLI.Item__c = String.valueOf(OLIListValue.get('Item__c'));
                if(String.valueOf(OLIListValue.get('Item_Number__c')) != null)OLI.Item_Number__c = String.valueOf(OLIListValue.get('Item_Number__c'));
                if(String.valueOf(OLIListValue.get('Quantity__c')) != null)OLI.Quantity__c = Decimal.valueOf(String.valueOf(OLIListValue.get('Quantity__c')));
                
                if(storeId != Null) {
                    OLI.Store_Order__c = storeId ;
                    storesID = storeId ;
                } else {
                    OLI.Store_Order__c = String.valueOf(OLIListValue.get('Store_Order__c'));
                    storesID = String.valueOf(OLIListValue.get('Store_Order__c'));
                }
                 if(String.valueOf(OLIListValue.get('ItemId')) != null)OLI.Store_Product__c = String.valueOf(OLIListValue.get('ItemId'));
                itemIds.add(OLI.Store_Product__c);
               
                if (OLIListValue.get('Shipping_Date__c') != Null && OLIListValue.get('Shipping_Date__c') != '')
                    OLI.Arrival_Date__c= date.parse(String.valueOf(OLIListValue.get('Shipping_Date__c')));
                else 
                    OLI.Arrival_Date__c= Null;

                    rowIndexAndShipToIdMap.put(totalRow,String.valueOf(OLIListValue.get('Ship_To__c')));
                rowIndexAndOrderLineItemMap.put(totalRow,OLI);
               //accountIds.add(String.valueOf(OLIListValue.get('Ship_To__c')));
                totalRow++;
            }
            
          
            Map <Id, Item__c> addedItemList = new Map <Id, Item__c>([select id,Case_UPC__c,Pack_Per_Deal__c,
                Type_of_display__c,Suggested_Retail_Price__c 
                from Item__c where id IN:itemIds
            ]);
            
            for(Order_Line_Items__c oli : rowIndexAndOrderLineItemMap.values()){
                if(addedItemList.get(oli.Store_Product__c) != null){
                    oli.Case_UPC__c = addedItemList.get(oli.Store_Product__c).Case_UPC__c;
                    oli.Deal_Pack__c = addedItemList.get(oli.Store_Product__c).Pack_Per_Deal__c;
                    oli.Type_of_Display__c = addedItemList.get(oli.Store_Product__c).Type_of_Display__c;
                    //Original_Deal_Cost__c
                }
            }              
            
            //accountIdsWithMaps = new Map<string,Account>([select id,Name from Account where id IN:accountIds]);
            List<Item_Component__c> itemComponentList = new List<Item_Component__c>([select id,Name,Item__c,Cost_Per_Deal__c,Unit_Cost__c,Quantity__c,Suggested_Retail__c,Item_UPC__c   
                from Item_Component__c where Item__c IN:itemIds
            ]);
            Map<string,List<Item_Component__c>> itemComponentMap = new Map<string,List<Item_Component__c>>();
            
            for(Item_Component__c o : itemComponentList){            
                if(itemComponentMap.containsKey(o.Item__c))
                        itemComponentMap.get(o.Item__c).add(o);                 
                else
                    itemComponentMap.put(o.Item__c,new List<Item_Component__c>{o});
            } 
            
            Map<Id,Order_Line_Items__c> oliMaps = new Map<Id,Order_Line_Items__c>();
            
            if(rowIndexAndOrderLineItemMap.values().size() > 0) 
                upsert rowIndexAndOrderLineItemMap.values();
            
            
            for(OrderLineItem_ShipTo__c oShips: [SELECT Id 
                                                FROM OrderLineItem_ShipTo__c
                                                WHERE Order_Line_Item__c IN :orderLineItemIdExistingSet]) {
                OrderlineShipList.add(oShips);
            }
            if(OrderlineShipList.size() > 0)Delete OrderlineShipList;
            
            for(Integer rowIndex : rowIndexAndOrderLineItemMap.keySet()) {
                if(rowIndexAndShipToIdMap.get(rowIndex).contains('~%!')) { //multiple accounts shipto
                    List<String> accIdList = rowIndexAndShipToIdMap.get(rowIndex).split('~%!');
                    for(String accId : accIdList) {
                        OrderLineItem_ShipTo__c OLIShipTo =new OrderLineItem_ShipTo__c();
                        OLIShipTo.Order_Line_Item__c = rowIndexAndOrderLineItemMap.get(rowIndex).Id;
                        OLIShipTo.Account__c = accId;
                        accountIds.add(accId);
                        //OLIShipTo.Name = accountIdsWithMaps.get(accId).Name;
                        OLIShipToList.add(OLIShipTo);
                    }
                } else {//single account shipto
                    OrderLineItem_ShipTo__c OLIShipTo =new OrderLineItem_ShipTo__c();
                    OLIShipTo.Order_Line_Item__c = rowIndexAndOrderLineItemMap.get(rowIndex).Id;
                    OLIShipTo.Account__c = rowIndexAndShipToIdMap.get(rowIndex);
                    accountIds.add(rowIndexAndShipToIdMap.get(rowIndex));
                    //OLIShipTo.Name = accountIdsWithMaps.get(rowIndexAndShipToIdMap.get(rowIndex)).Name;
                    OLIShipToList.add(OLIShipTo);
                }
            }
            accountIdsWithMaps = new Map<string,Account>([select id,Name from Account where id IN:accountIds]);
            for(OrderLineItem_ShipTo__c o : OLIShipToList)
                if(accountIdsWithMaps.get(o.Account__c)!= null) o.Name = accountIdsWithMaps.get(o.Account__c).Name;



            System.debug('OLIShipToList: ' + OLIShipToList);         
            if(OLIShipToList.size() > 0)upsert OLIShipToList;
               
            for(OrderLineItem_ShipTo__c o : OLIShipToList){
                if(shipToOrderLineItemsIdsMap.containsKey(o.Order_Line_Item__c))
                        shipToOrderLineItemsIdsMap.get(o.Order_Line_Item__c).add(o);
                else
                    shipToOrderLineItemsIdsMap.put(o.Order_Line_Item__c,new List<OrderLineItem_ShipTo__c>{o});
            }
                
                                                        
            List<PO__c> po_list = [SELECT ID FROM PO__c WHERE Store_Order__c =:storesID];
            if(po_list.size() > 0)
                //Delete po_list;
            set<String> shipToGroupingsIds = new set<String>();
            if(updateStoreOrderFlag != true) {
                List<Order_Line_Item_Component__c> OLILineItemComList = new List<Order_Line_Item_Component__c>();
                List<Order_Line_Items__c> soOliList = [SELECT Id, Name, Total__c, Store_Product__c, Store_Product__r.Vendor__r.Id,Store_Order__c,Bill_To__c,
                                                         Cost_per_Deal__c,Discount__c,Item__c,Item_Number__c,Quantity__c,Ship_To__c,
                                                         Type_of_Display__c,Arrival_Date__c,At_Once__c,Case_UPC__c,
                                                         Deal_Pack__c,Original_Deal_Cost__c
                                                      FROM Order_Line_Items__c 
                                                      WHERE Store_Order__c = :storesID];
                             
                                        
                for(Order_Line_Items__c OLIitems : soOliList ){
                    
                    // code for creating Order Line Item Components
                    if(itemComponentMap.get(OLIitems.Store_Product__c) != null){
                        List<Item_Component__c> oliComListRec = itemComponentMap.get(OLIitems.Store_Product__c);
                        for(Item_Component__c o : oliComListRec){
                            o.Unit_Cost__c = (o.Unit_Cost__c != null)? o.Unit_Cost__c : 0;
                            decimal quantityDeduct = (OLIitems.Discount__c != null) ? (OLIitems.Discount__c * o.Unit_Cost__c)/100 : 0;
                            decimal dealCost  = o.Unit_Cost__c - quantityDeduct;
                            OLILineItemComList.add(new Order_Line_Item_Component__c(Quantity__c = o.Quantity__c,Item_Component_UPC__c = o.Item_UPC__c,
                                Unit_Cost__c = dealCost,Suggested_Retail__c = o.Suggested_Retail__c,Order_Line_Item__c = OLIitems.id,Item_Component_Description__c = o.Name
                            ));            
                        }                    
                    }                                 
                                                           
                    List<OrderLineItem_ShipTo__c> oliShipToListRec = new List<OrderLineItem_ShipTo__c>();
                    
                    if(shipToOrderLineItemsIdsMap.get(OLIitems.id) != null){
                        oliShipToListRec = shipToOrderLineItemsIdsMap.get(OLIitems.id);
                        for(OrderLineItem_ShipTo__c opiShip : oliShipToListRec){
                            System.debug('opiShip: ' + opiShip);
                            string datesY = '';              
                            if(OLIitems.Arrival_Date__c != null)
                                datesY = String.valueOf(OLIitems.Arrival_Date__c.year())+'-'+String.valueOf(OLIitems.Arrival_Date__c.month())+'-'+ String.valueOf(OLIitems.Arrival_Date__c.day());
                            string vendorId = (OLIitems.Store_Product__r.Vendor__r.Id != null) ? OLIitems.Store_Product__r.Vendor__r.Id : '';
                            string shipTo = (opiShip.Account__c  != null) ? opiShip.Account__c : '';
                            string combinedIds = vendorId+datesY+shipTo; 
                            shipToIds.put(combinedIds,shipTo);  
                            Decimal amt =  0;
                            if(groupingOLIMapList.containsKey(combinedIds)){
                                groupingOLIMapList.get(combinedIds).add(OLIitems);
                            }else{
                                groupingOLIMapList.put(combinedIds,new List<Order_Line_Items__c>{OLIitems});
                                shipToMap.put(combinedIds,opiShip);
                            }
                        }   
                    }
                }
                if(OLILineItemComList.size()>0) insert OLILineItemComList;
                
                
                for(Order_Line_Item_Component__c o : OLILineItemComList){
                    if(OLILineItemComMap.containsKey(o.Order_Line_Item__c)){
                        OLILineItemComMap.get(o.Order_Line_Item__c).add(o);
                    }else{
                        OLILineItemComMap.put(o.Order_Line_Item__c,new List<Order_Line_Item_Component__c>{o});
                    }
                }
                    
                
                integer indexOLI = 0;
                Map<integer,string> poLOIOrderMap = new Map<integer,string>();
                Map<string,PO__c> poIds = new Map<string,PO__c>();
               
                for(string o : groupingOLIMapList.keyset()){
                    PO__c po = new PO__c();
                    if(groupingOLIMapList.get(o) != null){
                        List<Order_Line_Items__c> oliNewLists = groupingOLIMapList.get(o);
                        Decimal amt =  0;
                       
                        po.Bill_To__c = oliNewLists[0].Bill_To__c;
                        if(shipToMap.get(o) != null) po.Ship_To__c = shipToMap.get(o).Account__c;
                        if(shipToIds.get(o)!= null)po.Ship_To__c = shipToIds.get(o);
                        po.Account__c = oliNewLists[0].Store_Product__r.Vendor__r.Id;
                        po.At_Once__c = oliNewLists[0].At_Once__c;
                        po.Arrival_Date__c = oliNewLists[0].Arrival_Date__c;
                        po.Store_Order__c = storesID;
                        if(billToList != null && billToList.size()>0)po.Sales_Rep__c = billToList[0].OwnerId;
                        poList.add(po);
                        poLOIOrderMap.put(indexOLI,o);
                        indexOLI++;
                    }
                }
               
                if(poList.size() > 0) insert poList;
                integer poCount = 0;
                for(PO__c po : poList){
                    poIds.put(poLOIOrderMap.get(poCount),po);
                    poCount++;
                }
               
                integer indexPLI = 0;
                Map<integer,string> insertedPLIMap = new Map<integer,string>();
                for(string o : groupingOLIMapList.keyset()){               
                    if(groupingOLIMapList.get(o) != null){
                        for(Order_Line_Items__c ordLinItem : groupingOLIMapList.get(o)){
                                if(groupingOLIMapList.get(o) != null && poIds.get(o) != null){
                                    PO__c pos = poIds.get(o);  
                                        poLineItemList.add(new PO_Line_Item__c(PO__c = pos.Id,Store_Product__c = ordLinItem.Store_Product__c,
                                        Cost_per_Deal__c=ordLinItem.Cost_per_Deal__c,
                                        Discount__c=ordLinItem.Discount__c,Item_Number__c=ordLinItem.Item_Number__c,
                                        Quantity__c=ordLinItem.Quantity__c,Original_Deal_Cost__c = ordLinItem.Original_Deal_Cost__c,
                                        Store_Order__c=pos.Store_Order__c,Order_Line_Item__c=ordLinItem.Id,
                                        Case_UPC__c =ordLinItem.Case_UPC__c ,Deal_Pack__c = ordLinItem.Deal_Pack__c,
                                        Type_of_Display__c = ordLinItem.Type_of_Display__c
                                    ));
                                    insertedPLIMap.put(indexPLI,o);
                                    indexOLI++;
                                }
                        }
                    }                                 
                }
                
                if(poLineItemList.size() > 0)insert poLineItemList ;
                
                List<PO_Line_Item__c> updatedPLIList = new List<PO_Line_Item__c>([
                    select id,PO__c,Order_Line_Item__c,PO_ShipTo_Id__c from PO_Line_Item__c
                    where id IN:poLineItemList
                ]);
                Map<string,Id>pliWithOLIIds = new Map<string,Id>();
                for(PO_Line_Item__c po : updatedPLIList){
                    string oliId = (po.Order_Line_Item__c != null) ? po.Order_Line_Item__c : '';
                    string shipTo = (po.PO_ShipTo_Id__c != null) ? po.PO_ShipTo_Id__c : '';
                    string combinedIds = oliId+shipTo;
                    pliWithOLIIds.put(combinedIds,po.PO__c);
                }
                system.debug('-----pliWithOLIIds-------'+pliWithOLIIds);
                
                for(OrderLineItem_ShipTo__c o : OLIShipToList){
                    string oliId = (o.Order_Line_Item__c != null) ? o.Order_Line_Item__c : '';
                    string shipTo = (o.Account__c != null) ? o.Account__c : '';
                    string combinedIds = oliId+shipTo;
                    system.debug('----combinedIds-------'+combinedIds);
                    if(pliWithOLIIds.get(combinedIds) != null)o.PO__c = pliWithOLIIds.get(combinedIds);
                    system.debug('-----o--------'+o);
                }
                upsert OLIShipToList;
                
                // code for creating PO Line Item Components
                List<PO_Line_Item_Component__c>POLineItemComList = new List<PO_Line_Item_Component__c>();
               
                for(PO_Line_Item__c po : poLineItemList){
                    if(OLILineItemComMap.get(po.Order_Line_Item__c) != null){
                        List<Order_Line_Item_Component__c> oliComListRec = OLILineItemComMap.get(po.Order_Line_Item__c);
                        for(Order_Line_Item_Component__c o : oliComListRec){
                            POLineItemComList.add(new PO_Line_Item_Component__c(
                                Quantity__c = o.Quantity__c,
                                Item_Component_UPC__c = o.Item_Component_UPC__c,
                                Unit_Cost__c = o.Unit_Cost__c,
                                Suggested_Retail__c = o.Suggested_Retail__c,
                                PO_Line_Item__c = po.id,
                                Item_Component_Description__c = o.Item_Component_Description__c,
                                Order_Line_Item_Component__c = o.id
                            ));                 
                        }                    
                    }       
                }    
             
                if(POLineItemComList.size()>0) insert POLineItemComList; 
            } 
            jsonResult = storesID+'Success';
        }catch(Exception e)
        {
            Database.rollback(sp);
            jsonResult = 'Failure'+e.getMessage();
        }
        return jsonResult;
    }

    @RemoteAction
    public static String keepAlive(){
        return 'ok';
    }
}
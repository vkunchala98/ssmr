@isTest
public class AddDeleteControllerTest {
    static testMethod void saveMethodTest() {
        List<Account> accList = new List<Account>();
        List<Item__c> itemList = new List<Item__c>();
        List<Order_Line_Items__c> lineItemList = new List<Order_Line_Items__c>();
        
        Account aCustomer = new Account();
        aCustomer.Name = 'customerTest';
        aCustomer.Type = 'Customer';
        aCustomer.Direct_or_Central_Bill__c = 'Central Warehouse';        
        insert aCustomer;   
        
        Account aVendor1 = new Account();
        aVendor1.Name = 'VendorTest1';
        aVendor1.Type = 'Vendor';
        accList.add(aVendor1);   
        
        Account aVendor2 = new Account();
        aVendor2.Name = 'VendorTest2';
        aVendor2.Type = 'Vendor';
        accList.add(aVendor2);       
        
        insert accList;        
        
        Item__c item1V1 = new Item__c();
        item1V1.Item__c = '11';
        item1V1.Name = '11';
        item1V1.Cost_Per_Deal__c = 100.05;
        item1V1.Vendor__c = aVendor1.Id;
        itemList.add(item1V1);
        
        Item__c item2V1 = new Item__c();
        item2V1.Item__c = '21';
        item2V1.Name = '21';
        item2V1.Cost_Per_Deal__c = 200.05;
        item2V1.Vendor__c = aVendor1.Id;        
        itemList.add(item2V1);
            
        Item__c item1V2 = new Item__c();
        item1V2.Item__c = '12';
        item1V2.Name = '12';
        item1V2.Cost_Per_Deal__c = 300.35;
        item1V2.Vendor__c = aVendor2.Id;        
        itemList.add(item1V2);    
        
        Item__c item2V2 = new Item__c();
        item2V2.Item__c = '22';
        item2V2.Name = '22';
        item2V2.Cost_Per_Deal__c = 400.35;
        item2V2.Vendor__c = aVendor2.Id;        
        itemList.add(item2V2);
        
        insert itemList;
        
        for(Item__c anItem : itemList){
        	Order_Line_Items__c aNewLineItem = new Order_Line_Items__c();            
            aNewLineItem.Bill_To__c = aCustomer.Id;
            aNewLineItem.Quantity__c = 1;
            aNewLineItem.Total_Deal_Cost__c = anItem.Cost_Per_Deal__c;
            aNewLineItem.Store_Product__c = anItem.Id;                
            lineItemList.add(aNewLineItem);
        }
        
        List<PO_RT__c> pOrts = new List<PO_RT__c>();
        pOrts.add(new PO_RT__c(Name = 'Assorted', Id__c = aCustomer.Id));
        pOrts.add(new PO_RT__c(Name = 'Central Bill', Id__c = aCustomer.Id));
        pOrts.add(new PO_RT__c(Name = 'Direct Bill', Id__c = aCustomer.Id));
        insert pOrts;                              
                                      
		Test.startTest();
        AddDeleteController.saveStoreOrders(accList, lineItemList, aCustomer.Id, aCustomer.Direct_or_Central_Bill__c, aCustomer.Id);
		Test.stopTest();
        
        List<Store_Order__c> storeOrderList = [SELECT Id, Name, Total_Amount__c FROM Store_Order__c];		
		system.assertEquals(300.10, 300.10);
        system.assertEquals(700.70, 700.70);        
    }
}
public class Community_EventDetailCtrl {

    public Id EventRecordid{get;set;}
    public Event__c EventDetails{get;set;}
    public   Community_EventDetailCtrl(ApexPages.StandardController controller){
        try{
                 //EventRecordid='a0PM000000BynX5MAJ';

            List<User> lstUser = [Select u.ContactId from User u where u.Id = : UserInfo.getUserId()];
            if(lstUser[0].contactID!=null){
                Id contactb = lstUser[0].contactID;
                list<contact> lstContactinfo = [select Id,AccountId from Contact where Id = :contactb LIMIT 1];
                 List<Account> Accountlistinfo=[select id,Name,(select id,Name from Event__r) from Account where Id=:lstContactinfo[0].AccountId];
                if(Accountlistinfo[0].Event__r!=null){
                     EventRecordid= Accountlistinfo[0].Event__r[0].id;
                    system.debug('RTRRR'+EventRecordid);
           EventDetails=[select id,Name,Location__c,Start_Date__c,End_Date__c,Booth_Number__c,Booth_Name__c,Total_Attendees__c,Total_Hotel_Rooms_Need__c,Comm_Account_Name__c from Event__c where id=:EventRecordid];

                    
                }
                
            }
            
        }catch(DmlException ex){
            // throw new AuraHandledException('User-defined error');
            throw new AuraHandledException(ex.getMessage());
        }
    }
    public void SaveEvent(){
        Event__c event = new  Event__c(id=EventRecordid);
        event.Booth_Name__c=EventDetails.Booth_Name__c;
        Update event;
    }
}
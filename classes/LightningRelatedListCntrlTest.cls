@isTest
public class LightningRelatedListCntrlTest {
    @isTest
    static void starttest(){
        Id vendorRecordTypeId=Schema.SObjectType.Account.getRecordTypeInfosByName().get('Vendor').getRecordTypeId();
        Account portalAccount = new Account();
        PortalAccount.name = 'portalAccount';
        portalAccount.RecordTypeId=vendorRecordTypeId;
        insert portalAccount;
        Contact portalContact = new contact();
        portalContact.LastName = 'portalContact';
        portalContact.AccountId = portalAccount.Id;
        insert portalContact;
        profile p=[select id from profile where name='Show Vendor Community' limit 1];
        user u=new user();
        u.Email='dazeworks@gmail.com';
        u.ProfileId=p.id;
        u.UserName='dazeworks@gmail.com'; 
        u.Alias = 'GDS';
        u.TimeZoneSidKey='America/New_York';
        u.EmailEncodingKey='ISO-8859-1';
        u.LocaleSidKey='en_US';
        u.LanguageLocaleKey='en_US';
        u.ContactId = portalContact.Id; 
        u.PortalRole = 'Manager';
        u.FirstName = 'daze';
        u.LastName = 'works';
        insert u;
        Event__c e=new Event__c();
        e.Name='2019 GM Pet & Candy Show';
        e.Location__c='The Hilton Bonnet Creek';
        e.Start_Date__c=Date.Today();
        e.End_Date__c=Date.Today();
        e.Booth_Name__c='works';
        e.Account_Name__c=portalAccount.id;
        
        insert e;
        LightningRelatedWrap lr = new LightningRelatedWrap();

        system.runAs(u){
            LightningRelatedListCntrl.userEvent();
        }
        
    }
    
}
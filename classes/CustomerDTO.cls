global with sharing class CustomerDTO {
	
	@AuraEnabled
	global 	String 	Id 						{get; set;}

	@AuraEnabled
	global 	String 	Name 					{get; set;}
}
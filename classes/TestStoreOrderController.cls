@isTest
public class TestStoreOrderController{
	
	@TestSetup
	public static void setup()
	{
		TestUtilities.makeCustomSettings();
	}
	
	public static testMethod void StoreOrderController()
	{
		List<RecordType> recIdList = [SELECT Id,Name FROM RecordType WHERE DeveloperName = 'Customer' AND SobjectType ='Account'];
		Account acc = new Account();
		acc.Name = 'Test Account';
		acc.RecordTypeId = recIdList[0].Id;
		Insert acc;
		
		recIdList = [SELECT Id,Name FROM RecordType WHERE DeveloperName = 'Vendor' AND SobjectType ='Account'];
		Account acc1 = new Account();
		acc1.Name = 'Test Account';
		acc1.RecordTypeId = recIdList[0].Id;
		Insert acc1;
		
		Store_Order__c st = new Store_Order__c();
		st.Account__c = acc.Id;
		Insert st;
		
		Item__c it = new Item__c();
		it.Vendor__c = acc1.id;
		it.Item_Status__c = 'Active';
		it.Item_UPC__c = 'x-xxxxx-xxxxx-x';
		it.Name = 'testItem098';
		it.Item__c = 'testItem096';
		it.Cost_Per_Deal__c = 23;
		it.Suggested_Retail_Price__c = 20.98;
		it.Case_UPC__c = 'x-xxxxx-xxxxx-x';
		insert it;
		
		Item_Component__c itemComponent = new Item_Component__c();
		itemComponent.Name = 'testItem';
		itemComponent.Item__c = it.Id;
		itemComponent.Item_UPC__c = 'x-xxxxx-xxxxx-x';
		itemComponent.Quantity__c = 5;
		itemComponent.Unit_Cost__c = 20;
		itemComponent.Suggested_Retail__c = '12';
		insert itemComponent;
		
		Order_Line_Items__c oli= new Order_Line_Items__c();
		oli.Store_Order__c = st.Id;
		oli.Store_Product__c = it.Id;
		oli.Ship_To__c = acc.Id;
		oli.Bill_To__c =acc.Id;
		insert oli;
		
		Order_Line_Items__c oli1= new Order_Line_Items__c();
		oli1.Store_Order__c = st.Id;
		oli1.Store_Product__c = it.Id;
		insert oli1;
		
		//string oliJson ='{"StoreOrder":[{"Account__c":"'+acc.Id+'","Multiple_Ship_To__c":"true","Multiple_Bill_To__c":"false","StoreId":""}],"OrderLineItem":[{"Bill_To__c":"'+acc.Id+'","Ship_To__c":"'+acc.Id+'","Shipping_Date__c":"","Cost_per_Deal__c":"20","Discount__c":"30","Quantity__c":"1","Vendor__c":"'+acc1.Id+'","Suggested_Retail__c":"30","Item__c":"testItem","Item_Number__c":"123Trf","ItemId":"'+it.Id+'"}]}';
		//string oliJson = '{"StoreOrder":[{"Account__c":"'+acc.Id+'","Multiple_Ship_To__c":"false","Multiple_Bill_To__c":"false","StoreId":"","Bill_To__c":"'+acc.Id+'","Bill_Type__c":"Central Warehouse"}],"OrderLineItem":[{"Bill_To__c":"'+acc.Id+'","At_Once__c":"false","Ship_To__c":"'+acc.Id+'","Attention__c":"6","Shipping_Date__c":"04/23/2015","Cost_per_Deal__c":"50","Discount__c":"5","Quantity__c":"1","Vendor__c":"'+acc1.Id+'","Suggested_Retail__c":"5","Item__c":"Steel Pipe","Item_Number__c":"S001","ItemId":"'+it.Id+'","Id":"","Store_Order__c":""}]}';
		string oliJson = '{"StoreOrder":[{"Account__c":"'+acc.Id+'","Multiple_Ship_To__c":"false","Multiple_Bill_To__c":"false","StoreId":"","Bill_To__c":"'+acc.Id+'","Bill_Type__c":"Central Warehouse"}],"OrderLineItem":[{"Bill_To__c":"'+acc.Id+'","At_Once__c":"false","Ship_To__c":"'+acc.Id+'","Attention__c":"6","Shipping_Date__c":"04/23/2015","Cost_per_Deal__c":"50","Discount__c":"5","Quantity__c":"1","Vendor__c":"'+acc1.Id+'","Item__c":"Steel Pipe","Item_Number__c":"S001","ItemId":"'+it.Id+'","Id":"","Store_Order__c":""}]}';
		ApexPages.currentPage().getParameters().put('Id',acc.Id);
		StoreOrderController soc = new StoreOrderController();
		StoreOrderController.vendorList();
		StoreOrderController.itemList(acc1.Id);
		StoreOrderController.customerList(acc1.Id);
		StoreOrderController.createUpdateStoreOrderAndLineItems(oliJson);
		StoreOrderController.deleteOrderLineItem(oli1.id);
		//ApexPages.currentPage().getParameters().put('Id',st.Id);
		 // StoreOrderController soc1 = new StoreOrderController();
		
		ApexPages.currentPage().getParameters().put('Id',st.Id);
		StoreOrderController soc1 = new StoreOrderController();
		StoreOrderController.vendorList();
	}
	
}
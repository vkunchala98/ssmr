global with sharing class VendorDTO {

	@AuraEnabled
	global 	String 	Id 						{get; set;}

	@AuraEnabled
	global 	String 	Name 					{get; set;}

	@AuraEnabled
	global 	String 	BoothNumber 			{get; set;}


	public VendorDTO buildVendor(String vendorId, String vendorName){
		VendorDTO vendorDTO = new VendorDTO();

		vendorDTO.Id 	= vendorId;
		vendorDTO.Name 	= vendorName;

		return vendorDTO;
	}

	public static VendorDTO buildVendor(Account vendorSO){
		VendorDTO vendorDTO = new VendorDTO();

		vendorDTO.Id 			= vendorSO.Id;
		vendorDTO.Name 			= vendorSO.Name;
		//VendorDTO.BoothNumber 	= vendorSO.

		return vendorDTO;
	}

	public static List<VendorDTO> buildVendor(List<Account> listVendorSO){
		List<VendorDTO> listVendorDTO = new List<VendorDTO>();

		for(Account vendorSO : listVendorSO){
			VendorDTO vendorDTO = buildVendor(vendorSO);
			listVendorDTO.add(vendorDTO);
		}

		return(listVendorDTO);
	}
}
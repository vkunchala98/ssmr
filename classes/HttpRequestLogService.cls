public without sharing class HttpRequestLogService {
	
	// this is a wrapper class for the HTTP_Request_Log__c object that allows the custom object to be encapsulated with related functionality more like other classes
	public class HRL_Entry
	{
		private HTTP_Request_Log__c httpRequestLog;
		
		public HRL_Entry(String direction)
		{
			httpRequestLog = new HTTP_Request_Log__c();
			httpRequestLog.Direction__c = direction;
			if(direction == 'Outbound'){
				httpRequestLog.Sender__c = 'Salesforce';
			}
		}
		
		// take relevant data from an HttpRequest object and copy it to the HTTP_Request_Log__c record
		public void addRequestData(HttpRequest request)
		{
			this.setRequestMethod(request.getMethod());
			this.setRequestEndpoint(request.getEndpoint());
			this.setRequestBody(request.getBody());
		}
		
		// take relevant data from an HttpResponse object and copy it to the HTTP_Request_Log__c record
		public void addResponseData(HttpResponse response)
		{
			this.setResponseCode(response.getStatusCode());
			this.setResponseBody(response.getBody());
		}
		
		// set the HTTP response code
		public void setResponseCode(Integer responseCode){
			httpRequestLog.Response_Code__c = responseCode;
			if(responseCode >= 400){
				this.setRequestStatus('Failed');
			}
			else if(responseCode == 200){
				this.setRequestStatus('Succeeded');
			}
		}
		
		// set the response body
		public void setResponseBody(String responseBody){
			httpRequestLog.Response_Body__c = responseBody;
		}
		
		// set the HTTP request method
		public void setRequestMethod(String method){
			httpRequestLog.HTTP_Method__c = method;
		}
		
		// set the endpoint
		public void setRequestEndpoint(String endpoint){
			httpRequestLog.Recipient_Endpoint__c = endpoint;
		}
		
		// set the request header
		public void setRequestHeader(String header){
			httpRequestLog.Request_Header__c = header;
		}
		
		// set the request body
		public void setRequestBody(String body){
			httpRequestLog.Request_Body__c = body;
		}
		
		// set the request status
		public void setRequestStatus(String status){
			httpRequestLog.Status__c = status;
		}
		
		// return the HTTP_Request_Log__c record
		public HTTP_Request_Log__c getRawLog(){
			return httpRequestLog;
		}
		
		// replace the HTTP_Request_Log__c record being used
		public void setRawLog(HTTP_Request_Log__c newHttpRequestLog){
			this.httpRequestLog = newHttpRequestLog;
		}
	}
    
}
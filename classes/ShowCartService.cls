public with sharing class ShowCartService {
	
	/**
	 * Populate the sales rep field on the show cart using the bill to account value
	 */
	public static List<Show_Cart__c> setSalesRep(List<Show_Cart__c> showCartList)
	{
		// get the IDs of all the bill to accounts
		Set<Id> billToAccountIdSet = new Set<Id>();
		for(Show_Cart__c showCart : showCartList){
			billToAccountIdSet.add(showCart.Bill_To__c);
		}
		
		// build a map of bill to account IDs to their owner user IDs
		Map<Id, Id> billToIdToOwnerIdMap = new Map<Id, Id>();
		for(Account billToAccount : [SELECT Id, OwnerId FROM Account WHERE Id IN :billToAccountIdSet]){
			billToIdToOwnerIdMap.put(billToAccount.Id, billToAccount.OwnerId);
		}
		
		// populate the sales rep field with the ID of the owner of the bill to account
		for(Show_Cart__c showCart : showCartList){
			showCart.Sales_Rep__c = billToIdToOwnerIdMap.get(showCart.Bill_To__c);
		}
		
		return showCartList;
	}
	
	/**
	 * Take the list of show cart records and return a list of object wrappers containing the orders, order line items, order line item components, order line item ship to records, POs, PO line items, and PO line item components that should be created
	 */
	public static List<ShowCartService.CartOrder> convertToOrders(List<Show_Cart__c> showCartList)
	{
		List<ShowCartService.CartOrder> cartOrderList = new List<ShowCartService.CartOrder>();
		
		// get the show cart IDs and query for the cart and line item data needed
		Set<Id> showCartIdSet = new Set<Id>();
		for(Show_Cart__c showCart : showCartList){
			showCartIdSet.add(showCart.Id);
		}
		
		// build a map of item components by item ID
        Map<Id, List<Item_Component__c>> itemComponentListByItemIdMap = new Map<Id, List<Item_Component__c>>();
        for(Item_Component__c itemComponent : [SELECT Id, Name, Item__c, Cost_Per_Deal__c, Unit_Cost__c, Quantity__c, Suggested_Retail__c, Item_UPC__c FROM Item_Component__c])
        {            
            if(itemComponentListByItemIdMap.containsKey(itemComponent.Item__c)){
            	itemComponentListByItemIdMap.get(itemComponent.Item__c).add(itemComponent);
            }
            else{
                itemComponentListByItemIdMap.put(itemComponent.Item__c, new List<Item_Component__c>{itemComponent});
            }
        }
		
		// create the CartOrder records needed for converting to carts to orders
		for(Show_Cart__c showCart : [SELECT Id, Bill_To__c, Bill_Type__c, 
										Bill_To__r.OwnerId, Bill_To__r.Email_Contact__c, 
										(SELECT Id, Name, Bill_To__c, Ship_To__c, Ship_To__r.Name, Deal_Cost__c, Total__c, Discount_Percent__c, Discount_Dollar__c, Item__c, Item_Number__c, Quantity__c, Arrival_Date__c, 
											At_Once__c, Original_Deal_Cost__c, Customer_Comments__c, 
											Item__r.Case_UPC__c, Item__r.Pack_Per_Deal__c, Item__r.Type_of_Display__c, Item__r.Vendor__c, Item__r.Name 
										 FROM Show_Cart_Line_Items__r)
									 FROM Show_Cart__c 
									 WHERE Id IN :showCartIdSet])
		{
			ShowCartService.CartOrder cartOrder = new ShowCartService.CartOrder();
			
			cartOrder.showCart = showCart;
			
			// copy the cart data to an order record
			Store_Order__c storeOrder = new Store_Order__c();
            storeOrder.Account__c = showCart.Bill_To__c;
            storeOrder.Bill_To__c = showCart.Bill_To__c;
            storeOrder.Bill_Types__c = showCart.Bill_Type__c;
			
        	// TODO: move this to a trigger on Store_Order__c
        	storeOrder.Customer_Contact__c = showCart.Bill_To__r.Email_Contact__c;
        	storeOrder.Sales_Rep__c = showCart.Bill_To__r.OwnerId;
        	
        	cartOrder.storeOrder = storeOrder;
        	
        	// this map groups the line items by a compound key in order to construct multiple POs for each order
        	Map<String, List<CartOrderLineItem>> cartOrderLineItemListMap = new Map<String, List<CartOrderLineItem>>();
        	
			// copy the cart line item data to order line items
			cartOrder.cartOrderLineItemList = new List<CartOrderLineItem>();
			Map<Id, Id> itemIdToVendorIdMap = new Map<Id, Id>();
			for(Show_Cart_Line_Item__c showCartLineItem : showCart.Show_Cart_Line_Items__r)
			{
				// populate the vendor ID map
				itemIdToVendorIdMap.put(showCartLineItem.Item__c, showCartLineItem.Item__r.Vendor__c);
				
				CartOrderLineItem cartOrderLineItem = new CartOrderLineItem();
				Order_Line_Items__c orderLineItem = new Order_Line_Items__c();
				
            	orderLineItem.At_Once__c = showCartLineItem.At_Once__c;
                orderLineItem.Bill_To__c = showCartLineItem.Bill_To__c;
                orderLineItem.Ship_To__c = showCartLineItem.Ship_To__c;
                orderLineItem.Cost_per_Deal__c = showCartLineItem.Deal_Cost__c;
                orderLineItem.Total_Deal_Cost__c = showCartLineItem.Total__c;
                orderLineItem.Discount__c = showCartLineItem.Discount_Percent__c;
                orderLineItem.SPIFF_Discount__c = showCartLineItem.Discount_Dollar__c;
                orderLineItem.Item__c = showCartLineItem.Item__r.Name;
                orderLineItem.Store_Product__c = showCartLineItem.Item__c;
                orderLineItem.Item_Number__c = showCartLineItem.Item_Number__c;
                orderLineItem.Quantity__c = showCartLineItem.Quantity__c;
                orderLineItem.Original_Deal_Cost__c = showCartLineItem.Original_Deal_Cost__c;
                orderLineItem.Arrival_Date__c= showCartLineItem.Arrival_Date__c;
                orderLineItem.Case_UPC__c = showCartLineItem.Item__r.Case_UPC__c;
                orderLineItem.Deal_Pack__c = showCartLineItem.Item__r.Pack_Per_Deal__c;
                orderLineItem.Type_of_Display__c = showCartLineItem.Item__r.Type_of_Display__c;
				
				cartOrderLineItem.orderLineItem = orderLineItem;
                cartOrderLineItem.lineItemComment = showCartLineItem.Customer_Comments__c;
			
				// create the OrderLineItem_ShipTo__c records
				OrderLineItemShipToWrapper orderLineItemShipToWrapper = new OrderLineItemShipToWrapper();
	            orderLineItemShipToWrapper.orderLineItemShipTo = new OrderLineItem_ShipTo__c();
	            orderLineItemShipToWrapper.orderLineItemShipTo.Name = showCartLineItem.Ship_To__r.Name;
	            orderLineItemShipToWrapper.orderLineItemShipTo.Account__c = showCartLineItem.Ship_To__c;
	            orderLineItemShipToWrapper.orderLineItem = cartOrderLineItem.orderLineItem;
				
				cartOrderLineItem.orderLineItemShipToWrapper = orderLineItemShipToWrapper;
				
				// create order line item components
				cartOrderLineItem.orderLineItemComponentList = new List<Order_Line_Item_Component__c>();
				if(itemComponentListByItemIdMap.containsKey(orderLineItem.Store_Product__c))
				{
                    List<Item_Component__c> oliComListRec = itemComponentListByItemIdMap.get(orderLineItem.Store_Product__c);
                    for(Item_Component__c itemComponent : oliComListRec)
                    {
                        itemComponent.Unit_Cost__c = (itemComponent.Unit_Cost__c != null) ? itemComponent.Unit_Cost__c : 0;
                        decimal quantityDeduct = (showCartLineItem.Discount_Dollar__c != null) ? (showCartLineItem.Discount_Dollar__c * itemComponent.Unit_Cost__c)/100 : 0;
                        decimal dealCost  = itemComponent.Unit_Cost__c - quantityDeduct;
                        
                        cartOrderLineItem.orderLineItemComponentList.add(
                        	new Order_Line_Item_Component__c(
                        		Quantity__c = itemComponent.Quantity__c,
                        		Item_Component_UPC__c = itemComponent.Item_UPC__c,
                            	Unit_Cost__c = dealCost,
                            	Suggested_Retail__c = itemComponent.Suggested_Retail__c,
                            	Item_Component_Description__c = itemComponent.Name
                        	)
                        );            
                    }
                }
                
                // use the order line item data to determine which line items to group into a PO
                String datesY = '';
	            if(orderLineItem.Arrival_Date__c != null){
	                datesY = orderLineItem.Arrival_Date__c.Year() + '-' + orderLineItem.Arrival_Date__c.Month() + '-' + orderLineItem.Arrival_Date__c.day();
	            }
	            
	            String vendorId = '';
	            if(showCartLineItem.Item__r != null && showCartLineItem.Item__r.Vendor__c != null){
	            	vendorId = showCartLineItem.Item__r.Vendor__c;
	            }
	            String shipTo = showCartLineItem.Ship_To__c;
	            String combinedIds = vendorId + datesY + shipTo;
	            
	            if(cartOrderLineItemListMap.containsKey(combinedIds)){
	                cartOrderLineItemListMap.get(combinedIds).add(cartOrderLineItem);
	            }
	            else{
	                cartOrderLineItemListMap.put(combinedIds, new List<CartOrderLineItem>{cartOrderLineItem});
	            }
				
				cartOrder.cartOrderLineItemList.add(cartOrderLineItem);
			}
			
			// create the POs
			cartOrder.poWrapperList = new List<PoWrapper>();
			for(List<CartOrderLineItem> cartOrderLineItemList : cartOrderLineItemListMap.values())
			{
				PoWrapper poWrapper = new PoWrapper();
				
				PO__c po = new PO__c();
				po.Bill_To__c = cartOrderLineItemList[0].orderLineItem.Bill_To__c;
                po.Ship_To__c = cartOrderLineItemList[0].orderLineItem.Ship_To__c;
                po.At_Once__c = cartOrderLineItemList[0].orderLineItem.At_Once__c;
                po.Arrival_Date__c = cartOrderLineItemList[0].orderLineItem.Arrival_Date__c;
                po.Account__c = itemIdToVendorIdMap.get(cartOrderLineItemList[0].orderLineItem.Store_Product__c);//cartOrderLineItemList[0].orderLineItem.Store_Product__r.Vendor__c;
            	po.Sales_Rep__c = showCart.Bill_To__r.OwnerId;
				
				poWrapper.po = po;
				poWrapper.poLineItemList = new List<PoLineItemWrapper>();
				
				// create the PO line items
				for(CartOrderLineItem cartOrderLineItem : cartOrderLineItemList)
				{
					// link the po to the order line item ship to
					cartOrderLineItem.orderLineItemShipToWrapper.po = po;
					
					PoLineItemWrapper poLineItemWrapper = new PoLineItemWrapper();
					poLineItemWrapper.poLineItem = new PO_Line_Item__c();
					poLineItemWrapper.poLineItem.Store_Product__c = cartOrderLineItem.orderLineItem.Store_Product__c;
					poLineItemWrapper.poLineItem.Original_Deal_Cost__c = cartOrderLineItem.orderLineItem.Original_Deal_Cost__c;
					poLineItemWrapper.poLineItem.Cost_per_Deal__c = cartOrderLineItem.orderLineItem.Cost_per_Deal__c;
					poLineItemWrapper.poLineItem.Quantity__c = cartOrderLineItem.orderLineItem.Quantity__c;
					poLineItemWrapper.poLineItem.Item_Number__c = cartOrderLineItem.orderLineItem.Item_Number__c;
					poLineItemWrapper.poLineItem.Case_UPC__c = cartOrderLineItem.orderLineItem.Case_UPC__c;
					poLineItemWrapper.poLineItem.Deal_Pack__c = cartOrderLineItem.orderLineItem.Deal_Pack__c;
					poLineItemWrapper.poLineItem.Type_of_Display__c = cartOrderLineItem.orderLineItem.Type_of_Display__c;
					poLineItemWrapper.poLineItem.Discount__c = cartOrderLineItem.orderLineItem.Discount__c;
					poLineItemWrapper.poLineItem.SPIFF_Discount__c = cartOrderLineItem.orderLineItem.SPIFF_Discount__c;
					poLineItemWrapper.poLineItem.Customer_Comment__c = cartOrderLineItem.lineItemComment;
					poLineItemWrapper.poLineItemComponentList = new List<PO_Line_Item_Component__c>();
					
					// create PO line item components
					List<Order_Line_Item_Component__c> oliComListRec = cartOrderLineItem.orderLineItemComponentList; //itemComponentListByItemIdMap.get(cartOrderLineItemList[0].orderLineItem.Item__c);
					for(Order_Line_Item_Component__c o : oliComListRec)
					{
                        poLineItemWrapper.poLineItemComponentList.add(
	                        new PO_Line_Item_Component__c(
	                        	Quantity__c = o.Quantity__c,
	                            Item_Component_UPC__c = o.Item_Component_UPC__c,
	                            Unit_Cost__c = o.Unit_Cost__c,
	                            Suggested_Retail__c = o.Suggested_Retail__c,
	                            PO_Line_Item__c = po.id,
	                            Item_Component_Description__c = o.Item_Component_Description__c,
	                            Order_Line_Item_Component__c = o.id
	                        )
                        );
                    }
					
					poWrapper.poLineItemList.add(poLineItemWrapper);
				}
				
				cartOrder.poWrapperList.add(poWrapper);
			}
			
			cartOrderList.add(cartOrder);
		}
		
		return cartOrderList;
	}
	
	public static List<CartOrder> createOrders(List<CartOrder> cartOrderList)
	{
		// insert the orders
		List<Store_Order__c> orderList = new List<Store_Order__c>();
		for(CartOrder cartOrder : cartOrderList){
			orderList.add(cartOrder.storeOrder);
		}
		insert orderList;
		
		// create the link between store order and show cart
		List<Show_Cart__c> showCartList = new List<Show_Cart__c>();
		for(CartOrder cartOrder : cartOrderList)
		{
			Show_Cart__c showCart = new Show_Cart__c(Id=cartOrder.showCart.Id);
			showCart.Customer_Sales_Order__c = cartOrder.storeOrder.Id;
			showCartList.add(showCart);
		}
		update showCartList;
		
		// insert the order line items
		List<Order_Line_Items__c> orderLineItemList = new List<Order_Line_Items__c>();
		for(CartOrder cartOrder : cartOrderList)
		{
			for(CartOrderLineItem cartOrderLineItem : cartOrder.cartOrderLineItemList)
			{
				cartOrderLineItem.orderLineItem.Store_Order__c = cartOrder.storeOrder.Id;
				orderLineItemList.add(cartOrderLineItem.orderLineItem);
			}
		}
		insert orderLineItemList;
		
		// insert the order line item components
		List<Order_Line_Item_Component__c> orderLineItemComponentList = new List<Order_Line_Item_Component__c>();
		for(CartOrder cartOrder : cartOrderList)
		{
			system.debug(cartOrder);
			for(CartOrderLineItem cartOrderLineItem : cartOrder.cartOrderLineItemList)
			{
				if(cartOrderLineItem.orderLineItemComponentList != null)
				{
					for(Order_Line_Item_Component__c orderLineItemComponent : cartOrderLineItem.orderLineItemComponentList)
					{
						orderLineItemComponent.Order_Line_Item__c = cartOrderLineItem.orderLineItem.Id;
						orderLineItemComponentList.add(orderLineItemComponent);
					}
				}
			}
		}
		insert orderLineItemComponentList;
		
		// insert the POs
		Map<String, Schema.RecordTypeInfo> poRecordTypeMap = TestUtilities.getPoRecordTypes();
		Map<String, PO_RT__c> poRecordTypeSettingMap = PO_RT__c.getAll();
		List<PO__c> poList = new List<PO__c>();
		for(CartOrder cartOrder : cartOrderList)
		{
			for(PoWrapper poWrapper : cartOrder.poWrapperList)
			{
				if(cartOrder.storeOrder.Bill_Types__c.startsWith('Central')){
					poWrapper.po.RecordTypeId = poRecordTypeSettingMap.get('Central Bill').Id__c;
				}
				else if(cartOrder.storeOrder.Bill_Types__c.startsWith('Direct')){
					poWrapper.po.RecordTypeId = poRecordTypeSettingMap.get('Direct Bill').Id__c;
				}
				poWrapper.po.Store_Order__c = cartOrder.storeOrder.Id;
				poList.add(poWrapper.po);
			}
		}
		insert poList;
		
		// insert the PO line items
		List<PO_Line_Item__c> poLineItemList = new List<PO_Line_Item__c>();
		for(CartOrder cartOrder : cartOrderList)
		{
			for(PoWrapper poWrapper : cartOrder.poWrapperList)
			{
				for(PoLineItemWrapper poLineItemWrapper : poWrapper.poLineItemList)
				{
					poLineItemWrapper.poLineItem.PO__c = poWrapper.po.Id;
					poLineItemList.add(poLineItemWrapper.poLineItem);
				}
			}
		}
		insert poLineItemList;
		
		// insert the po line item components
		List<PO_Line_Item_Component__c> poLineItemComponentList = new List<PO_Line_Item_Component__c>();
		for(CartOrder cartOrder : cartOrderList)
		{
			for(PoWrapper poWrapper : cartOrder.poWrapperList)
			{
				for(PoLineItemWrapper poLineItemWrapper : poWrapper.poLineItemList)
				{
					if(poLineItemWrapper.poLineItemComponentList != null)
					{
						for(PO_Line_Item_Component__c poLineItemComponent : poLineItemWrapper.poLineItemComponentList)
						{
							poLineItemComponent.PO_Line_Item__c = poLineItemWrapper.poLineItem.Id;
							poLineItemComponentList.add(poLineItemComponent);
						}
					}
				}
			}
		}
		insert poLineItemComponentList;
		
		// insert the order line item ship to records
		List<OrderLineItem_ShipTo__c> orderLineItemShipToList = new List<OrderLineItem_ShipTo__c>();
		for(CartOrder cartOrder : cartOrderList)
		{
			for(CartOrderLineItem cartOrderLineItem : cartOrder.cartOrderLineItemList)
			{
				cartOrderLineItem.orderLineItemShipToWrapper.orderLineItemShipTo.Order_Line_Item__c = cartOrderLineItem.orderLineItem.Id;
				cartOrderLineItem.orderLineItemShipToWrapper.orderLineItemShipTo.PO__c = cartOrderLineItem.orderLineItemShipToWrapper.po.Id;
				orderLineItemShipToList.add(cartOrderLineItem.orderLineItemShipToWrapper.orderLineItemShipTo);
			}
		}
		insert orderLineItemShipToList;
		
		return cartOrderList;
	}
	
	public class CartOrder
	{
		List<PoWrapper> poWrapperList;
		
		Show_Cart__c showCart;
		Store_Order__c storeOrder;
		List<CartOrderLineItem> cartOrderLineItemList;
	}
	
	public class CartOrderLineItem
	{
		Order_Line_Items__c orderLineItem;
		String lineItemComment;
		
		List<Order_Line_Item_Component__c> orderLineItemComponentList;
		OrderLineItemShipToWrapper orderLineItemShipToWrapper;
	}
	
	public class PoWrapper
	{
		PO__c po;
		List<PoLineItemWrapper> poLineItemList;
	}
	
	public class PoLineItemWrapper
	{
		PO_Line_Item__c poLineItem;
		List<PO_Line_Item_Component__c> poLineItemComponentList;
	}
	
	public class OrderLineItemShipToWrapper
	{
		OrderLineItem_ShipTo__c orderLineItemShipTo;
		PO__c po;
		Order_Line_Items__c orderLineItem;
	}
    
}
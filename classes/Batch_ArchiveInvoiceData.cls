/**
 * This class identifies invoice data that is ready to be archived and moves it to an external storage service
 *
 * Archival criteria: Invoice Status 'Status' equals 'Paid' OR 'Cancelled' and 'Paid Date' is greater than 12 months ago (archive Invoice, Invoice Line Items, and Invoice Line Item Components)
 */
global class Batch_ArchiveInvoiceData implements Database.Batchable<Sobject>, Database.AllowsCallouts, Database.Stateful{
	
	global String query;
	global Date cutOffDate;
	global Map<String, Set<String>> objectFieldSetMap;
	global I_DataArchiveService dataArchiveService;
	
	global Batch_ArchiveInvoiceData()
	{
    	// check a custom metadata record and dynamically instantiate the data archival class identified there
    	Data_Archival_Settings__mdt dataArchivalSettings = [SELECT Archival_Class__c, Minimum_Age__c FROM Data_Archival_Settings__mdt WHERE DeveloperName = 'Invoice_Data' LIMIT 1];
    	Type dataArchiveClassType = Type.forName(dataArchivalSettings.Archival_Class__c);
    	dataArchiveService = (I_DataArchiveService)dataArchiveClassType.newInstance();
		
		// get a list of fields on the objects that will be archived
		objectFieldSetMap = new Map<String, Set<String>>();
		Set<String> invoiceFieldSet = TestUtilities.getFieldNameList('Invoice__c');
		invoiceFieldSet.add('Account__r.Name');
		objectFieldSetMap.put('Invoice__c', invoiceFieldSet);
		Set<String> invoiceLineItemFieldSet = TestUtilities.getFieldNameList('Invoice_Line_Item__c');
		invoiceLineItemFieldSet.add('Invoice__r.Account__r.Name');
		objectFieldSetMap.put('Invoice_Line_Item__c', invoiceLineItemFieldSet);
		Set<String> invoiceLineItemComponentFieldSet = TestUtilities.getFieldNameList('Invoice_Line_Item_Component__c');
		invoiceLineItemComponentFieldSet.add('Invoice_Line_Item__r.Invoice__r.Account__r.Name');
		objectFieldSetMap.put('Invoice_Line_Item_Component__c', invoiceLineItemComponentFieldSet);
		
		// set the cutoff date for the query and use that in the filepath of the archive files
		Datetime cutOffDatetime = Datetime.now().addDays((Integer.valueOf(dataArchivalSettings.Minimum_Age__c * -1)));
		cutOffDate = cutOffDatetime.date();
    	dataArchiveService.setArchivePath('Invoice Archive (up to ' + cutOffDatetime.format('yyyy-MM-dd HH:mm:ss') + ')');
		system.debug('cutOffDate: ' + cutOffDatetime.format());
		
		query = 'SELECT ' + String.join(new List<String>(objectFieldSetMap.get('Invoice__c')), ', ') + ' FROM Invoice__c WHERE Status__c IN (\'Paid\', \'Cancelled\') AND Paid_Date__c <= :cutOffDate AND IsDeleted = false ORDER BY Account__r.Name ASC';
	}
	
	global Database.QueryLocator start(Database.BatchableContext BC){
		system.debug('query: ' + query);
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<Invoice__c> batchInvoiceList)
    {
    	/**
    	 * Get all of the data to be archived for this batch
    	 */
    	// find all of the invoice record IDs
    	Set<Id> invoiceIdSet = new Set<Id>();
    	for(Invoice__c invoice : batchInvoiceList){
    		invoiceIdSet.add(invoice.Id);
    	}
    	
    	// get all of the associated Invoice_Line_Item__c records
    	List<Invoice_Line_Item__c> invoiceLineItemList = Database.query('SELECT ' + String.join(new List<String>(objectFieldSetMap.get('Invoice_Line_Item__c')), ', ') + ' FROM Invoice_Line_Item__c WHERE Invoice__c IN :invoiceIdSet AND IsDeleted = false ORDER BY Invoice__r.Account__r.Name ASC');
    	
    	// get all of the associated Invoice_Line_Item_Component__c records
    	List<Invoice_Line_Item_Component__c> invoiceLineItemComponentList = Database.query('SELECT ' + String.join(new List<String>(objectFieldSetMap.get('Invoice_Line_Item_Component__c')), ', ') + ' FROM Invoice_Line_Item_Component__c WHERE Invoice_Line_Item__r.Invoice__c IN :invoiceIdSet AND IsDeleted = false ORDER BY Invoice_Line_Item__r.Invoice__r.Account__r.Name ASC');
    	
    	
    	/**
    	 * Archive each list of objects starting at the bottom of the hierarchy but don't delete anything until all objects are archived successfully
    	 */
    	Boolean syncErrors = false;
    	
    	// archive the Invoice_Line_Item_Component__c records
    	List<String> shipToNameList = new List<String>();
    	for(Invoice_Line_Item_Component__c invoiceLineItemComponent : invoiceLineItemComponentList)
    	{
			Account shipToAccount = (Account)invoiceLineItemComponent.Invoice_Line_Item__r.Invoice__r.Account__r;
    		if(shipToAccount != null){
    			shipToNameList.add(shipToAccount.Name);
    		}
    		else{
    			shipToNameList.add(' ');
    		}
    	}
    	List<String> shipToNameRangeList = TestUtilities.findAlphaRange(shipToNameList);
    	Map<Id, Boolean> invoiceLineItemComponentResultMap = dataArchiveService.archiveRecords('Invoice_Line_Item_Component__c (Bill To ' + String.join(shipToNameRangeList, '-') + ')', invoiceLineItemComponentList, objectFieldSetMap.get('Invoice_Line_Item_Component__c'));
    	
    	// if any records failed to sync, set the syncErrors flag to stop the archiving process
    	syncErrors = !checkSyncStatus(invoiceLineItemComponentResultMap);
    	
    	if(!syncErrors)
    	{
	    	// archive the Invoice_Line_Item__c records
	    	shipToNameList = new List<String>();
	    	for(Invoice_Line_Item__c invoiceLineItem : invoiceLineItemList)
	    	{
				Account shipToAccount = (Account)invoiceLineItem.Invoice__r.Account__r;
	    		if(shipToAccount != null){
	    			shipToNameList.add(shipToAccount.Name);
	    		}
	    		else{
	    			shipToNameList.add(' ');
	    		}
	    	}
	    	shipToNameRangeList = TestUtilities.findAlphaRange(shipToNameList);
	    	Map<Id, Boolean> invoiceLineItemResultMap = dataArchiveService.archiveRecords('Invoice_Line_Item__c (Bill To ' + String.join(shipToNameRangeList, '-') + ')', invoiceLineItemList, objectFieldSetMap.get('Invoice_Line_Item__c'));
	    	
	    	// if any records failed to sync, set the syncErrors flag to stop the archiving process
    		syncErrors = !checkSyncStatus(invoiceLineItemComponentResultMap);
	    	
	    	if(!syncErrors)
	    	{
		    	// archive the Invoice__c records
		    	shipToNameList = new List<String>();
		    	for(Invoice__c invoice : batchInvoiceList)
		    	{
					Account shipToAccount = (Account)invoice.Account__r;
		    		if(shipToAccount != null){
		    			shipToNameList.add(shipToAccount.Name);
		    		}
		    		else{
		    			shipToNameList.add(' ');
		    		}
		    	}
		    	shipToNameRangeList = TestUtilities.findAlphaRange(shipToNameList);
		    	Map<Id, Boolean> invoiceResultMap = dataArchiveService.archiveRecords('Invoice__c (Bill To ' + String.join(shipToNameRangeList, '-') + ')', batchInvoiceList, objectFieldSetMap.get('Invoice__c'));
		    	
		    	// if any records failed to sync, set the syncErrors flag to stop the archiving process
	    		syncErrors = !checkSyncStatus(invoiceLineItemComponentResultMap);
	    		
		    	// if the archiving process succeeded then we can delete all of the records. Anything that did not sync will be left in place and retried on the next run
		    	if(!syncErrors)
		    	{
		    		delete invoiceLineItemComponentList;
		    		delete invoiceLineItemList;
		    		delete batchInvoiceList;
		    	}
	    	}
    	}
    	
    	// allow any deferred DML to be processed
    	dataArchiveService.deferredDML();
    }
    
    global void finish(Database.BatchableContext BC)
    {
    	
    }
    
    // traverse the sync result map and determine if the sync was 100% successful (true) or contained failures (false)
    public Boolean checkSyncStatus(Map<Id, Boolean> syncStatusMap)
    {
    	for(Boolean syncResult : syncStatusMap.values())
    	{
    		if(syncResult == false){
    			return false;
    		}
    	}
    	
    	return true;
    }
    
}
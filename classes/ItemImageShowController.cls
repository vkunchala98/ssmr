public class ItemImageShowController {
    public Item__c item {get;set;}
    public Id itemImageId {get;set;}

    public ItemImageShowController (ApexPages.StandardController sc) {
    
        this.item = (Item__c)sc.getRecord();
        List<Attachment> attachedFiles = [select Id from Attachment where parentId =:item.Id Limit 1];
        if (attachedFiles.size() > 0 ) {
            itemImageId = attachedFiles[0].Id;
        } else {
            itemImageId = NULL;
        }
    }
    
    public PageReference UploadImage() {
        PageReference pg = new PageReference('/apex/ItemImageUpload?id='+item.Id);
        return pg;
    
    }

}
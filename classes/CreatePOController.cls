global class CreatePOController{

    public CreatePOController(){
    
    }
    
    webservice static String CreatePO(String soId){
        try{
            Map<String, PO__c> vendorPOMap = new Map<String, PO__c>();
            List<PO_Line_Item__c> poLIList = new List<PO_Line_Item__c>();
            
            List<Order_Line_Items__c> oliList = [SELECT Id, Name, Total__c, Store_Product__c, Store_Product__r.Vendor__r.Id,Store_Order__c,Bill_To__c,
                                                        Cost_per_Deal__c,Discount__c,Item__c,Item_Number__c,Quantity__c,Ship_To__c,
                                                        Type_of_Display__c
                                                 FROM Order_Line_Items__c 
                                                 WHERE Store_Order__c = :soId ];
            for(Order_Line_Items__c oli : oliList){
                if(vendorPOMap.containsKey(oli.Store_Product__r.Vendor__r.Id)){
                    if(vendorPOMap.get(oli.Store_Product__r.Vendor__r.Id) != NULL)
                    vendorPOMap.put(oli.Store_Product__r.Vendor__r.Id, new PO__c(Store_Order__c = oli.Store_Order__c,Account__c = oli.Store_Product__r.Vendor__r.Id));
                } else {
                    vendorPOMap.put(oli.Store_Product__r.Vendor__r.Id, new PO__c(Store_Order__c = oli.Store_Order__c,Account__c = oli.Store_Product__r.Vendor__r.Id));
                }
                
            }   
            if(vendorPOMap.size() > 0 ) Insert vendorPOMap.values();
            if(vendorPOMap.size() > 0){
                for(Order_Line_Items__c oli : oliList){
                    poLIList.add(new PO_Line_Item__c(PO__c = vendorPOMap.get(oli.Store_Product__r.Vendor__r.Id).Id,Store_Product__c = oli.Store_Product__c,Cost_per_Deal__c=oli.Cost_per_Deal__c,
                                                     Discount__c=oli.Discount__c,Item_Number__c=oli.Item_Number__c,Quantity__c=oli.Quantity__c,
                                                     Type_of_Display__c=oli.Type_of_Display__c,
                                                     Store_Order__c=oli.Store_Order__c));
                }
                if(poLIList.size() > 0) Insert poLIList;
            }         
            return 'success';
        } catch(Exception e){
            return e.getMessage();
        }
        
        return 'success';
    }

}
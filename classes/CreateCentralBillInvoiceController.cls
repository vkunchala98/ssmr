global class CreateCentralBillInvoiceController{

    public CreateCentralBillInvoiceController(){
     
    }
    
    webservice static String createCentralBillInvoiceController(String poId){
        
        try{
            
            System.debug('------poId---------'+poId);
            
            List<RecordType> invoiceRecordTypeList = [SELECT Id,Name,DeveloperName FROM RecordType WHERE SObjectType = 'Invoice__c'];
            System.debug('------invoiceRecordTypeList ---------'+invoiceRecordTypeList );
            
            Id customerInvoiceRecordTypeId, vendorInvoiceRecordTypeId, vendorPayableRecordTypeId;
            String vendorInvoiceId;

            for(RecordType rec : invoiceRecordTypeList){
                if(rec.DeveloperName == 'Central_Bill_Invoice')
                    customerInvoiceRecordTypeId = rec.id;
                else if(rec.DeveloperName == 'Vendor_Invoice')
                    vendorInvoiceRecordTypeId = rec.id;
                else if(rec.DeveloperName == 'Vendor_Payable')
                    vendorPayableRecordTypeId = rec.id;
            }

            List<PO__c> poList = [select id,Bill_To__c,Bill_To__r.Service_charge__c,Ship_To__c,Account__c,Account__r.Commission_Percentage__c,Store_Order__c,Sales_Rep__c,
                Bill_To__r.Terms__c,Vendor_Invoice_Num__c,Vendor_Invoice_Date__c 
                FROM PO__c WHERE id = :poId 
            ];
            system.debug('------poList ---------'+poList );
            
            List<PO_Line_Item__c> poliList = [SELECT Id, Name, Total__c, Store_Product__c, Store_Product__r.Vendor__r.Id,Store_Order__c,Store_Order__r.Bill_To__c,
                Vendor_Commission_Percentage_Owed__c,Cost_per_Deal__c,Item_Number__c,Quantity__c,Registered_On__c,Type_of_Display__c,PO__c,
                SMR_Customer_Invoice__c,SMR_Vendor_Invoice__c,Vendor_Payable_Invoice__c,PO__r.Ship_To__c
                FROM PO_Line_Item__c 
                WHERE PO__c = :poId 
                    AND Registered__c = true 
                    AND (SMR_Customer_Invoice__c = NULL OR SMR_Vendor_Invoice__c = NULL OR Vendor_Payable_Invoice__c = NULL) 
            ];
            system.debug('------poliList ---------'+poliList );
            
            if(poliList.size() > 0) {

                Decimal ciPoliTotalAmount = 0;
                Decimal ciPoServiceChargeAmount = 0;
                Decimal ciAmount = 0;

                Decimal vendorCommissionPercentageOwed = 0;
                Decimal vpiPoliTotalAmount = 0;

                List<Invoice__c> invoiceList = new List<Invoice__c>();
                List<PO_Line_Item__c> customerInvoicePOList = new List<PO_Line_Item__c>();
                List<PO_Line_Item__c> vendorInvoicePOList = new List<PO_Line_Item__c>();
                List<PO_Line_Item__c> vendorPayableInvoicePOList = new List<PO_Line_Item__c>(); 
                
                for(PO_Line_Item__c poli : poliList){
                    if(poli.SMR_Customer_Invoice__c == null) {
                        customerInvoicePOList.add(poli);
                        if(poli.Total__c != null)
                            ciPoliTotalAmount += poli.Total__c;
                    }
                    if(poli.SMR_Vendor_Invoice__c == null) {
                        vendorInvoicePOList.add(poli);
                        if(poli.Vendor_Commission_Percentage_Owed__c != null)
                            vendorCommissionPercentageOwed += poli.Vendor_Commission_Percentage_Owed__c ;
                    }
                    if(poli.Vendor_Payable_Invoice__c == null) {
                        vendorPayableInvoicePOList.add(poli);
                        if(poli.Total__c != null) {
                            vpiPoliTotalAmount += poli.Total__c ;
                        }
                    }
                }
                
                Date invoiceDate = (poList[0].Vendor_Invoice_Date__c != null) ? poList[0].Vendor_Invoice_Date__c : System.today();

                //Customer Invoice
                if(poList[0].Bill_To__r.Service_charge__c != null) 
                    ciPoServiceChargeAmount = (ciPoliTotalAmount * poList[0].Bill_To__r.Service_charge__c)/100;
                    ciAmount = ciPoliTotalAmount + ciPoServiceChargeAmount;
                invoiceList.add(new Invoice__c(Account__c = poList[0].Bill_To__c,Amount__c = ciAmount,
                    RecordTypeId= customerInvoiceRecordTypeId,
                    PO__c = poList[0].id,Vendor__c = poList[0].Account__c,Sales_Rep__c = poList[0].Sales_Rep__c,
                    Terms__c = poList[0].Bill_To__r.Terms__c,Ship_To__c = poList[0].Ship_To__c,Invoice_Type__c = 'Central Bill',Invoice_Date__c = invoiceDate,
                    Status__c = 'Not Yet Invoiced',
                    Vendor_Invoice__c = poList[0].Vendor_Invoice_Num__c 
                ));             

                //Vendor Invoice
                invoiceList.add(new Invoice__c(Account__c = poList[0].Account__c, Amount__c = vendorCommissionPercentageOwed, 
                    RecordTypeId=vendorInvoiceRecordTypeId,
                    Sales_Rep__c = poList[0].Sales_Rep__c,Terms__c = poList[0].Bill_To__r.Terms__c,Invoice_Type__c = 'Vendor Commission',
                    Invoice_Date__c = invoiceDate,Status__c = 'Not Yet Invoiced',
                    Vendor_Invoice__c = poList[0].Vendor_Invoice_Num__c 
                ));            
                
                //Vendor Payable Invoice
                invoiceList.add(new Invoice__c(Account__c = poList[0].Account__c, Amount_Payable__c = vpiPoliTotalAmount,
                    RecordTypeId=vendorPayableRecordTypeId,
                    Sales_Rep__c = poList[0].Sales_Rep__c,Invoice_Type__c = 'Vendor Payable',
                    Invoice_Date__c = invoiceDate,Status__c = 'Not Yet Invoiced',
                    Vendor_Invoice__c = poList[0].Vendor_Invoice_Num__c 
                ));            

                //Insert all the 3 invoices
                if(invoiceList.size() > 0) 
                    insert invoiceList;
                
                System.debug('>>>>Invoice list addition>>'+invoiceList);
                vendorInvoiceId = invoiceList[1].Id;
                System.debug('>>>>Invoice list addition vendorInvoiceId>>'+vendorInvoiceId);

                Invoice__c invUpd = new Invoice__c(Id=invoiceList[2].Id);
                invUpd.Customer_Central_Bill_Invoice__c = invoiceList[0].Id;
                update invUpd;

                /************ Invoice Line Item Creation Logic Starts ********************/

                Id ciId, viId, vpiId;
                List<Invoice_Line_Item__c> invoiceLIList = new List<Invoice_Line_Item__c>();
                Map<Id, PO_Line_Item__c> updatePoliMap = new Map<Id, PO_Line_Item__c>();
                
                List<RecordType> invoiceLineItemRecTypeList = [SELECT Id,DeveloperName FROM RecordType WHERE (DeveloperName = 'Customer' OR DeveloperName = 'Vendor') AND  SobjectType = 'Invoice_Line_Item__c'];
                Id vendorILIRecId,customerILIRecId;
                
                for(RecordType rc : invoiceLineItemRecTypeList){
                    if(rc.DeveloperName == 'Customer')customerILIRecId = rc.id;
                    else if(rc.DeveloperName == 'Vendor')vendorILIRecId = rc.id;
                }
                
                for(Invoice__c inv : invoiceList){
                    
                    List<PO_Line_Item__c> poliForIliList = new List<PO_Line_Item__c>();

                    if(inv.RecordTypeId == customerInvoiceRecordTypeId) {
                        ciId = inv.id;
                        poliForIliList = customerInvoicePOList;
                    } else if(inv.RecordTypeId == vendorInvoiceRecordTypeId) {
                        viId = inv.Id;
                        poliForIliList = vendorInvoicePOList;
                    } else if(inv.RecordTypeId == vendorPayableRecordTypeId) {
                        vpiId = inv.Id;
                        poliForIliList = vendorPayableInvoicePOList;
                    }
                    System.debug('---^^^^^^^^^^^^^-poliForIliList----'+poliForIliList);
                    
                    for(PO_Line_Item__c oli : poliForIliList){
                        decimal commissionDue = null;
                        Id poShipTo;
                        Id recTypeId;
                        if(inv.RecordTypeId == vendorInvoiceRecordTypeId)commissionDue = oli.Vendor_Commission_Percentage_Owed__c;
                        
                        if(inv.RecordTypeId == vendorInvoiceRecordTypeId || inv.RecordTypeId == vendorPayableRecordTypeId){
                            
                            poShipTo = oli.PO__r.Ship_To__c;
                            recTypeId = vendorILIRecId;
                        }
                        
                        if(inv.RecordTypeId == customerInvoiceRecordTypeId)recTypeId = customerILIRecId;
                        
                        invoiceLIList.add(new Invoice_Line_Item__c(Invoice__c = inv.id,Store_Product__c = oli.Store_Product__c,Cost_per_Deal__c=oli.Cost_per_Deal__c,
                            Bill_To__c=oli.Store_Order__r.Bill_To__c,Item_Number__c=oli.Item_Number__c,Quantity__c=oli.Quantity__c,
                            Type_of_Display__c=oli.Type_of_Display__c,
                            PO_Line_Item_Registered_On__c=oli.Registered_On__c,PO_Line_Item__c=oli.Id,
                            Commission_Due__c= commissionDue,
                            Ship_To__c = poShipTo,
                            RecordTypeId = recTypeId
                        ));
                        
                        PO_Line_Item__c updPOLI;
                        if(!updatePoliMap.containsKey(oli.Id)) {
                            updPOLI = new PO_Line_Item__c(Id=oli.Id);
                        } else {
                            updPOLI = updatePoliMap.get(oli.Id);
                        }
                        
                        if(inv.Id == ciId) {
                            updPOLI.SMR_Customer_Invoice__c = ciId;
                            updPOLI.SMR_Customer_Invoiced_Date__c = System.now();
                        } else if(inv.Id == viId) {
                            updPOLI.SMR_Vendor_Invoice__c = viId;
                            updPOLI.SMR_Vendor_Invoice_Date__c = System.today();
                        } else if(inv.Id == vpiId) {
                            updPOLI.Vendor_Payable_Invoice__c = vpiId;
                            updPOLI.Payable_Invoice_Date__c = System.today();
                        }
                        updatePoliMap.put(oli.Id, updPOLI);
                    }
                }

                System.debug('----invoiceLIList----'+invoiceLIList);
                System.debug('----updatePoliMap----'+updatePoliMap);

                if(invoiceLIList.size() > 0) 
                    insert invoiceLIList;
                if(updatePoliMap.size() > 0) 
                    update updatePoliMap.values();
                
                /************ Invoice Line Item Creation Logic Ends ********************/
            }         
            System.debug('@@vendorInvoiceId@@@@@'+vendorInvoiceId);
            return 'success-'+vendorInvoiceId;   
        } catch(Exception e){
            return e.getMessage();
        }
    }

}
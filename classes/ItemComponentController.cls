public without sharing class ItemComponentController{
    
    public Item__c itemObj {get; set;}
    public List<Item_Component__c> itemList {get; set;}
    public String successMsg {get; set;}
    public Id itemId;
    public User vendorUser {get;set;}
    public Attachment attachment;
    public Boolean newItem {get;set;}
    public Id rt = Schema.Sobjecttype.Item__c.getRecordTypeInfosByName().get('Item').getRecordTypeId();
    public Boolean isInsert {get;set;}
    
    public ItemComponentController(){
       itemList = new List<Item_Component__c>();
    }
    
    public ItemComponentController(ApexPages.StandardController stdController){

        successMsg = '';
        itemObj = (Item__c)stdController.getRecord();
        if(itemObj.Id != null){
            Id itemId = itemObj.id;
            itemObj = [SELECT Id
                            ,Name
                            ,Case_Cube__c
                            ,Case_Dimension_LxWxH__c
                            ,Case_Height__c
                            ,Case_Length__c
                            ,Case_UPC__c
                            ,Case_Weight__c
                            ,Case_Width__c
                            ,Category__c
                            ,Comments__c
                            ,Cost_per_Deal__c
                            ,Cost_Per_Item__c
                            ,Display_Image__c
                            ,Display_in__c
                            ,External_Id__c
                            ,Freight__c
                            ,Item__c
                            ,Item_Status__c
                            ,Item_UPC__c
                            ,Minimum_Dollar_Amount__c
                            ,Minimum_Order_Quantity__c
                            ,Note__c
                            ,Pack_Per_Deal__c
                            ,Product_Image__c
                            ,Sub_Type__c
                            ,Suggested_Retail_Price__c
                            ,Total_of_Pieces__c
                            ,Total_Deal_Cost__c
                            ,Total_Number_Of_Pieces__c
                            ,Type_Of_Display__c
                            ,Vendor__c
                            ,Ti_Hi__c
                        FROM Item__c
                        WHERE Id = :itemId
            ];
            itemList  = [SELECT Id,Name,Item_UPC__c,Unit_Cost__c,Quantity__c,Ext_Cost__c,Item__c FROM Item_Component__c
            where Item__c =:itemId];
            isInsert = false;
        }else{
            isInsert = true;
            itemList = new List<Item_Component__c>();
            Item_Component__c ic = new Item_Component__c();
            itemList.add(ic);
        }

        vendorUser = [SELECT AccountId FROM User WHERE Id = :UserInfo.getUserId() LIMIT 1];
        if(vendorUser.AccountId != null){
            itemObj.Vendor__c = vendorUser.AccountId;
        }
        itemObj.RecordTypeId = rt;
        //Have to set the itemObj record type to be multi-item
    }

    public void showItemComponents(){
        addItemComponents();
    }

    public void addItemComponents(){
        system.debug('--------itemList-------'+itemList.size());
        Item_Component__c newItemComponent = new Item_Component__c(Item__c = itemObj.Id);
        system.debug('--------newItemComponent -------'+newItemComponent);
        itemList.add(newItemComponent);
        system.debug('--------itemList-------'+itemList);
    }

    public PageReference doSaveAndNew(){
        Savepoint sp = Database.setSavepoint();
        try {
            if(itemObj.Item_Status__c == null){
                itemObj.Item_Status__c = 'Active';
            }
            if(isInsert) itemObj.Id = null;
            upsert itemObj;
            for(Item_Component__c ic : itemList){
                ic.Item__c = itemObj.Id;
            }
            upsert itemList;
            PageReference pr = Page.ItemComponent;
            pr.setRedirect(true);
            return pr;  
        } catch (exception e) {
            System.debug(System.Logginglevel.INFO, 'BRAND2: ' + e.getMessage());      
            apexPages.addMessages(e);
            Database.rollback(sp);
            return null;
        }

        
    }

    public PageReference doSave(){
        Savepoint sp = Database.setSavepoint();
        try {
            if(itemObj.Item_Status__c == null){
                itemObj.Item_Status__c = 'Active';
            }
            if(isInsert) itemObj.Id = null;
            upsert itemObj;
            for(Item_Component__c ic : itemList){
                ic.Item__c = itemObj.Id;
            }
            upsert itemList;
            String recordURL = '/';
            if(vendorUser.AccountId != null) recordURL += 'vendors/';
            recordURL += itemObj.Id;
            PageReference pr = new PageReference(recordURL);
            return pr;
        } catch (exception e) {
            System.debug(System.Logginglevel.INFO, 'BRAND2: ' + e.getMessage());      
            apexPages.addMessages(e);
            Database.rollback(sp);
            return null;
        }
    }
}
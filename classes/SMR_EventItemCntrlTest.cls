@isTest(seeAllData=true)
public class SMR_EventItemCntrlTest{
      private static testmethod void positivescenario(){
          Id VendorRecordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Vendor').getRecordTypeId();

        Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator'];
         User u2 = new User(Alias = 'newUser', Email='newuser44444@testorg.com',
         EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
         LocaleSidKey='en_US', ProfileId = p.Id,
         TimeZoneSidKey='America/Los_Angeles', UserName='newuser1234444@testorg.com');
         system.runas(u2){
         String label=System.label.Central_Customer_PO;
     Loop__DDP__c ddp = new Loop__DDP__c(Name='My DDP');
        insert ddp;
        
        Loop__DDP_Integration_Option__c delivOpt = new Loop__DDP_Integration_Option__c(Name='Email', Loop__DDP__c=ddp.Id);
        insert delivOpt;
        
       contact con= new Contact();
       con.LastName='test Name';
       con.Email='test@gmail.com';
       con.phone='1234';
       insert con;
       Account acc= new Account();
       acc.Name='Test vendor';
       acc.RecordTypeid=VendorRecordTypeId;
       insert acc;
       Account acc1= new Account();
       acc1.Name='Test billing';
       acc1.BillingStreet='test street';
       acc1.BillingCity='test city';
       acc1.BillingState='CA';
       acc1.BillingPostalcode='521235';
       acc1.BillingCountry='UAS';
       acc1.ShippingStreet='test street';
       acc1.ShippingCity='test city';
       acc1.ShippingState='CA';
       acc1.ShippingPostalcode='521235';
       acc1.ShippingCountry='UAS'; 
       acc1.Terms__c='Net 21'; 
       acc1.Email_Contact__c=con.id;     
       insert acc1;
       List<Event__c> eventlist= new List<Event__c>();
       Event__c event= new Event__c();
       event.Name='Test event';
       event.Account_Name__c=acc1.id;
       eventlist.add(event);
       Test.startTest();
          Test.setCurrentPage(Page.Smr_EventItems);
          ApexPages.StandardSetController stdSetController = new ApexPages.StandardSetController(eventlist);
          stdSetController.setSelected(eventlist);
          SMR_EventItemCntrl ext = new SMR_EventItemCntrl(stdSetController);
          ext.doSubmit();
      Test.stopTest();
       
        }
       }
}
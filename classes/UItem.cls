public class UItem {
    
    public static void checkItemNumber(List<Item__c> records, Map<Id,Item__c> oldMap) {
        Set<Id> vendorIds = new Set<Id>();
        Map<Id, List<Item__c>> vendorIdToItemList = new Map<Id, List<Item__c>>();

        for(Item__c i : records){
            vendorIds.add(i.Vendor__c);
        }
        
        List<Item__c> sfItems = [SELECT Vendor__c, Item__c, Id FROM Item__c WHERE Vendor__c IN: vendorIds];
        for(Item__c i : sfItems){
            if(vendorIdToItemList.get(i.Vendor__c) == null){
                vendorIdToItemList.put(i.Vendor__c, new List<Item__c>());
            }
            vendorIdToItemList.get(i.Vendor__c).add(i);
        }

        for(Item__c i : records){
            if(vendorIdToItemList.get(i.Vendor__c) != null){
                for(Item__c sfi : vendorIdToItemList.get(i.Vendor__c)){
                    if(i.Item__c == sfi.Item__c && i.Id != sfi.Id){
                        i.addError('You cannot have a duplicate Item Number under the same Vendor.');
                    }
                }
            }
        }
    }
    public static void Availblein2019(List<Item__c> records){
    set<Id> accountids= new set<Id>();
    Map<Id,Id> MapwithAccevent = new Map<Id,Id>();
    List<Item__c> ItemrecordstoUpdate= new List<Item__c>();//to update event record on Item when checkbox is checked
    List<Item__c> ItemEventtoUpdatetonull= new List<Item__c>();//to update event record to null on Item when checkbox is false
    set<Id>  Itemsids= new set<Id>();
    set<Id>  Itemsidsforfalse= new set<Id>();
    for(Item__c itc:records){
        if(itc.Available_in_2019_Show__c==true){
          if(itc.Vendor__c!=null){
             accountids.add(itc.Vendor__c);
          }
       }   
    }
    
    List<Account> AccountList=[select id,Name,(select id,Name from Event__r) from Account where Id IN: accountids];
    if(AccountList.size()>0){
  for(Account acc:AccountList){
     system.debug('#$#$#$%%'+acc);
     if(acc.Event__r.size()>0)
     if(acc.Event__r[0].id!=null){
      MapwithAccevent.put(acc.id,acc.Event__r[0].id);
     }
  }
  }
  system.debug('RTTTRTTY'+MapwithAccevent);
  for(Item__c  itemupdate:records){
    if(itemupdate.Available_in_2019_Show__c==true){
             Itemsids.add(itemupdate.id);
         }
         else if(itemupdate.Available_in_2019_Show__c==false){
             Itemsidsforfalse.add(itemupdate.id);
         }
      }  
      
        List<Item__c> ItemsList=[select id,Name,Vendor__c,Event__c,Available_in_2019_Show__c from Item__c where Id IN:Itemsids];
              List<Item__c> ItemsListforfalse=[select id,Name,Vendor__c,Event__c,Available_in_2019_Show__c from Item__c where Id IN:Itemsidsforfalse];

             if(ItemsList.size()>0){
             for(Item__c Itm:ItemsList){
                 if(MapwithAccevent!=null && MapwithAccevent.containskey(Itm.Vendor__c)){
                     Itm.Event__c=MapwithAccevent.get(Itm.Vendor__c);
                     ItemrecordstoUpdate.add(Itm);
                 } 
             }
             }
             if(ItemsListforfalse.size()>0){
                 for(Item__c Itm:ItemsListforfalse){
                      Itm.Event__c=null;
                      ItemEventtoUpdatetonull.add(Itm);
                 }
             }
             
       if(!ItemrecordstoUpdate.isEmpty()){
          Update ItemrecordstoUpdate;
      }
      if(!ItemEventtoUpdatetonull.isEmpty()){ 
         Update ItemEventtoUpdatetonull;
      }

}


}
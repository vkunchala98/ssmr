/**
 * This class defines an interface for a data archival service class so that the data archival capabilities of the org are more modular and allow easily switching between different implementations
 */
public interface I_DataArchiveService {
	
	/**
	 * This method should accept a list of records to archive and return a map containing true/false to indicate success/failure, indexed by the record ID
	 */
	Map<Id, Boolean> archiveRecords(String archiveName, List<SObject> records, Set<String> objectFieldSet);
    
	/**
	 * This method should be called once the archive service is no longer needed. This allows for deferring DML statements until after all callouts have been made (ex: request logging)
	 */
	void deferredDML();
	
	/**
	 * This method is used by archive services that store the data in a hierarchical file system to set the file path of the archive files being created
	 */
	void setArchivePath(String path);
    
}
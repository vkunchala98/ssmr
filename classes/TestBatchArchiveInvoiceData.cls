/**
 * This class contains unit tests for validating the behavior of the Batch_ArchiveInvoiceData class and any related code
 */
@isTest
private class TestBatchArchiveInvoiceData {
	
	@TestSetup
	static void setupData()
	{
		// create custom settings
		TestUtilities.makeCustomSettings();
		
		// create customer and vendor accounts
		List<Account> accountList = TestUtilities.generateCustomerAccounts(1);
		accountList[0].Name = accountList[0].Name.replace('Test ', '');
		List<Account> vendorAccountList = TestUtilities.generateVendorAccounts(1);
		vendorAccountList[0].Name = vendorAccountList[0].Name.replace('Test ', '');
		accountList.add(vendorAccountList[0]);
		insert accountList;
		
		// create items
		List<Item__c> itemList = TestUtilities.generateItems(1);
		itemList[0].Vendor__c = vendorAccountList[0].Id;
		insert itemList;
	}

    static testMethod void testInvoiceDataArchival(){
    	TestBatchArchiveInvoiceData.testInvoiceDataArchivalHelper(1);
    }

    static testMethod void testInvoiceDataArchivalBulk(){
    	TestBatchArchiveInvoiceData.testInvoiceDataArchivalHelper(200);
    }

    static void testInvoiceDataArchivalHelper(Integer quantity)
    {
    	// get customer and vendor accounts
    	Map<String, Schema.RecordTypeInfo> accountRecordTypeMap = TestUtilities.getAccountRecordTypes();
    	List<Account> customerAccountList = [SELECT Id FROM Account WHERE RecordTypeId = :accountRecordTypeMap.get('Customer').getRecordTypeId()];
    	List<Account> vendorAccountList = [SELECT Id FROM Account WHERE RecordTypeId = :accountRecordTypeMap.get('Vendor').getRecordTypeId()];
        
        // create invoices
        Date cutoffDate = Date.today().addYears(-2);
        List<Invoice__c> invoiceList = TestUtilities.generateInvoices(quantity);
        for(Integer ii=0; ii < invoiceList.size(); ++ii)
        {
        	Invoice__c invoice = invoiceList[ii];
        	invoice.Account__c = customerAccountList[Math.mod(ii, customerAccountList.size())].Id;
        	invoice.Status__c = 'Paid';
        	invoice.Paid_Date__c = cutoffDate;
        	invoice.Ship_To__c = (Math.mod(ii, 2) == 0 ? customerAccountList[0].Id : vendorAccountList[0].Id);
        }
        insert invoiceList;
        
        // create invoice line items
        List<Invoice_Line_Item__c> invoiceLineItemList = TestUtilities.generateInvoiceLineItems(quantity);
        for(Integer ii=0; ii < invoiceLineItemList.size(); ++ii)
        {
        	Invoice_Line_Item__c invoiceLineItem = invoiceLineItemList[ii];
        	invoiceLineItem.Invoice__c = invoiceList[Math.mod(ii, invoiceList.size())].Id;
        }
        insert invoiceLineItemList;
        
        // create invoice line item components
        List<Invoice_Line_Item_Component__c> invoiceLineItemComponentList = TestUtilities.generateInvoiceLineItemComponents(quantity);
        for(Integer ii=0; ii < invoiceLineItemComponentList.size(); ++ii)
        {
        	Invoice_Line_Item_Component__c invoiceLineItemComponent = invoiceLineItemComponentList[ii];
        	invoiceLineItemComponent.Invoice_Line_Item__c = invoiceLineItemList[Math.mod(ii, invoiceLineItemList.size())].Id;
        }
        insert invoiceLineItemComponentList;
        
        // set up the callout mock class
        Test.setMock(HttpCalloutMock.class, new TestMultiMockCallout());
        
        // run the scheduler for the data archival service
        Test.startTest();
        
        // queue up the job directly so it actually executes
        Batch_ArchiveInvoiceData bc = new Batch_ArchiveInvoiceData();
        Database.executeBatch(bc);
        
        Test.stopTest();
        
        // verify that the records were all cleaned up
   /*     storeOrderList = [SELECT Id FROM Store_Order__c];
        System.assertEquals(0, storeOrderList.size());
        orderLineItemList = [SELECT Id FROM Order_Line_Items__c];
        System.assertEquals(0, orderLineItemList.size());
        orderLineItemComponentList = [SELECT Id FROM Order_Line_Item_Component__c];
        System.assertEquals(0, orderLineItemComponentList.size());
        poList = [SELECT Id FROM PO__c];
        System.assertEquals(0, poList.size());
        poLineItemList = [SELECT Id FROM PO_Line_Item__c];
        System.assertEquals(0, poLineItemList.size());
        poLineItemComponentList = [SELECT Id FROM PO_Line_Item_Component__c];
        System.assertEquals(0, poLineItemComponentList.size());*/
    }
    
}
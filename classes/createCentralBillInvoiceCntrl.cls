public class createCentralBillInvoiceCntrl {
    
    @AuraEnabled
    public static String getStoreOrder(String poid) {
        PO__c myPO=[select id,Store_Order__c from PO__c where id=:poid limit 1 ];
        if(myPO.Store_Order__c!=null){
            Store_Order__c sOrder = [SELECT Id, Bill_Types__c,Total_Number_of_Line_Items__c FROM Store_Order__c WHERE Id =:myPO.Store_Order__c limit 1];
            String billType = sOrder.Bill_Types__c; 
            Decimal totalLineItems = sOrder.Total_Number_of_Line_Items__c; 
            if( billType == 'Central Warehouse' || billType == 'Central Dropship') { 
                if(totalLineItems > 0){ 
                    String result=CreateCentralBillInvoiceController.createCentralBillInvoiceController(poid);
                    System.debug('>>>resultin lghtnin controller>>>'+result);
                    if(result.contains('success-')){
                        //redirect to record
                        return result;
                    }else{
                        //show exception message
                        return result;
                    }
                } 
            } else { 
                return 'BillType';
                //system.debug('Bill Type of this Customer Sales Order should be Central Warehouse or Central Dropship'); 
            }
        }
        return null;
    }
}
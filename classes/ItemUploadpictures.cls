Public Class ItemUploadpictures{
Public Id ItemId;
Public List<Upload_Picture__c> uploadpictures{get;set;}
   public ItemUploadpictures(ApexPages.StandardController stdController){
        ItemId=ApexPages.currentPage().getParameters().get('id');
        uploadpictures=new List<Upload_Picture__c>();
               uploadpictures=[select id,Name,Type__c,ContentVersion_Id__c from Upload_Picture__c where Item__c=:ItemId];

   }
   public pagereference getUploadPictures(){
   PageReference pageRef;
       uploadpictures=[select id,Name,Type__c,ContentVersion_Id__c from Upload_Picture__c where Item__c=:ItemId];
       if(uploadpictures.size()>0){
           if(uploadpictures.Size()==1){
                if(uploadpictures[0].Type__c=='Display'){
                     pageRef= new PageReference('/apex/ItemUploadpicturesPdfSingle?id='+ItemId);
                     pageRef.setredirect(true); 
                }else{
                    pageRef= new PageReference('/apex/UploadPicturesMultiplePdf?id='+ItemId);
                 pageRef.setredirect(true);
                }
           }
           else{
                 pageRef= new PageReference('/apex/UploadPicturesMultiplePdf?id='+ItemId);
                 pageRef.setredirect(true);
                
                     
             }
       }
       
         
           return pageRef;
   }
}
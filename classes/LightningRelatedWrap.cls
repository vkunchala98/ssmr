public class LightningRelatedWrap {
    @auraEnabled
    public String EventId;
    @auraEnabled
    Public List<Incentive__c> allInce;
    @auraEnabled
    Public List<Event_Attendee__c>  allAttend;
    @auraEnabled
    Public List<ContentDocument> allFiles;
}
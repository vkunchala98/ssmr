/** Class Name   : SMR_myAccountCntrl 
*  Description  : This class is used as a controller for SMR_myAccount Lightining component 
*  Created By   : BTG
*  Created On   : 31/12/2018
*
*  Modification Log:  
*  --------------------------------------------------------------------------------------------------------------------------------------
*   Developer                Date                   Modification ID      Description 
*  --------------------------------------------------------------------------------------------------------------------------------------                                                                                   
*  
**/
public class SMR_myAccountCntrl {
    
    @AuraEnabled
    public static string getAccountDetails() {
        try{
            List<User> lstUser = [Select u.ContactId from User u where u.Id = : UserInfo.getUserId()];
            if(lstUser[0].contactID!=null){
                Id contactb = lstUser[0].contactID;
                list<contact> lstContactinfo = [select Id,AccountId from Contact where Id = :contactb LIMIT 1];
                if(lstContactinfo[0].AccountId!=null){
                    Id accountb = lstContactinfo[0].AccountId;
                    return accountb;
                }
                return null;
            }
            return null;
        }catch(DmlException ex){
            // throw new AuraHandledException('User-defined error');
            throw new AuraHandledException(ex.getMessage());
        }
    }
}
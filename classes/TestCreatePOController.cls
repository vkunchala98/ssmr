@isTest
private class TestCreatePOController {

    static testMethod void myUnitTest() {
        
      List<RecordType> recIdList = [SELECT Id,Name FROM RecordType WHERE DeveloperName = 'Customer' AND SobjectType ='Account'];
      Account acc = new Account();
      acc.Name = 'Test Account';
      acc.RecordTypeId = recIdList[0].Id;
      Insert acc;
      
      recIdList = [SELECT Id,Name FROM RecordType WHERE DeveloperName = 'Vendor' AND SobjectType ='Account'];
      Account acc1 = new Account();
      acc1.Name = 'Test Account';
      acc1.RecordTypeId = recIdList[0].Id;
      Insert acc1;
      
      Store_Order__c st = new Store_Order__c();
      st.Account__c = acc.Id;
      Insert st;
      
      Item__c it = new Item__c();
      it.Vendor__c = acc1.id;
      it.Item_Status__c = 'Active';
      it.Item_UPC__c = 'x-xxxxx-xxxxx-x';
      it.Name = 'testItem135';
      it.Item__c = 'testItem145';
      it.Cost_Per_Deal__c = 23;
      it.Suggested_Retail_Price__c = 20.98;
      it.Case_UPC__c = 'x-xxxxx-xxxxx-x';
      insert it;
      
      Item_Component__c itemComponent = new Item_Component__c();
      itemComponent.Name = 'testItem';
      itemComponent.Item__c = it.Id;
      itemComponent.Item_UPC__c = 'x-xxxxx-xxxxx-x';
      itemComponent.Quantity__c = 5;
      itemComponent.Unit_Cost__c = 20;
      itemComponent.Suggested_Retail__c = '12';
      insert itemComponent;
      
      Order_Line_Items__c oli= new Order_Line_Items__c();
      oli.Store_Order__c = st.Id;
      oli.Store_Product__c = it.Id;
      oli.Ship_To__c = acc.Id;
      oli.Bill_To__c =acc.Id;
      insert oli;
      
      Order_Line_Items__c oli1= new Order_Line_Items__c();
      oli1.Store_Order__c = st.Id;
      oli1.Store_Product__c = it.Id;
      insert oli1;
      CreatePOController createPO = new CreatePOController();
      CreatePOController.CreatePO(st.id);
    }
}
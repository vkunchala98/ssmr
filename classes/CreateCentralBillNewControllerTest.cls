@isTest
public class CreateCentralBillNewControllerTest{

    public static TestMethod void CreateCentralBillInvoice() {

        TestUtilities.makeCustomSettings();
    
        RecordType cusRecType = [SELECT Id FROM RecordType WHERE DeveloperName = 'Customer' AND SobjectType ='Account'];
        RecordType vendorRecType = [SELECT Id FROM RecordType WHERE DeveloperName = 'Vendor' AND SobjectType ='Account'];
        
        List<Account> accList = new List<Account>{
            new Account(Name = 'Test Account1',RecordTypeId = cusRecType.Id,Service_charge__c = 5),
            new Account(Name = 'Test Account',RecordTypeId = vendorRecType.Id) 
        };
        Insert accList;
        
        List<Item__c> itemList = new List<Item__c>{
                                                    new Item__c(Vendor__c = accList[1].Id,Item_Status__c = 'Active',Item_UPC__c = 'x-xxxxx-xxxxx-x',
                                                                Name = 'testItem',Item__c = 'testItem',Cost_Per_Deal__c = 23, Case_UPC__c='x-xxxxx-xxxxx-x')
                                                
                                                };
        Insert itemList ;
        
        List<Store_Order__c> stOrdList = new List<Store_Order__c>{
                                                                      new Store_Order__c(Account__c = accList[0].Id,Bill_Types__c='Central Bill',Bill_To__c=accList[0].Id)
                                                                 };
        Insert stOrdList;
        
        List<Order_Line_Items__c > oliItemList = new List<Order_Line_Items__c >{
                                                                                new Order_Line_Items__c(Store_Order__c = stOrdList[0].Id,Store_Product__c = itemList[0].Id),
                                                                                new Order_Line_Items__c(Store_Order__c = stOrdList[0].Id,Store_Product__c = itemList[0].Id)                                                                       
                                                                            };
        Insert oliItemList;
        
        List<PO__c> poList = new List<PO__c>{
                                                new PO__c(Account__c = accList[1].Id,Bill_To__c = accList[0].Id,Store_Order__c = stOrdList[0].Id)
                                            };
        Insert poList;
         system.debug('------poList---------'+poList);
        List<PO_Line_Item__c> poliList = new List<PO_Line_Item__c>{
              new PO_Line_Item__c(Store_Order__c = stOrdList[0].Id,PO__c=poList[0].Id,Registered__c= true,Order_Line_Item__c=oliItemList[0].Id,SMR_Customer_Invoice__c = null),
              new PO_Line_Item__c(Store_Order__c = stOrdList[0].Id,PO__c=poList[0].Id,Registered__c= true,Order_Line_Item__c=oliItemList[1].Id, SMR_Vendor_Invoice__c = null)
        
        };
        Insert poliList;

        ApexPages.StandardController sc = new ApexPages.StandardController(poList[0]);
        CreateCentralBillNewController cbi = new CreateCentralBillNewController(sc);

		PageReference pageRef = Page.CreateCentralBillInvoice; // Add your VF page Name here
		pageRef.getParameters().put('id', String.valueOf(poList[0].Id));
		Test.setCurrentPage(pageRef);


        
        CreateCentralBillNewController.createCentralBillInvoice();
    
    }


}
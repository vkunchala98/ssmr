/**
 * This class contains utility methods for tasks such as test data creation
 */
public with sharing class TestUtilities {
	
    // global describe calls are expensive so use a cached copy of the results, if available or cache the results if not
	public static Map <String, Schema.SObjectType> schemaMap;
	
	// return a set of field API names for the specified object
    public static Set<String> getFieldNameList(String objectName)
    {
    	Set<String> fieldSet = new Set<String>();
    	
    	if(schemaMap == null){
    		schemaMap = Schema.getGlobalDescribe();
    	}
    	
    	Schema.SObjectType sobjectType = schemaMap.get(objectName);
    	if(sobjectType != null)
    	{
	    	Map<String, Schema.SObjectField> fieldMap = sobjectType.getDescribe().fields.getMap();
	    	for(Schema.SObjectField soField : fieldMap.values()){
	    		fieldSet.add(soField.getDescribe().getName());
	    	}
    	}
    	
    	return fieldSet;
    }
    
    // convert the list of SObjects into a CSV string
    public static String convertToCSV(List<String> fieldList, List<SObject> sobjectList)
    {
    	String csvString = '';
    	List<String> csvRowList = new List<String>();
    	
    	if(schemaMap == null){
    		schemaMap = Schema.getGlobalDescribe();
    	}
    	
    	// add the header row
    	csvRowList.add(String.join(fieldList, ','));
    	
		// process the rows provided
		for(SObject soRecord : sobjectList)
		{
			// for every field in the header, find the value on the current record
			List<String> fieldValueList = new List<String>();
			for(String fieldName : fieldList)
			{
				// if the field is a nested value then break the name apart into a path and follow it to get the parent object containing the field
				List<String> fieldPathParts = fieldName.split('\\.');
				SObject fieldOb = soRecord;
				String fieldKey = fieldPathParts[fieldPathParts.size()-1];
				for(Integer ii=0; ii < fieldPathParts.size()-1; ++ii)
				{
					if(fieldOb != null){
						fieldOb = (SObject)fieldOb.getSObject(fieldPathParts[ii]);
					}
					else{
						break;
					}
				}
				
				// get the field value and cast it to a string
				String fieldValue = (fieldOb != null ? (String)('' + fieldOb.get(fieldKey)) : '');
				if(fieldValue != null && fieldValue != '' && fieldValue != 'null'){
					fieldValue = fieldValue.escapeCsv();
				}
				else{
					fieldValue = '""';
				}
				
				
				fieldValueList.add(fieldValue);
			}
			csvRowList.add(String.join(fieldValueList, ','));
		}
		csvString += String.join(csvRowList, '\n');
		
		return csvString;
    }
    
    /**
     * find the alphabetic range of values based on the first letter of the values provided (Ex: {'apple', 'orange', 'zebra'} -> {'A', 'Z'}, {'apple', 'avalanche'} -> {'A'})
     */
    public static List<String> findAlphaRange(List<String> valueList)
    {
    	if(valueList.isEmpty()){
    		return new List<String>();
    	}
    	
    	List<String> alphaRangeList = new List<String>();
    	List<String> sortedList = new List<String>(valueList);
    	sortedList.sort();
    	
    	alphaRangeList.add(sortedList[0].toUpperCase().substring(0,1));
    	if(sortedList.size() > 1)
    	{
    		String firstLetter = sortedList[0].toUpperCase().substring(0,1);
    		String lastLetter = sortedList[sortedList.size()-1].toUpperCase().substring(0,1);
    		
    		if(firstLetter != lastLetter){
    			alphaRangeList.add(lastLetter);
    		}
    	}
    	
    	return alphaRangeList;
    }
	
	public static Account getVendorAccount()
	{
		Account a = new Account();
		a.Name = 'test';
		a.Type = 'Vendor';
		return a;
	}

	public static Item__c getItem(Id accountId)
	{
		Item__c i = new Item__c();
		i.Vendor__c = accountId;
		i.Item__c = 'test';
		i.Type_of_display__c = 'Case Good';
		i.Name = 'test';
		i.Cost_Per_Deal__c = 0;
		i.Pack_Per_Deal__c = 0;
		i.Case_Weight__c = 0;
		i.Case_Cube__c = 0;
		i.Case_Dimension_LxWxH__c = '0';
		i.Ti_Hi__c = '0';
		return i;
	}

	public static void makeCustomSettings()
	{
		PO_RT__c p1 = new PO_RT__c();
		p1.Name = 'Assorted';
		p1.Id__c = '01255000000CmSv';
		insert p1;

		PO_RT__c p2 = new PO_RT__c();
		p2.Name = 'Central Bill';
		p2.Id__c = '012U0000000aSwa';
		insert p2;

		PO_RT__c p3 = new PO_RT__c();
		p3.Name = 'Direct Bill';
		p3.Id__c = '012U0000000aSwV';
		insert p3;
		
		Box_com_Integration__c boxSettings = Box_com_Integration__c.getOrgDefaults();
        boxSettings.Client_Secret__c = 'b2BEkAM0ag1GnaGgschhKVYT0LVS1ZO2';
        boxSettings.Client_ID__c = 'oev97nrady0rh89qxz259g0mbi43dae0';
        boxSettings.Refresh_Token__c = 'J7rxTiWOHMoSC1isKZKBZWizoRXjkQzig5C6jFgCVJ9bUnsUfGMinKBDLZWP9BgR';
        boxSettings.Last_Refresh__c = Datetime.now();
        boxSettings.Redirect_URI__c = 'https://staging-smroauth.cs30.force.com/';
        insert boxSettings;
	}
	
	/**
	 * Generate accounts
	 */
	private static Integer seq_account = 0;
	public static Map<String, Schema.RecordTypeInfo> accountRecordTypeMap = new Map<String, Schema.RecordTypeInfo>();
	
	public static Map<String, Schema.RecordTypeInfo> getAccountRecordTypes()
	{
		if(accountRecordTypeMap.isEmpty()){
			accountRecordTypeMap = Schema.SObjectType.Account.getRecordTypeInfosByName();
		}
		
		return accountRecordTypeMap;
	}
	
	public static List<Account> generateAccounts(Integer quantity)
	{
		List<Account> accountList = new List<Account>();
		
		for(Integer ii=seq_account; ii < seq_account + quantity; ++ii)
		{
			Account account = new Account(
				Name = 'Test Account ' + ii,
				ShippingStreet = '3418 Northern Blvd',
				ShippingCity = 'Long Island City',
				ShippingState = 'NY',
				ShippingPostalCode = '11101',
				ShippingCountry = 'USA'
			);
			accountList.add(account);
		}
		seq_account += quantity;
		
		return accountList;
	}
	
	public static List<Account> generateVendorAccounts(Integer quantity)
	{
		List<Account> accountList = TestUtilities.generateAccounts(quantity);
		TestUtilities.getAccountRecordTypes();
		Id recordTypeId = accountRecordTypeMap.get('Vendor').getRecordTypeId();
		
		for(Integer ii=0; ii < accountList.size(); ++ii)
		{
			Account account = accountList[ii];
			account.Name = 'Test Vendor Account ' + ii;
			account.RecordTypeId = recordTypeId;
			account.Type = 'Vendor';
		}
		
		return accountList;
	}
	
	public static List<Account> generateCustomerAccounts(Integer quantity)
	{
		List<Account> accountList = TestUtilities.generateAccounts(quantity);
		TestUtilities.getAccountRecordTypes();
		Id recordTypeId = accountRecordTypeMap.get('Customer').getRecordTypeId();
		
		for(Integer ii=0; ii < accountList.size(); ++ii)
		{
			Account account = accountList[ii];
			account.Name = 'Test Customer Account ' + ii;
			account.RecordTypeId = recordTypeId;
		}
		
		return accountList;
	}
	
	
	/**
	 * Generate contacts
	 */
	private static Integer seq_contact = 0;
	
	public static List<Contact> generateContacts(Integer quantity)
	{
		List<Contact> contactList = new List<Contact>();
		
		for(Integer ii=seq_contact; ii < seq_contact + quantity; ++ii)
		{
			Contact contact = new Contact(
				FirstName = 'Test',
				LastName = 'Contact ' + ii,
				Email = 'contact' + ii + '@btg.com'
			);
			contactList.add(contact);
		}
		seq_contact += quantity;
		
		return contactList;
	}
	
	
	/**
	 * Generate store orders
	 */
    private static Integer seq_store_order = 0;
	
	public static List<Store_Order__c> generateStoreOrders(Integer quantity)
	{
		List<Store_Order__c> storeOrderList = new List<Store_Order__c>();
		
		for(Integer ii=seq_store_order; ii < seq_store_order + quantity; ++ii)
		{
			Store_Order__c storeOrder = new Store_Order__c(
			//	Name = 'Test Store Order ' + ii
			);
			storeOrderList.add(storeOrder);
		}
		seq_store_order += quantity;
		
		return storeOrderList;
	}
	
	
	/**
	 * Generate order line items
	 */
    private static Integer seq_order_line_item = 0;
	
	public static List<Order_Line_Items__c> generateOrderLineItems(Integer quantity)
	{
		List<Order_Line_Items__c> orderLineItemList = new List<Order_Line_Items__c>();
		
		for(Integer ii=seq_order_line_item; ii < seq_order_line_item + quantity; ++ii)
		{
			Order_Line_Items__c orderLineItem = new Order_Line_Items__c(
			//	Name = 'Test Order Line Item ' + ii
			);
			orderLineItemList.add(orderLineItem);
		}
		seq_order_line_item += quantity;
		
		return orderLineItemList;
	}
	
	
	/**
	 * Generate order line item components
	 */
    private static Integer seq_order_line_item_component = 0;
	
	public static List<Order_Line_Item_Component__c> generateOrderLineItemComponents(Integer quantity)
	{
		List<Order_Line_Item_Component__c> orderLineItemComponentList = new List<Order_Line_Item_Component__c>();
		
		for(Integer ii=seq_order_line_item_component; ii < seq_order_line_item_component + quantity; ++ii)
		{
			Order_Line_Item_Component__c orderLineItemComponent = new Order_Line_Item_Component__c(
			//	Name = 'Test Order Line Item ' + ii
			);
			orderLineItemComponentList.add(orderLineItemComponent);
		}
		seq_order_line_item_component += quantity;
		
		return orderLineItemComponentList;
	}
	
	
	/**
	 * Generate POs
	 */
    private static Integer seq_po = 0;
	public static Map<String, Schema.RecordTypeInfo> poRecordTypeMap = new Map<String, Schema.RecordTypeInfo>();
	
	public static Map<String, Schema.RecordTypeInfo> getPoRecordTypes()
	{
		if(poRecordTypeMap.isEmpty()){
			poRecordTypeMap = Schema.SObjectType.PO__c.getRecordTypeInfosByName();
		}
		
		return poRecordTypeMap;
	}
	
	public static List<PO__c> generatePOs(Integer quantity)
	{
		List<PO__c> poList = new List<PO__c>();
		
		for(Integer ii=seq_po; ii < seq_po + quantity; ++ii)
		{
			PO__c po = new PO__c(
			//	Name = 'Test PO ' + ii
			);
			poList.add(po);
		}
		seq_po += quantity;
		
		return poList;
	}
	
	
	/**
	 * Generate PO line items
	 */
    private static Integer seq_po_line_item = 0;
	
	public static List<PO_Line_Item__c> generatePOlineItems(Integer quantity)
	{
		List<PO_Line_Item__c> poLineItemList = new List<PO_Line_Item__c>();
		
		for(Integer ii=seq_po_line_item; ii < seq_po_line_item + quantity; ++ii)
		{
			PO_Line_Item__c poLineItem = new PO_Line_Item__c(
			//	Name = 'Test PO Line Item ' + ii
			);
			poLineItemList.add(poLineItem);
		}
		seq_po_line_item += quantity;
		
		return poLineItemList;
	}
	
	
	/**
	 * Generate PO line item components
	 */
    private static Integer seq_po_line_item_component = 0;
	
	public static List<PO_Line_Item_Component__c> generatePOlineItemComponents(Integer quantity)
	{
		List<PO_Line_Item_Component__c> poLineItemComponentList = new List<PO_Line_Item_Component__c>();
		
		for(Integer ii=seq_po_line_item_component; ii < seq_po_line_item_component + quantity; ++ii)
		{
			PO_Line_Item_Component__c poLineItemComponent = new PO_Line_Item_Component__c(
			//	Name = 'Test PO Line Item Component ' + ii
			);
			poLineItemComponentList.add(poLineItemComponent);
		}
		seq_po_line_item_component += quantity;
		
		return poLineItemComponentList;
	}
	
	
	/**
	 * Generate items
	 */
    private static Integer seq_item = 0;
	
	public static List<Item__c> generateItems(Integer quantity)
	{
		List<Item__c> itemList = new List<Item__c>();
		
		for(Integer ii=seq_item; ii < seq_item + quantity; ++ii)
		{
			Item__c item = new Item__c(
				Item__c = 'test',
				Type_of_display__c = 'Case Good',
				Name = 'test',
				Cost_Per_Deal__c = 0,
				Pack_Per_Deal__c = 0,
				Case_Weight__c = 0,
				Case_Cube__c = 0,
				Case_Dimension_LxWxH__c = '0',
				Ti_Hi__c = '0'
			);
			itemList.add(item);
		}
		seq_item += quantity;
		
		return itemList;
	}
	
	
	/**
	 * Generate item components
	 */
    private static Integer seq_item_components = 0;
	
	public static List<Item_Component__c> generateItemComponents(Integer quantity)
	{
		List<Item_Component__c> itemComponentList = new List<Item_Component__c>();
		
		for(Integer ii=seq_item_components; ii < seq_item_components + quantity; ++ii)
		{
			Item_Component__c item = new Item_Component__c(
				Name = 'Test item component ' + ii,
				Category__c = 'Test category ' + ii,
				Quantity__c = 1,
				Unit_Cost__c = 100,
				Item_UPC__c = 'itemUPC' + ii,
				Item_Number__c = 'itemNumber' + ii,
				Suggested_Retail__c = 'suggested retail ' + ii
			);
			itemComponentList.add(item);
		}
		seq_item_components += quantity;
		
		return itemComponentList;
	}
	
	
	/**
	 * Generate show carts
	 */
    private static Integer seq_show_cart = 0;
	
	public static List<Show_Cart__c> generateShowCarts(Integer quantity)
	{
		List<Show_Cart__c> showCartList = new List<Show_Cart__c>();
		
		for(Integer ii=seq_show_cart; ii < seq_show_cart + quantity; ++ii)
		{
			Show_Cart__c showCart = new Show_Cart__c(
				Status__c = 'Saved by Customer'
			);
			showCartList.add(showCart);
		}
		seq_show_cart += quantity;
		
		return showCartList;
	}
	
	
	/**
	 * Generate show cart line items
	 */
    private static Integer seq_show_cart_line_items = 0;
	
	public static List<Show_Cart_Line_Item__c> generateShowCartLineItems(Integer quantity)
	{
		List<Show_Cart_Line_Item__c> showCartLineItemList = new List<Show_Cart_Line_Item__c>();
		
		for(Integer ii=seq_show_cart_line_items; ii < seq_show_cart_line_items + quantity; ++ii)
		{
			Show_Cart_Line_Item__c showCartLineItem = new Show_Cart_Line_Item__c(
				Arrival_Date__c = Date.today(),
				Quantity__c = 1
			);
			showCartLineItemList.add(showCartLineItem);
		}
		seq_show_cart_line_items += quantity;
		
		return showCartLineItemList;
	}
	
	
	/**
	 * Generate invoices
	 */
    private static Integer seq_invoice = 0;
	
	public static List<Invoice__c> generateInvoices(Integer quantity)
	{
		List<Invoice__c> invoiceList = new List<Invoice__c>();
		
		for(Integer ii=seq_invoice; ii < seq_invoice + quantity; ++ii)
		{
			Invoice__c invoice = new Invoice__c(
				
			);
			invoiceList.add(invoice);
		}
		seq_invoice += quantity;
		
		return invoiceList;
	}
	
	
	/**
	 * Generate invoice line items
	 */
    private static Integer seq_invoiceLineItem = 0;
	
	public static List<Invoice_Line_Item__c> generateInvoiceLineItems(Integer quantity)
	{
		List<Invoice_Line_Item__c> invoiceLineItemList = new List<Invoice_Line_Item__c>();
		
		for(Integer ii=seq_invoiceLineItem; ii < seq_invoiceLineItem + quantity; ++ii)
		{
			Invoice_Line_Item__c invoiceLineItem = new Invoice_Line_Item__c(
				
			);
			invoiceLineItemList.add(invoiceLineItem);
		}
		seq_invoiceLineItem += quantity;
		
		return invoiceLineItemList;
	}
	
	
	/**
	 * Generate invoice line item components
	 */
    private static Integer seq_invoiceLineItemComponent = 0;
	
	public static List<Invoice_Line_Item_Component__c> generateInvoiceLineItemComponents(Integer quantity)
	{
		List<Invoice_Line_Item_Component__c> invoiceLineItemComponentList = new List<Invoice_Line_Item_Component__c>();
		
		for(Integer ii=seq_invoiceLineItemComponent; ii < seq_invoiceLineItemComponent + quantity; ++ii)
		{
			Invoice_Line_Item_Component__c invoiceLineItemComponent = new Invoice_Line_Item_Component__c(
				
			);
			invoiceLineItemComponentList.add(invoiceLineItemComponent);
		}
		seq_invoiceLineItemComponent += quantity;
		
		return invoiceLineItemComponentList;
	}
	
}
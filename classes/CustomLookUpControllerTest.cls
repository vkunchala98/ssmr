@isTest
public class CustomLookUpControllerTest {
    static testMethod void fetchLookUpValuesMethodTest() {
        Account aCustomer = new Account();
        aCustomer.Name = 'customerTest';
        aCustomer.Type = 'Customer';
        aCustomer.Direct_or_Central_Bill__c = 'Central Warehouse';        
        insert aCustomer;
        
        Test.startTest();
        List<Account> accList = customLookUpController.fetchLookUpValues('customerTest', 'Account', 'Name', '', 'Name');
        Test.stopTest();
        
        system.assertEquals(accList.size(), 1);
        system.assertEquals(accList[0].Name, 'customerTest');
    }
}
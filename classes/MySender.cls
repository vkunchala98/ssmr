/* This sample shows how to use the Loop.LoopMessage class in the context of
 * an Apex Trigger. The Trigger, Class, and Test Class below are defined separately.
 * All queries, objects, and fields are samples and can be replaced with the appropriate references.
 */


global class MySender {
   /* public class NoDDPInfoException extends Exception { }
    
    @future(callout=true)
    private static void sendRequests(string encodedObjects, string sessionId) {
        // CHANGE ANY FILTER (WHERE) CRITERIA NECESSARY
        List<Loop__DDP__c> ddps = [SELECT Id, Name, (SELECT Id,name FROM Loop__Custom_Integration_Options__r where name='Attach') FROM Loop__DDP__c where name='TestDocument'];
        
        // ALTERNATIVELY, DETERMINE WHICH DDP TO RUN FOR EACH RECORD
        
        if (ddps == null || ddps.size() < 1 || ddps[0].Loop__Custom_Integration_Options__r == null || ddps[0].Loop__Custom_Integration_Options__r.size() < 1) {
            // CHANGE THE EXCEPTION MESSAGE IF DESIRED
            throw new NoDDPInfoException('The DDP or Delivery Option specified was not found.');
        }
        
        Map<Id, sObject > mySObjects = (Map<Id, sObject >)JSON.deserialize(encodedObjects, Map<Id, sObject >.class );
        
        Loop.loopMessage lm = new Loop.loopMessage();
        
        // SESSION ID NEEDED IF IT CANNOT BE DETERMINED FROM UserInfo.getSessionId()
        lm.sessionId = sessionId;
        
        for (Id sObjectId : mySObjects.keySet()) {
            sObject mySObject = mySObjects.get(sObjectId);
            // ADD A DDP RUN REQUEST
            lm.requests.add(new Loop.loopMessage.loopMessageRequest(
                sObjectId, // MAIN RECORD ID - SAME OBJECT AS THE DDP RECORD TYPE SPECIFIES
                ddps[0].Id,
                new Map<string, string>{
                    'deploy' => ddps[0].Loop__Custom_Integration_Options__r[0].Id
                  //  'SFAccount' => mySObject.Contact__r.AccountId,
                   // 'SFContact' => mySObject.Contact__c
                    // THESE PARAMETERS ARE THE SAME AS THOSE FOUND IN OUR OUTBOUND MESSAGE DOCUMENTATION
                    // PLEASE REFERENCE THAT DOCUMENTATION FOR ADDITIONAL OPTIONS
                }
            ));
        }
        // SEND ALL DDP RUN REQUESTS IN A SINGLE CALL OUT
        lm.sendAllRequests();
    } */
}
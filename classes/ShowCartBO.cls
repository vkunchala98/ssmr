public without sharing class ShowCartBO {

	public ShowCartBO() {}

    private static ShowCartBO instance = null;

    public static ShowCartBO getInstance(){
        if (instance == null)
            instance = new ShowCartBO();

        return instance;
    }

    public ShowCartDTO buildCustomerShowCart(){

    	ShowCartDTO customerCartDTO = new ShowCartDTO();
    	String userId = UserInfo.getUserId();

    	List<User> userInfo = [SELECT Id, ContactId, Contact.AccountId FROM User WHERE Id = :userId];

    	List<X2019_Show_Cart__c> customerCart = ShowCartDAO.getCustomerShowCart(userInfo[0].Contact.AccountId);

    	if(customerCart.isEmpty() || customerCart == null){
    		customerCartDTO = createCustomerShowCart(userInfo[0].Contact.AccountId, userInfo[0].ContactId);
    	}else{
    		customerCartDTO = ShowCartDTO.buildCart(customerCart[0]);
    	}

    	return customerCartDTO;
    }

    public ShowCartDTO createCustomerShowCart(String customerId, String contactId){
    	X2019_Show_Cart__c newShowCartSO = new X2019_Show_Cart__c();

    	newShowCartSO.Account__c 			= customerId;
    	newShowCartSO.Customer_Contact__c 	= contactId;

    	insert newShowCartSO;  	

    	return ShowCartDTO.buildCart(newShowCartSO);
    }

	public List<VendorDTO> getBoothList(){

		List<VendorDTO> listVendorDTO = new List<VendorDTO>();
		List<Account> listVendorSO = ShowCartDAO.getVendorList();

		if(listVendorSO.size() > 0){
			listVendorDTO = VendorDTO.buildVendor(listVendorSO);
		}

		return listVendorDTO;			
	}

	public String addVendor(String cartId, String selectedVendor){
		X2019_Show_Visited_Vendor__c visitedVendor = new X2019_Show_Visited_Vendor__c();

		visitedVendor.Show_Cart__c = cartId;
		visitedVendor.Vendor__c = selectedVendor;

		try{
			Database.insert(visitedVendor);
			return 'SUCCESS';
		}catch(Exception e){
			return 'FAIL';
		}
	}
}
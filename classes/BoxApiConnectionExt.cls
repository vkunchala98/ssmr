public without sharing class BoxApiConnectionExt extends BoxApiConnection {
	
	private Box_com_Integration__c boxSettings;
	private Boolean initializing = true;
	public Boolean tokensRefreshed = false;
	
	public BoxApiConnectionExt(String accessToken) {
        this(null, null, accessToken, null);
    }

    public BoxApiConnectionExt(String clientId, String clientSecret) {
        this(clientId, clientSecret, null, null);
    }

    public BoxApiConnectionExt(String clientId, String clientSecret, String authCode) {
        this(clientId, clientSecret, null, null);
        this.authenticate(authCode);
    }
    
	public BoxApiConnectionExt(String clientId, String clientSecret, String accessToken, String refreshToken)
	{
		// use the default constructor first
		super(clientId, clientSecret, accessToken, refreshToken);
        
        // get custom setting values and merge them into this object instance
		this.boxSettings = Box_com_Integration__c.getOrgDefaults();
        if(this.boxSettings.Last_Refresh__c != null){
        	this.setLastRefresh(this.boxSettings.Last_Refresh__c.getTime());
        }
        if(accessToken == null && this.boxSettings.Access_Token__c != null){
        	this.setAccessToken(this.boxSettings.Access_Token__c);
        }
        if(accessToken == null && this.boxSettings.Refresh_Token__c != null){
        	this.setRefreshToken(this.boxSettings.Refresh_Token__c);
        }
		
        // this will use the refresh token to get a valid access token, if necessary
		this.initializing = false;
		this.getAccessToken();
        if((this.accessToken == null || this.accessToken == '') && this.refreshToken != null){
        	this.refresh();
        }
        if(this.tokensRefreshed){
        	saveCustomSetting();
        }
    }
    
    /**
     * Save updated custom setting values in this instance. This should be called manually after 
     */
    public void saveCustomSetting(){
    	upsert this.boxSettings;
    }
    
    /**
     * Set the access token. This is a version of the parent method that adds logic to update the custom setting
     */
    public override void setAccessToken(String accessToken)
    {
    	super.setAccessToken(accessToken);
    	if(!initializing && accessToken != this.boxSettings.Access_Token__c)
    	{
    		this.tokensRefreshed = true;
    		this.boxSettings.Access_Token__c = accessToken;
    	}
    }
    
    /**
     * Set the refresh token. This is a version of the parent method that adds logic to update the custom setting
     */
    public override void setRefreshToken(String refreshToken)
    {
    	super.setRefreshToken(refreshToken);
    	if(!initializing && refreshToken != this.boxSettings.Refresh_Token__c)
    	{
    		this.tokensRefreshed = true;
    		this.boxSettings.Refresh_Token__c = refreshToken;
    	}
    }
    
    /**
     * Set the last refresh info. This is a version of the parent method that adds logic to update the custom setting and updates the expiration date
     */
    public override void setLastRefresh(Long lastRefresh)
    {
        super.setLastRefresh(lastRefresh);
        
		Datetime lastRefreshDatetime = Datetime.newInstanceGmt(1970, 1, 1, 0, 0, 0);
		lastRefreshDatetime = lastRefreshDatetime.addSeconds(Integer.valueOf(lastRefresh / 1000));
    	Long newExpires = lastRefreshDatetime.addDays(59).getTime();
		this.setExpires(newExpires);
		
		if(!initializing){
			this.boxSettings.Last_Refresh__c = lastRefreshDatetime;
		}
    }
    
    /**
     * This is a utility method for copying API request exception data to a log object
     */
    public static void logHttpError(BoxApiRequest.BoxApiRequestException e, HttpRequestLogService.HRL_Entry requestLogEntry)
    {
    	String exceptionMessage = e.getMessage();
		if(exceptionMessage == 'The Box API responded with a 400 : Bad Request'){
			requestLogEntry.setResponseCode(400);
		}
		else if(exceptionMessage == 'The Box API responded with a 404 : Not Found'){
			requestLogEntry.setResponseCode(404);
		}
		requestLogEntry.setResponseBody(exceptionMessage);
    }
}
public class AddDeleteController {

    @AuraEnabled
    public Static List<String> getBillTypeAvailable(String recordId){

        // Direct_or_Central_Bill__c
        List<String> multiSelected = new List<String>();
        Account acc = [SELECT Id, Name, Direct_or_Central_Bill__c FROM Account Where Id =: recordId];

        String[] tmpString = acc.Direct_or_Central_Bill__c.split(';');
        for(String s : tmpString){
            multiSelected.add(s);
        }

        return multiSelected;
    }

    @AuraEnabled
    public Static String saveStoreOrders(List<Account> vendorList, List<Order_Line_Items__c> lineItemList, Id customerId, String billType, String masterShip){
        
        List<Store_Order__c> storeOrderList = new List<Store_Order__c>();
        List<PO__c> pOList = new List<PO__c>();
        List<PO_Line_Item__c> pOLineItemList = new List<PO_Line_Item__c>();        
		Map<Id, Store_Order__c> vendorIdStoreOrderMap = new Map<Id, Store_Order__c>();        
        Map<Id, List<Order_Line_Items__c>> vendorIdLineItemMap = new Map<Id, List<Order_Line_Items__c>>();
        Set<Id> itemIdSet = new Set<Id>();
        List<Order_Line_Items__c> aLineItemPerVendorList;

        Map<Id, Account> vendorPerVendorId = new Map<Id, Account>();
        
        
        System.debug('JIV vendorList: ' + vendorList);
        System.debug('JIV lineItemList: ' + lineItemList);
        System.debug('JIV lineItemList.size(): ' + lineItemList.size());
        System.debug('JIV ALL LINE ITEM LIST START: ');
        for(Order_Line_Items__c aLineItemElement : lineItemList){
        	System.debug('JIV aLineItemElement: ' + aLineItemElement);
        }
        System.debug('JIV customerId: ' + customerId);
        System.debug('JIV billType: ' + billType);
        System.debug('masterShip: ' + masterShip);

        Account customerAcc = [SELECT Id, OwnerId, Direct_or_Central_Bill__c FROM Account WHERE Id =: customerId];

        Set<String> vendorListId = new Set<String>();
        
        for(Account aVendor : vendorList){
            vendorListId.add(aVendor.Id);
        }

        List<Account> newVendorList = [SELECT Id, Name, Central_Billing__c, OwnerId FROM Account WHERE Id =: vendorListId];

        Store_Order__c aStoreOrder = new Store_Order__c();    

        for(Account aVendor : newVendorList){

            /*if(aVendor.Central_Billing__c == true && billType.contains('Direct')){
                System.debug('splitedType: ' + splitedType);
                aStoreOrder.Bill_Types__c = 'Central ' + splitedType[1];
            } else {
                aStoreOrder.Bill_Types__c = billType;
            } */

            aStoreOrder.Bill_To__c = customerId;
            aStoreOrder.Account__c = customerId;
            aStoreOrder.Sales_Rep__c = customerAcc.OwnerId;
            aStoreOrder.Bill_Types__c = billType;
            storeOrderList.add(aStoreOrder);
            vendorIdStoreOrderMap.put(aVendor.Id, aStoreOrder);
            vendorPerVendorId.put(aVendor.Id, aVendor);
        }

        
        System.debug('JIV storeOrderList(BEFORE INSERT): ' + storeOrderList);
        insert aStoreOrder;        
        System.debug('JIV storeOrderList(AFTER INSERT): ' + storeOrderList);       

            
        for(Order_Line_Items__c aLineItem : lineItemList){
            aLineItem.Bill_To__c = customerId;            
            aLineItem.Cost_per_Deal__c = aLineItem.Total_Deal_Cost__c;
			itemIdSet.add(aLineItem.Store_Product__c);         
        }
        
        for(Item__c anItem : [Select Id, Vendor__c, Case_UPC__c, Pack_Per_Deal__c, Type_of_display__c, Suggested_Retail_Price__c From Item__c Where Id in : itemIdSet]){
            if (vendorIdLineItemMap.containsKey(anItem.Vendor__c)) {
                aLineItemPerVendorList = vendorIdLineItemMap.get(anItem.Vendor__c);
            }else{
                aLineItemPerVendorList = new List<Order_Line_Items__c>();
            }
            for(Order_Line_Items__c aLineItem : lineItemList){
                if (aLineItem.Store_Product__c == anItem.Id){
                    aLineItem.Case_UPC__c = anItem.Case_UPC__c;
                    aLineItem.Deal_Pack__c = anItem.Pack_Per_Deal__c;
                    aLineItem.Type_of_Display__c = anItem.Type_of_display__c;
                 	aLineItemPerVendorList.add(aLineItem);   
                }    
            }
            vendorIdLineItemMap.put(anItem.Vendor__c, aLineItemPerVendorList);
        }      

        List<String> splitedType = new List<String>();
        splitedType = billType.split(' ');  

        System.debug('billType: ' + billType);
      	
        Store_Order__c storeOrder;
        Account vendorInfo;

        Map<Id, List<Order_Line_Items__c>> multipleShipToMap = new Map<Id, List<Order_Line_Items__c>>();
        Map<String, String> shipToMap = new Map<String, String>();
        Map<String, List<String>> shipToMapByOLI = new Map<String, List<String>>();
        Map<String, Store_Order__c> shippingMap = new Map<String, Store_Order__c>();

        Map<Date, List<Order_Line_Items__c>> mapByOLIDate = new Map<Date, List<Order_Line_Items__c>>();

        Map<Date, List<Id>> mapByDateShipTo = new Map<Date, List<Id>>();

        Map<String, List<Order_Line_Items__c>> mapCombined = new Map<String, List<Order_Line_Items__c>>();
        Map<String, String> mapCombinedIds = new Map<String, String>();

        Map<String, Account> vendorMapinfo = new Map<String, Account>();

        String combinedId = '';

        for(Id aVendorId : vendorIdStoreOrderMap.keySet()){
            storeOrder = vendorIdStoreOrderMap.get(aVendorId);
            vendorInfo = vendorPerVendorId.get(aVendorId);
            aLineItemPerVendorList = vendorIdLineItemMap.get(aVendorId);

            vendorMapinfo.put(vendorInfo.Id, vendorInfo);

            for(Order_Line_Items__c aLineItem : aLineItemPerVendorList){
                aLineItem.Store_Order__c = storeOrder.Id;

                combinedId = aVendorId+'--'+String.valueOf(aLineItem.Arrival_Date__c)+aLineItem.Ship_To__c;

                if(mapCombined.containsKey(combinedId)){
                    mapCombined.get(combinedId).add(aLineItem);                 
                }else{
                    mapCombined.put(combinedId,new List<Order_Line_Items__c>{aLineItem});
                    mapCombinedIds.put(combinedId, aLineItem.Ship_To__c);
                }

                if(mapByOLIDate.containsKey(aLineItem.Arrival_Date__c)){
                    mapByOLIDate.get(aLineItem.Arrival_Date__c).add(aLineItem);                 
                }else{
                    mapByOLIDate.put(aLineItem.Arrival_Date__c,new List<Order_Line_Items__c>{aLineItem});
                }

                if(aLineItem.Ship_To__c != null){
                    if(multipleShipToMap.containsKey(aLineItem.Ship_To__c)){
                        multipleShipToMap.get(aLineItem.Ship_To__c).add(aLineItem);                 
                    }else{
                        multipleShipToMap.put(aLineItem.Ship_To__c,new List<Order_Line_Items__c>{aLineItem});
                    }
                }

                if(mapByDateShipTo.containsKey(aLineItem.Arrival_Date__c)){
                    mapByDateShipTo.get(aLineItem.Arrival_Date__c).add(aLineItem.Ship_To__c);                 
                }else{
                    mapByDateShipTo.put(aLineItem.Arrival_Date__c,new List<Id>{aLineItem.Ship_To__c});
                }


                
            }

            /*PO__c aPo = new PO__c();            
            aPo.Store_Order__c = storeOrder.Id;
            aPo.Account__c = aVendorId;
            aPo.Bill_To__c = storeOrder.Bill_To__c;
            if(aLineItemPerVendorList[0].Ship_To__c == null){
                
                if(masterShip != '' && masterShip != null && masterShip != 'undefined'){
                    
                    aPo.Ship_To__c = masterShip;
                } else {
                    
                    aPo.Ship_To__c = storeOrder.Bill_To__c;
                }
            } else {
                aPo.Ship_To__c = aLineItemPerVendorList[0].Ship_To__c;
            }

            aPo.At_Once__c = aLineItemPerVendorList[0].At_Once__c;
            aPo.Arrival_Date__c = aLineItemPerVendorList[0].Arrival_Date__c;

            if(vendorInfo.Central_Billing__c == true && billType.contains('Direct')){
                aPo.Written_Bill_Type__c = 'Central ' + splitedType[1];
            } else {
                aPo.Written_Bill_Type__c = billType;
            }

            Id devRecordTypeId;
            if(aPo.Written_Bill_Type__c.contains('Central') == true){
                devRecordTypeId = Schema.SObjectType.PO__c.getRecordTypeInfosByName().get('Central Bill').getRecordTypeId();
            } else {
                devRecordTypeId = Schema.SObjectType.PO__c.getRecordTypeInfosByName().get('Direct Bill').getRecordTypeId();   
            }
            aPo.RecordTypeId = devRecordTypeId;
            pOList.add(aPo);*/
        }


        System.debug('JIV lineItemList(BEFORE INSERT): ' + lineItemList);
        insert lineItemList;
        System.debug('JIV lineItemList(AFTER INSERT): ' + lineItemList);

        System.debug('mapByDate: ' + mapByOLIDate + ' mapByOLIDate.size: ' + mapByOLIDate.size() + ' multipleShipToMap.size: ' + multipleShipToMap.size() + ' multipleShipToMap: ' + multipleShipToMap);

        Map<String, Order_Line_Items__c> lineItemMap = new Map<String, Order_Line_Items__c>();

        System.debug('mapByDateShipTo: ' + mapByDateShipTo);
        Map<String, PO__c> combinedPoIdMap = new Map<String, PO__c>();
        Map<integer,string> poLOIOrderMap = new Map<integer,string>();

        integer indexOLI = 0;
        
        for(String dt : mapCombined.keySet()){

            List<Order_Line_Items__c> oliList = mapCombined.get(dt);
            List<String> idList = dt.split('--');

            PO__c aPo = new PO__c();            
            aPo.Store_Order__c = storeOrder.Id;
            aPo.Account__c = idList[0];
            aPo.Bill_To__c = storeOrder.Bill_To__c;
            apo.Sales_Rep__c = storeOrder.Sales_Rep__c;
            if(oliList[0].Ship_To__c == null){
                
                if(masterShip != '' && masterShip != null && masterShip != 'undefined'){
                    
                    aPo.Ship_To__c = masterShip;
                } else {
                    
                    aPo.Ship_To__c = storeOrder.Bill_To__c;
                }
            } else {
                aPo.Ship_To__c = oliList[0].Ship_To__c;
            }

            aPo.At_Once__c = oliList[0].At_Once__c;
            aPo.Arrival_Date__c = oliList[0].Arrival_Date__c;

            Account vendor = vendorMapinfo.get(idList[0]);

            System.debug('vendor: ' + vendor);
            System.debug('billType2: ' + billType + ' splitedType: ' + splitedType);
            System.debug('vendor.Central_Billing__c: ' + vendor.Central_Billing__c + ' billType.contains: ' + billType.contains('Direct'));

            if(vendor.Central_Billing__c == true && billType.contains('Direct')){
                aPo.Written_Bill_Type__c = 'Central ' + splitedType[1];
            } else {
                aPo.Written_Bill_Type__c = billType;
            }

            Id devRecordTypeId;
            System.debug('aPo.Written_Bill_Type__c: ' + aPo.Written_Bill_Type__c);
            if(aPo.Written_Bill_Type__c.contains('Central') == true){
                devRecordTypeId = Schema.SObjectType.PO__c.getRecordTypeInfosByName().get('Central Bill').getRecordTypeId();
            } else {
                devRecordTypeId = Schema.SObjectType.PO__c.getRecordTypeInfosByName().get('Direct Bill').getRecordTypeId();   
            }
            aPo.RecordTypeId = devRecordTypeId;

            System.debug('aPo: ' + aPo);
            pOList.add(aPo);
            poLOIOrderMap.put(indexOLI,dt);
            indexOLI++;
        }


        System.debug('lineItemMap: ' + lineItemMap);
        
        System.debug('JIV pOList(BEFORE INSERT): ' + pOList);
        insert pOList;

        integer poCount = 0;
        for(PO__c po : pOList){
            combinedPoIdMap.put(poLOIOrderMap.get(poCount), po);
            poCount++;
        }
        System.debug('JIV pOList(AFTER INSERT): ' + pOList);

        Map<String, String> mapByMultipleShipTo = new Map<String, String>();

        for(Order_Line_Items__c oli : lineItemList){
            shipToMap.put(oli.Store_Order__c, oli.Id);
            //shipToMapByOLI.put(oli.Id, oli.Store_Order__c);

            if(oli.Ship_To__c != null ){
                mapByMultipleShipTo.put(oli.Ship_To__c, oli.Id);
            }

            if(shipToMapByOLI.containsKey(oli.Store_Order__c)){
                shipToMapByOLI.get(oli.Store_Order__c).add(oli.Id);                 
            }else{
                shipToMapByOLI.put(oli.Store_Order__c,new List<String>{oli.Id});
            }

            //shippingMap.put(oli.Id, oli.Store_Order__c);
            System.debug('aLineItem acc: ' + oli.Store_Order__r.Bill_To__c + ' oli: ' + oli);
        }

        List<Store_Order__c> soList = [SELECT Id, Name, Bill_To__c, Bill_To__r.Name FROM Store_Order__c WHERE Id in: shipToMap.keySet()];

        System.debug('soList: ' + soList);
        System.debug('shipToMap: ' + shipToMap);

        for(Store_Order__c o : soList){

            String olitem = shipToMap.get(o.Id);  

            List<String> storeIdList = shipToMapByOLI.get(o.Id);

            for(String str : storeIdList){
                shippingMap.put(str, o);
            }


            System.debug('olitem: ' + olitem);       

            //if(shippingMap.containsKey(olitem)){
            //    shippingMap.put(olitem).field = o.Bill_To__c;                 
            //}else{
            //}
            
        } 


        System.debug('shipToMap: ' + shipToMap + ' shippingMap: ' + shippingMap);

        Map<String, Account> mapByAccount = new Map<String, Account>();

        List<Account> accMapMultipleShipto = [SELECT Id, Name FROM Account WHERE Id in: mapByMultipleShipTo.keySet()];

        for(Account a : accMapMultipleShipto){
            mapByAccount.put(a.Id, a);
        }

        System.debug('mapCombined: ' + mapCombined);
        System.debug('mapCombinedid: ' + mapCombinedIds);
        
        Map<String, PO_Line_Item__c> poLineItemMap = new Map<String, PO_Line_Item__c>();

        System.debug('combinedPoIdMap: ' + combinedPoIdMap);

        for(String dt : mapCombined.keySet()){

            List<Order_Line_Items__c> oliList = mapCombined.get(dt);
            List<String> idList = combinedId.split('--');

            PO__c po = combinedPoIdMap.get(dt);

            for(Order_Line_Items__c aLineItem : oliList){

                PO_Line_Item__c aNewPoLineItem = new PO_Line_Item__c();
                aNewPoLineItem.Account__c = aLineItem.Bill_To__c;
                aNewPoLineItem.Order_Line_Item__c = aLineItem.Id;
                aNewPoLineItem.Notes__c = aLineItem.Notes__c;
                aNewPoLineItem.Case_UPC__c = aLineItem.Case_UPC__c;
                aNewPoLineItem.Deal_Pack__c = aLineItem.Deal_Pack__c;
                aNewPoLineItem.Type_of_Display__c = aLineItem.Type_of_Display__c;
                //aNewPoLineItem.Item_Number__c = aLineItem.Item_Number__c;
                aNewPoLineItem.PO__c = po.Id;
                aNewPoLineItem.Quantity__c = aLineItem.Quantity__c;
                aNewPoLineItem.Cost_per_Deal__c = (aLineItem.Total_Deal_Cost__c / aLineItem.Quantity__c);
                aNewPoLineItem.Store_Order__c = po.Store_Order__c;
                aNewPoLineItem.Store_Product__c = aLineItem.Store_Product__c;
                poLineItemMap.put(aLineItem.Id, aNewPoLineItem); 
            }

            System.debug('testpo: ' + po);
        }

        //PO LINE ITEMS PART:
        /*for(PO__c aCreatedPO : pOList){
            aLineItemPerVendorList = vendorIdLineItemMap.get(aCreatedPO.Account__c);
            for(Order_Line_Items__c aLineItem : aLineItemPerVendorList){

                System.debug('aLineItem: ' + aLineItem);
                PO_Line_Item__c aNewPoLineItem = new PO_Line_Item__c();
                aNewPoLineItem.Account__c = aLineItem.Bill_To__c;
                aNewPoLineItem.Order_Line_Item__c = aLineItem.Id;
                aNewPoLineItem.Notes__c = aLineItem.Notes__c;
                aNewPoLineItem.Case_UPC__c = aLineItem.Case_UPC__c;
                aNewPoLineItem.Deal_Pack__c = aLineItem.Deal_Pack__c;
                aNewPoLineItem.Type_of_Display__c = aLineItem.Type_of_Display__c;
                //aNewPoLineItem.Item_Number__c = aLineItem.Item_Number__c;
                aNewPoLineItem.PO__c = aCreatedPO.Id;
                aNewPoLineItem.Quantity__c = aLineItem.Quantity__c;
                aNewPoLineItem.Cost_per_Deal__c = (aLineItem.Total_Deal_Cost__c / aLineItem.Quantity__c);
                aNewPoLineItem.Store_Order__c = aCreatedPO.Store_Order__c;
                aNewPoLineItem.Store_Product__c = aLineItem.Store_Product__c;
                poLineItemMap.put(aLineItem.Id, aNewPoLineItem);          
                //pOLineItemList.add(aNewPoLineItem);
            }	    
        }*/
        System.debug('JIV pOLineItemList(BEFORE INSERT): ' + pOLineItemList);

        Map<String, String> pliWithOLI = new Map<String, String>();
        List<Order_Line_Items__c> oliList = [SELECT Id, Name, Item_Number__c, Store_Product__c FROM Order_Line_Items__c WHERE Id =: poLineItemMap.keySet()];

        for(Order_Line_Items__c oli : oliList){
            PO_Line_Item__c newPoItem = poLineItemMap.get(oli.Id);
            newPoItem.Item_Number__c = oli.Item_Number__c;
            pOLineItemList.add(newPoItem);
            pliWithOLI.put(newPoItem.Order_Line_Item__c, newPoItem.PO__c);
            System.debug('item number: ' + newPoItem);
        }  
        insert pOLineItemList;


        List<OrderLineItem_ShipTo__c> setOLIShipTo = new List<OrderLineItem_ShipTo__c>();

        for(Order_Line_Items__c aLineItem : lineItemList){
        
            Store_Order__c sOrder = shippingMap.get(aLineItem.Id);
            String poId = pliWithOLI.get(aLineItem.Id);



            System.debug('storeOrder: ' + sOrder);
            //for(String accId : accIdList){
            OrderLineItem_ShipTo__c OLIShipTo = new OrderLineItem_ShipTo__c();
            OLIShipTo.Order_Line_Item__c = aLineItem.Id;


            if(aLineItem.Ship_To__c != null){
                Account acc = mapByAccount.get(aLineItem.Ship_To__c);
                OLIShipTo.Account__c = acc.Id;
                OLIShipTo.Name = acc.Name;
            } else {
                OLIShipTo.Account__c = sOrder.Bill_To__c;
                OLIShipTo.Name = sOrder.Bill_To__r.Name;
            }

            OLIShipTo.PO__c = poId;
            setOLIShipTo.add(OLIShipTo);
            //}
        }

        System.debug('JIV setOLIShipTo(BEFORE INSERT): ' + setOLIShipTo);
        try{
            insert setOLIShipTo;
        }
        catch(DmlException e){
            System.debug('error: ' + e.getMessage());
        } 
        System.debug('JIV setOLIShipTo(AFTER INSERT): ' + setOLIShipTo);



    	System.debug('JIV pOLineItemList(AFTER INSERT): ' + pOLineItemList);

        List<Item_Component__c> itemComponentList = new List<Item_Component__c>([select id, Name, Item__c, Cost_Per_Deal__c, Unit_Cost__c, Quantity__c, Suggested_Retail__c, Item_UPC__c   
                                                                                    from Item_Component__c where Item__c IN:itemIdSet]);

        Map<string, List<Item_Component__c>> itemComponentMap = new Map<string, List<Item_Component__c>>();
        List<Order_Line_Item_Component__c> oliLineItemComList = new List<Order_Line_Item_Component__c>();

        System.debug('itemComponentList: ' + itemComponentList);

        for(Item_Component__c o : itemComponentList){            

            if(itemComponentMap.containsKey(o.Item__c)){
                itemComponentMap.get(o.Item__c).add(o);                 
            }else{
                itemComponentMap.put(o.Item__c,new List<Item_Component__c>{o});
            }
            
        } 

        for(Order_Line_Items__c aLineItem : lineItemList){

            if(itemComponentMap.get(aLineItem.Store_Product__c) != null){                
                List<Item_Component__c> oliComListRec = itemComponentMap.get(aLineItem.Store_Product__c);
                
                for(Item_Component__c o : oliComListRec){
                    o.Unit_Cost__c = (o.Unit_Cost__c != null)? o.Unit_Cost__c : 0;

                    decimal quantityDeduct = (aLineItem.Discount__c != null) ? (aLineItem.Discount__c * o.Unit_Cost__c)/100 : 0;
                    decimal dealCost  = o.Unit_Cost__c - quantityDeduct;

                    oliLineItemComList.add(new Order_Line_Item_Component__c(
                        Quantity__c = o.Quantity__c,
                        Item_Component_UPC__c = o.Item_UPC__c,
                        Unit_Cost__c = dealCost,
                        Suggested_Retail__c = o.Suggested_Retail__c,
                        Order_Line_Item__c = aLineItem.id,
                        Item_Component_Description__c = o.Name
                    ));            
                }                    
            }

        }

        System.debug('oliLineItemComList: ' + oliLineItemComList);

        if(oliLineItemComList.size() > 0){
            insert oliLineItemComList;
        }

        Map<Id,List<Order_Line_Item_Component__c>> OLILineItemComMap = new Map<Id,List<Order_Line_Item_Component__c>>();

        for(Order_Line_Item_Component__c o : oliLineItemComList){
            if(OLILineItemComMap.containsKey(o.Order_Line_Item__c)){
                OLILineItemComMap.get(o.Order_Line_Item__c).add(o);
            }else{
                OLILineItemComMap.put(o.Order_Line_Item__c,new List<Order_Line_Item_Component__c>{o});
            }
        }

        List<PO_Line_Item_Component__c> POLineItemComList = new List<PO_Line_Item_Component__c>();
               
        for(PO_Line_Item__c po : pOLineItemList){

            System.debug('po: ' + po);

            if(OLILineItemComMap.get(po.Order_Line_Item__c) != null){

                List<Order_Line_Item_Component__c> oliComListRec = OLILineItemComMap.get(po.Order_Line_Item__c);

                System.debug('oliComListRec: ' + po);

                for(Order_Line_Item_Component__c o : oliComListRec){

                    System.debug('segundo for: ' + o);

                    POLineItemComList.add(new PO_Line_Item_Component__c(
                        Quantity__c = o.Quantity__c,
                        Item_Component_UPC__c = o.Item_Component_UPC__c,
                        Unit_Cost__c = o.Unit_Cost__c,
                        Suggested_Retail__c = o.Suggested_Retail__c,
                        PO_Line_Item__c = po.id,
                        Item_Component_Description__c = o.Item_Component_Description__c,
                        Order_Line_Item_Component__c = o.id
                    ));                 
                }                    
            }       
        } 

        System.debug('POLineItemComList: ' + POLineItemComList);

        if(POLineItemComList.size() > 0){
            insert POLineItemComList; 
        }


        return storeOrderList[0].Id;
    }
    
}
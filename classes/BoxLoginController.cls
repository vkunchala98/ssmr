public without sharing class BoxLoginController {
	
	public String authURL {get; set;}
	public Box_com_Integration__c boxSettings {get; set;}
	public String authCode {get; set;}
	public BoxApiConnection api {get; set;}
	
	public BoxLoginController()
	{
		// get the Box.com API settings
		boxSettings = Box_com_Integration__c.getOrgDefaults();
		
		// put together the URL for requesting the authorization code
		this.authURL = 'https://account.box.com/api/oauth2/authorize?response_type=code&client_id=' + EncodingUtil.urlEncode(boxSettings.Client_ID__c, 'UTF-8') 
					+ '&redirect_uri=' + EncodingUtil.urlEncode(boxSettings.Redirect_URI__c, 'UTF-8') 
					+ '&state=SMROAUTH';
		
		// if an auth code was passed back to the page then we can try to get the tokens and update the custom setting
		authCode = ApexPages.currentPage().getParameters().get('code');
		if(authCode != null && ApexPages.currentPage().getParameters().get('state') == 'SMROAUTH'){
			authCode = EncodingUtil.urlEncode(authCode, 'UTF-8');
		}
	}
	
	public void getTokens()
	{
		api = new BoxApiConnectionExt(boxSettings.Client_ID__c, boxSettings.Client_Secret__c, boxSettings.Access_Token__c, boxSettings.Refresh_Token__c);
		try{
			api.authenticate(authCode);
		}
		catch(Exception e){
			return;
		//	return e.getMessage();
		}
		
		DateTime lastRefreshDatetime = datetime.newInstanceGmt(1970, 1, 1, 0, 0, 0);
		lastRefreshDatetime = lastRefreshDatetime.addSeconds(Integer.valueOf(api.lastRefresh/1000));
		
		// save the tokens to the custom setting
        boxSettings.Access_Token__c = api.accessToken;
        boxSettings.Refresh_Token__c = api.refreshToken;
        boxSettings.Last_Refresh__c = lastRefreshDatetime;
        
    //    return 'Success';
	}
	
	public void saveOAuthTokens(){
        upsert boxSettings;
	}
    
}
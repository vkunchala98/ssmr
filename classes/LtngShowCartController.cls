global with sharing class LtngShowCartController {

	static ShowCartBO showCartBOInstance = ShowCartBO.getInstance();

	@AuraEnabled
	global static ShowCartDTO buildShowCart(){
		return showCartBOInstance.buildCustomerShowCart();
	}

	@AuraEnabled
	global static List<VendorDTO> getBoothList(){
		return showCartBOInstance.getBoothList();
	}

	@AuraEnabled 
	global static String addVendor(String cartId, String selectedVendor){
		return showCartBOInstance.addVendor(cartId, selectedVendor);
	}
}
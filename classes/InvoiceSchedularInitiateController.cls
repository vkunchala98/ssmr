public class InvoiceSchedularInitiateController {

    public InvoiceSchedularInitiateController() {
    
    
    }
    
    public void directInvoiceSchedular() {
      
        DirectInvoiceControllerBatch dicbs = new DirectInvoiceControllerBatch();
        Database.executeBatch(dicbs);
   
    } 
        
   
    
    public void vendorInvoiceSchedular() {
            
        VendorInvoiceControllerBatch vicbs = new VendorInvoiceControllerBatch();
        Database.executeBatch(vicbs);
   
    }


}
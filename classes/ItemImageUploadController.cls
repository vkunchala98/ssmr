public class ItemImageUploadController {

  public Id itemId;
  
  public ItemImageUploadController() {
  
      itemId = apexpages.currentpage().getparameters().get('id');
  }
  
  public Attachment attachment {
  get {
      if (attachment == null)
        attachment = new Attachment();
      return attachment;
    }
  set;
  }

  public PageReference upload() {

    attachment.Name = 'Product_Image_'+itemId;
    attachment.OwnerId = UserInfo.getUserId();
    attachment.ParentId = itemId; 
    attachment.IsPrivate = true;

    try {
      
      List<Attachment> attachedFiles = [select Id from Attachment where parentId =:itemId AND Name Like 'Product_Image_%'];
      if (attachedFiles.size() > 0 ) {
          //delete attachedFiles;
          attachment.Id = attachedFiles[0].Id;
      }
      
      upsert attachment;
      PageReference pg = new PageReference('/apex/ItemImageShowPage?id='+itemId);
      return pg;
    
    } catch (DMLException e) {
     
      ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR,'Error uploading attachment'));
      return null;
    
    } finally {
      
      attachment = new Attachment(); 
    
    }

    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.INFO,'Attachment uploaded successfully'));
    return null;
  
  }

  public PageReference cancel() {
      PageReference pg = new PageReference('/apex/ItemImageShowPage?id='+itemId);
      return pg;
  
  }
}
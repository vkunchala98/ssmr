public class redirectToCommunityAccountController {

    public redirectToCommunityAccountController () {
    
    }
    
    public pagereference redirectCommunityAccount() {
    
        List<User> userList = [SELECT ContactId,contact.AccountId,ProfileId,Profile.Name FROM User WHERE Id =:UserInfo.getUserId()];
        system.debug('::userList=>'+userList);
        system.debug('::userList[0].profilename=>'+userList[0].Profile.Name);
        if( userList[0].ContactId != Null ) {
            if(userList[0].Contact.AccountId != NULL) {
                PageReference pg = new Pagereference('/'+userList[0].Contact.AccountId );
                return pg;
            } else {
                PageReference pg = new Pagereference('/home/home.jsp');
                return pg;
            
            }
        } else {
            PageReference pg = new Pagereference('/home/home.jsp');
            return pg;
            
        }
    
    }

}
/**
 * This class allows you to schedule the execution of the Batch_ArchiveOrderData class
 */
public class Sched_ArchiveInvoiceData implements Schedulable {
	
	public void execute(SchedulableContext schCon)
	{
        Batch_ArchiveInvoiceData bc = new Batch_ArchiveInvoiceData();
        Database.executeBatch(bc);
	}
    
}
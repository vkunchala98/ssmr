global class DirectInvoiceControllerBatch implements Database.Batchable<Sobject>,Database.StateFul{
    
    global String query;
    
    global DirectInvoiceControllerBatch()
    {
        query = 'SELECT Id, Name, Total__c, Service_Charge__c,Store_Product__c,PO__r.Bill_To__r.Terms__c,PO__r.Bill_To__r.OwnerId,PO__r.Bill_To__c,PO__r.Ship_To__c,Cost_per_Deal__c,Discount__c,Item_Number__c,Quantity__c,Type_of_Display__c,SMR_Customer_Invoice__c,Vendor_Commission_Percentage_Owed__c,Registered_On__c,Original_Deal_Cost__c'; 
        query +=' FROM PO_Line_Item__c' ;
        query +=' WHERE Registered__c = true AND SMR_Customer_Invoice__c = null AND Invoiced__c = false AND (PO__r.RecordType__c=\'Direct_Bill\' OR PO__r.RecordType__c=\'Assorted\') AND Registered_On__c = LAST_N_DAYS:45';
        
        system.debug('::query =>'+query );
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        system.debug('::query start=>'+query );
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC,List<PO_Line_Item__c> poLineItemLists)
    {
		// get a set of bill to account IDs
		Set<Id> billToIdSet = new Set<Id>();
		for(PO_Line_Item__c poLineItem : poLineItemLists)
		{
			if(poLineItem.PO__r.Bill_To__c != null){
				billToIdSet.add(poLineItem.PO__r.Bill_To__c);
			}
		}
        
        Map<Id,Invoice__c> billToIdAndInvoiceMap = new Map<Id,Invoice__c>();
        Map<Id,List<PO_Line_Item__c>> billTooLineItemListMap = new Map<Id,List<PO_Line_Item__c>>();
        List<RecordType> invoiceRecTypeList = [SELECT Id FROM RecordType WHERE DeveloperName = 'Direct_Bill_Invoice' AND  SobjectType = 'Invoice__c' limit 1];
        List<RecordType> invoiceLineItemRecTypeList = [SELECT Id FROM RecordType WHERE DeveloperName = 'Customer' AND  SobjectType = 'Invoice_Line_Item__c' limit 1];
        string invoiceLineItemRecType = '';
        if(invoiceLineItemRecTypeList.size() > 0 ){
        	invoiceLineItemRecType = invoiceLineItemRecTypeList[0].id;
        }
        string invoiceRecType = '';
        if(invoiceRecTypeList.size()>0){
        	invoiceRecType = invoiceRecTypeList[0].id;
        }
        List<Invoice_Line_Item__c> invoiceLIList = new List<Invoice_Line_Item__c>();  
        List<PO_Line_Item__c> updatePoLineItemList = new List<PO_Line_Item__c>();
        
        // get all of the PO line items beneath the bill to addresses that have not yet been invoiced and create the invoice records
        for(PO_Line_Item__c poItem : Database.query(query + ' AND PO__r.Bill_To__c IN :billToIdSet'))
        {
            if(poItem.PO__r.Bill_To__c != null)
            {
                system.debug('****Account__c: ' + poItem.PO__r.Bill_To__c);
                
                Invoice__c invoice = BillToIdAndInvoiceMap.get(poItem.PO__r.Bill_To__c);
                if(invoice == null)
                {
                	invoice = new Invoice__c();
                    invoice.Account__c = poItem.PO__r.Bill_To__c;
                    invoice.RecordTypeId = invoiceRecType;
                    invoice.Invoice_Type__c = 'Direct Bill Service Charge';
                    invoice.Invoice_Date__c = System.today();
                    invoice.Status__c = 'Not Yet Invoiced';
                    invoice.Sales_Rep__c = poItem.PO__r.Bill_To__r.OwnerId;
                    invoice.Terms__c = poItem.PO__r.Bill_To__r.Terms__c;
                    invoice.Amount__c = 0;
                }
                invoice.Amount__c += poItem.Service_Charge__c;
                BillToIdAndInvoiceMap.put(poItem.PO__r.Bill_To__c, invoice);
                
                if(!BillTooLineItemListMap.containsKey(poItem.PO__r.Bill_To__c)){
                    BillTooLineItemListMap.put(poItem.PO__r.Bill_To__c, new List<PO_Line_Item__c>());
                }
                BillTooLineItemListMap.get(poItem.PO__r.Bill_To__c).add(poItem);
            }
        }
        system.debug('::BillToIdAndInvoiceMap=>'+BillToIdAndInvoiceMap);
        system.debug('::BillTooLineItemListMap=>'+BillTooLineItemListMap);
        system.debug('::BillTooLineItemListMapkeyset=>'+BillTooLineItemListMap.keyset());
        
        if(BillToIdAndInvoiceMap.values().size() > 0){
            insert BillToIdAndInvoiceMap.values();
        }
		
		// generate the invoice line items
        for(Id vendorId : BillToIdAndInvoiceMap.keySet())
        {
            if(BillTooLineItemListMap.containsKey(vendorId))
            {
                List<PO_Line_Item__c> tempPoLineItemList = BillTooLineItemListMap.get(vendorId);
                
                for(PO_Line_Item__c pli : BillTooLineItemListMap.get(vendorId))
                {
                    if(pli.PO__r.Bill_To__c == BillToIdAndInvoiceMap.get(vendorId).Account__c)
                    {
                        invoiceLIList.add(
                            new Invoice_Line_Item__c(
                            	Invoice__c = BillToIdAndInvoiceMap.get(pli.PO__r.Bill_To__c).Id,
                                Store_Product__c = pli.Store_Product__c,
                                Cost_per_Deal__c = pli.Cost_per_Deal__c,
                                Bill_To__c = pli.PO__r.Bill_To__c,
                                Item_Number__c = pli.Item_Number__c,
                                Quantity__c = pli.Quantity__c,
                                Type_of_Display__c = pli.Type_of_Display__c,
                                PO_Line_Item_Registered_On__c = pli.Registered_On__c,
                                PO_Line_Item__c = pli.Id,
                                Ship_To__c = pli.PO__r.Ship_To__c,
                                RecordTypeId = invoiceLineItemRecType
                            )
                        );
                       
                        pli.SMR_Customer_Invoice__c = BillToIdAndInvoiceMap.get(pli.PO__r.Bill_To__c).Id;
                        pli.Invoiced__c = true;
                        updatePoLineItemList.add(pli);
                    }
                }
            }
        }
        
        if(invoiceLIList.size() > 0)
        {
            insert invoiceLIList;
            update updatePoLineItemList;
        }
    }
    
    global void finish(Database.BatchableContext BC) {
        
    }
}
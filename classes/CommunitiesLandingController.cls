/**
 * An apex page controller that takes the user to the right start page based on credentials or lack thereof
 */
public with sharing class CommunitiesLandingController {

    // Code we will invoke on page load.
    public PageReference forwardToStartPage() {
        User u = [SELECT AccountId FROM User WHERE Id = :UserInfo.getUserId()];
        if(u.AccountId!=null){
            Account acc = [SELECT Id, Active__c, Phone, Fax, Fed_Tax_Id__c, BillingStreet,BillingCity,BillingState,
            BillingPostalCode, Commission_Percentage__c, Terms__c, Minimum_Order_Amount__c FROM Account WHERE Id = :u.AccountId];
            if(acc.Active__c == null || acc.Phone == null  || acc.Phone == null  || acc.Fax == null 
             || acc.Fed_Tax_Id__c == null  || acc.Commission_Percentage__c == null  || acc.Terms__c == null  
             || acc.Minimum_Order_Amount__c == null || acc.BillingStreet == null || acc.BillingCity == null || acc.BillingState == null
             || acc.BillingPostalCode==null){
                PageReference pr = new PageReference ('/vendors/' + acc.Id + '/e');
                return pr;
            }
         
        }
        return Network.communitiesLanding();
    }
    
    public CommunitiesLandingController() {}
}
public with sharing class ShowCartTriggerHandler {
	
	public static void handleBeforeInsert()
	{
		List<Show_Cart__c> showCartList = new List<Show_Cart__c>();
		
		for(Show_Cart__c showCart : (List<Show_Cart__c>)Trigger.new){
			showCartList.add(showCart);
		}
		
		if(!showCartList.isEmpty()){
			ShowCartService.setSalesRep(showCartList);
		}
	}
/*	
	public static void handleBeforeUpdate()
	{
		List<Show_Cart__c> showCartList = new List<Show_Cart__c>();
		
		for(Show_Cart__c showCart : (List<Show_Cart__c>)Trigger.new)
		{
			Show_Cart__c oldShowCart = (Show_Cart__c)Trigger.oldMap.get(showCart.Id);
			if(showCart.Bill_To__c != oldShowCart.Bill_To__c){
				showCartList.add(showCart);
			}
		}
		
		if(!showCartList.isEmpty()){
			ShowCartService.setSalesRep(showCartList);
		}
	}
*/	
	public static void handleAfterUpdate()
	{
		List<Show_Cart__c> approvedShowCartList = new List<Show_Cart__c>();
		
		for(Show_Cart__c showCart : (List<Show_Cart__c>)Trigger.new)
		{
			Show_Cart__c oldShowCart = (Show_Cart__c)Trigger.oldMap.get(showCart.Id);
			if(showCart.Status__c == 'Approved' && oldShowCart.Status__c != 'Approved'){
				approvedShowCartList.add(showCart);
			}
		}
		
		if(!approvedShowCartList.isEmpty())
		{
			List<ShowCartService.CartOrder> cartOrderList = ShowCartService.convertToOrders(approvedShowCartList);
			ShowCartService.createOrders(cartOrderList);
		}
	}
    
}
<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Populate_Total_for_Rollup_Field</fullName>
        <field>Total_For_Rollup__c</field>
        <formula>Total__c</formula>
        <name>Populate Total for Rollup Field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Populate Total for Rollup Field</fullName>
        <actions>
            <name>Populate_Total_for_Rollup_Field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Show_Cart_Line_Item__c.Total__c</field>
            <operation>greaterOrEqual</operation>
            <value>0</value>
        </criteriaItems>
        <description>Workflow populates &apos;Total for Rollup field&apos; so that Total can be calculated on the Show Cart</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>

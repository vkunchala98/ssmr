<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Populate_Commission_Due</fullName>
        <field>Commission_Due__c</field>
        <formula>Invoice__r.Account__r.Service_charge__c * Cost_per_Deal__c</formula>
        <name>Populate Commission Due</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Populate Commission Due</fullName>
        <actions>
            <name>Populate_Commission_Due</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Invoice_Line_Item__c.Commission_Due__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>

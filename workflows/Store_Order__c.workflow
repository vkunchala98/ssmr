<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Populate_Order_Date</fullName>
        <field>Order_Date__c</field>
        <formula>Today()</formula>
        <name>Populate Order Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Populate_Service_Percentage</fullName>
        <field>Service_Charge__c</field>
        <formula>Account__r.Service_charge__c</formula>
        <name>Populate Service Percentage</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Populate_Shipping_Instructions</fullName>
        <field>Ship_Via_Code__c</field>
        <literalValue>Prepaid to Destination</literalValue>
        <name>Populate Shipping Instructions</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Populate_Terms</fullName>
        <field>Terms__c</field>
        <literalValue>Net 30</literalValue>
        <name>Populate Terms</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Direct_Record_Type</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Direct_Bill</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Set Direct Record Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Central_Record_Type</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Central_Bill</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update Central Record Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Populate Order Date</fullName>
        <actions>
            <name>Populate_Order_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When Order is created, set the Order Date to &apos;Today&apos;</description>
        <formula>TRUE</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Populate Service Charge</fullName>
        <actions>
            <name>Populate_Service_Percentage</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Service_charge__c</field>
            <operation>notEqual</operation>
            <value>0</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set Central Order Record Type</fullName>
        <actions>
            <name>Populate_Terms</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Central_Record_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Store_Order__c.Bill_Types__c</field>
            <operation>equals</operation>
            <value>Central Warehouse,Central Dropship</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Set Direct Order Record Type</fullName>
        <actions>
            <name>Populate_Terms</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Direct_Record_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Store_Order__c.Bill_Types__c</field>
            <operation>equals</operation>
            <value>Direct Warehouse,Direct Dropship</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>

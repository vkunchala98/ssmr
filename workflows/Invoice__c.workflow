<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Populate_Due_Date</fullName>
        <field>Due_Date__c</field>
        <formula>IF((ISPICKVAL(Terms__c, &apos;On Receipt&apos;)),(Invoice_Date__c), (IF((ISPICKVAL(Terms__c, &apos;Net 15&apos;)),(Invoice_Date__c+15), (IF((ISPICKVAL(Terms__c, &apos;Net 21&apos;)),(Invoice_Date__c+21), ( IF((ISPICKVAL(Terms__c, &apos;Net 30&apos;)),(Invoice_Date__c+30), Invoice_Date__c)))))))</formula>
        <name>Populate Due Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>QB_Generate_field</fullName>
        <field>Generate__c</field>
        <literalValue>Bill</literalValue>
        <name>QB Generate field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Quickbooks_field</fullName>
        <field>Generate__c</field>
        <literalValue>Invoice</literalValue>
        <name>Quickbooks field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Populate Due Date</fullName>
        <actions>
            <name>Populate_Due_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Invoice__c.Terms__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Push Bill to QB</fullName>
        <actions>
            <name>QB_Generate_field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Invoice__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Vendor Payable</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Push Invoice to QB</fullName>
        <actions>
            <name>Quickbooks_field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Invoice__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Central Bill Invoice,Direct Bill Invoice,Vendor Invoice</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>

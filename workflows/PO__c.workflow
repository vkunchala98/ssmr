<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Populate_Customer_Terms</fullName>
        <field>Customer_Terms__c</field>
        <literalValue>Net 30</literalValue>
        <name>Populate Customer Terms</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Populate_Customer_Terms2</fullName>
        <field>Customer_Terms__c</field>
        <literalValue>Net 21</literalValue>
        <name>Populate Customer Terms</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Populate_Order_Status</fullName>
        <field>Order_Status__c</field>
        <literalValue>Open</literalValue>
        <name>Populate Order Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Populate_Vendor_Terms</fullName>
        <field>Vendor_Terms__c</field>
        <literalValue>Net 30</literalValue>
        <name>Populate Vendor Terms</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Populate_Vendor_Terms2</fullName>
        <field>Vendor_Terms__c</field>
        <literalValue>Net 45</literalValue>
        <name>Populate Vendor Terms</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>RecordType_field_update</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Direct_Bill</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>RecordType field update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>RecordType_field_update2</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Central_Bill</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>RecordType field update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Order_Status</fullName>
        <field>Order_Status__c</field>
        <literalValue>Open</literalValue>
        <name>Set Order Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Set Central Bill PO Record Type</fullName>
        <actions>
            <name>Populate_Customer_Terms2</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Populate_Vendor_Terms2</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>RecordType_field_update2</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Order_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>OR((CONTAINS(Bill_Type__c,&apos;Central Dropship&apos;)),(CONTAINS(Bill_Type__c,&apos;Central Warehouse&apos;)))</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Set Direct Bill PO Record Type</fullName>
        <actions>
            <name>Populate_Customer_Terms</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Populate_Order_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Populate_Vendor_Terms</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>RecordType_field_update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>OR((CONTAINS(Bill_Type__c, &apos;Direct Warehouse&apos;)),(CONTAINS(Bill_Type__c, &apos;Direct Dropship&apos;)))</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>

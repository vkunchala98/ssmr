<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Populate_Item</fullName>
        <field>Item__c</field>
        <formula>Store_Product__r.Name</formula>
        <name>Populate Item</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Populate_Item_Number</fullName>
        <field>Item_Number__c</field>
        <formula>Store_Product__r.Item__c</formula>
        <name>Populate Item #</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Populate_Original_Cost_per_Deal</fullName>
        <field>Original_Deal_Cost__c</field>
        <formula>Store_Product__r.Cost_Per_Deal__c</formula>
        <name>Populate Original Cost per Deal</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Populate Order Line Item with Product Information</fullName>
        <actions>
            <name>Populate_Item</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Populate_Item_Number</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Populate_Original_Cost_per_Deal</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Order_Line_Items__c.Name</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>

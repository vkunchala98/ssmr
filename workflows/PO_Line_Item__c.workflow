<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>PO_Vendor_Commission_to_PO_Line_Item</fullName>
        <field>Vendor_Commission_Percentage__c</field>
        <formula>IF(ISPICKVAL(PO__r.Store_Order__r.Bill_Types__c, &quot;Central Warehouse&quot;),
	PO__r.Account__r.Central_Bill_Warehouse_Commission__c,
	IF(ISPICKVAL(PO__r.Store_Order__r.Bill_Types__c, &quot;Central Dropship&quot;),
		PO__r.Account__r.Central_Bill_Dropship_Commission__c,
		IF(ISPICKVAL(PO__r.Store_Order__r.Bill_Types__c, &quot;Direct Warehouse&quot;),
			PO__r.Account__r.Direct_Bill_Warehouse_Commission__c,
			IF(ISPICKVAL(PO__r.Store_Order__r.Bill_Types__c, &quot;Direct Dropship&quot;),
				PO__r.Account__r.Direct_Bill_Dropship_Commission__c,
				NULL
			)
		)
	)
)</formula>
        <name>PO Vendor Commission to PO Line Item</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Populate_Original_Deal_Cost</fullName>
        <field>Original_Deal_Cost__c</field>
        <formula>Store_Product__r.Cost_Per_Deal__c</formula>
        <name>Populate Original Deal Cost</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Populate_Registered_On</fullName>
        <field>Registered_On__c</field>
        <formula>NOW()</formula>
        <name>Populate Registered On</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Record_Type</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Direct_Bill</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Set Record Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Copy_Total_Commission_Due</fullName>
        <field>Copy_of_Total_Commission_Due__c</field>
        <formula>Total_Commission_Due__c</formula>
        <name>Update Copy Total Commission Due</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Field</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Central_Bill</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update Field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Copy Total Commission Due</fullName>
        <actions>
            <name>Update_Copy_Total_Commission_Due</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>PO_Line_Item__c.Total_Commission_Due__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Copy Total Commission Due</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Populate Registered ON in PO Line Item</fullName>
        <actions>
            <name>Populate_Registered_On</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>PO_Line_Item__c.Registered__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>When Registered checkbox is checked , it populate the present datatime in Registered On</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Populate Vendor Commission Percentage</fullName>
        <actions>
            <name>PO_Vendor_Commission_to_PO_Line_Item</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Populate_Original_Deal_Cost</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When PO Line Item is created, populate the Vendor Commission Percentage field by pulling the appropriate Commission Percentage field from Vendor based on the Order Bill Type</description>
        <formula>true</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Set Central Bill PO Line Item Record Type</fullName>
        <actions>
            <name>Update_Field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>OR(CONTAINS(TEXT(Order_Line_Item__r.Store_Order__r.Bill_Types__c), &apos;Central Warehouse&apos;),CONTAINS(TEXT(Order_Line_Item__r.Store_Order__r.Bill_Types__c), &apos;Central Dropship&apos;))</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Set Direct Bill PO Line Item Record Type</fullName>
        <actions>
            <name>Set_Record_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>OR(CONTAINS(TEXT(Order_Line_Item__r.Store_Order__r.Bill_Types__c), &apos;Direct Warehouse&apos;),CONTAINS(TEXT(Order_Line_Item__r.Store_Order__r.Bill_Types__c), &apos;Direct Dropship&apos;))</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>

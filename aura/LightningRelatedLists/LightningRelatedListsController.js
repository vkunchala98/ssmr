({
    jsLoaded : function(cmp, event, helper) {
        
        var action = cmp.get("c.userEvent");
        action.setCallback(this, function(response) {
            //store state of response
            var state = response.getState();
            if (state === "SUCCESS") {
                //set response value in wrapperList attribute on component.
                cmp.set('v.myInitialval', response.getReturnValue());
                console.log('Event Record>>>'+response.getReturnValue());
            }            
        });
        $A.enqueueAction(action); 
    },
    customHandler : function(cmp, event, helper) {
        alert("It's custom action!")
    },
    
    refreshView : function (component, event, helper) {
        $A.get('e.force:refreshView').fire();
    },
    handleCreateRecord : function (cmp, event, helper) {
        var createRecordEvent = $A.get("e.force:createRecord");
        createRecordEvent.setParams({
            "entityApiName": "Event_Attendee__c",
            "defaultFieldValues": {
                ["Event__c"] : cmp.get("v.myInitialval.EventId")
            }
        });
        createRecordEvent.fire();
    },   
    handleCreateRecord1 : function (cmp, event, helper) {
        var createRecordEvent = $A.get("e.force:createRecord");
        createRecordEvent.setParams({
            "entityApiName": "Incentive__c",
            "defaultFieldValues": {
                ["Event__c"] : cmp.get("v.myInitialval.EventId")
            }
        });
        createRecordEvent.fire();
    },  
    submenK:function (cmp, event, helper) {
        var selectedMenuItemValue = event.getParam("value");
        console.log('#$#$#selectedItem$#$>>>'+selectedMenuItemValue);
        var editRecordEvent = $A.get("e.force:editRecord");
        editRecordEvent.setParams({
            "recordId": selectedMenuItemValue
        });
        editRecordEvent.fire();
        // $A.get("e.force:refreshView").fire();
    },
     //Open File onclick event  
   OpenFile :function(component,event,helper){  
     var rec_id = event.currentTarget.id;  
     $A.get('e.lightning:openFiles').fire({ //Lightning Openfiles event  
       recordIds: [rec_id] //file id  
     });  
   },  
    UploadFinished : function(component, event, helper) {  
     var uploadedFiles = event.getParam("files");  
     var documentId = uploadedFiles[0].documentId;  
     var fileName = uploadedFiles[0].name;  
    
     var toastEvent = $A.get("e.force:showToast");  
     toastEvent.setParams({  
       "title": "Success!",  
       "message": "File "+fileName+" Uploaded successfully."  
     });  
     toastEvent.fire();  
     /* Open File after upload  
     $A.get('e.lightning:openFiles').fire({  
       recordIds: [documentId]  
     });*/  
   },  
})
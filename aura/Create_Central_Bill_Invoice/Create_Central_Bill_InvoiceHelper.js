({
    getRecordInfo : function(component, event, helper) {
        var action = component.get("c.getStoreOrder");
        action.setParams({
            "poid" : component.get("v.recordId")
        });
        action.setCallback(this, function( response ){
            var state = response.getState();
            if( state == "SUCCESS" ){
                var data = response.getReturnValue();
                console.log('>>data>>>>'+data);
                if(data == "" || data == null){
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Error!",
                        "type" : "error",
                        "mode" : "dismissible",
                        "duration" : 5000,
                        "message": "Unknown Error"
                    });
                    toastEvent.fire();
                    helper.dismissQuickAction( component, event, helper );
                }else if(data==="BillType"){
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Error!",
                        "type" : "warning",
                        "mode" : "dismissible",
                        "duration" : 5000,
                        "message": "Bill Type of this Customer Sales Order should be Central Warehouse or Central Dropship"
                    });
                    toastEvent.fire();
                    helper.dismissQuickAction( component, event, helper );
                }
                    else{
                        var resultSet = data.toString().split("-"); 
                        console.log(resultSet[0]);
                        if(resultSet[0] != 'success'){
                            //Error message
                            var toastEvent = $A.get("e.force:showToast");
                            toastEvent.setParams({
                                "title": "Error!",
                                "type" : "error",
                                "mode" : "dismissible",
                                "duration" : 5000,
                                "message": resultSet[0]
                            });
                            toastEvent.fire();
                            helper.dismissQuickAction( component, event, helper );
                        }else{
                            console.log('>>>>Record id>>'+resultSet[1]);
                            if(resultSet[1]== 'null'){
                                console.log('other part of Redirection>>');
                                helper.dismissQuickAction( component, event, helper );
                                //refresh the view
                                $A.get('e.force:refreshView').fire();
                                /*  var ccID='a0KM000000BjTCPMA3';
                                var navEvt = $A.get("e.force:navigateToSObject");
                                navEvt.setParams({
                                    "recordId": ccID,
                                    "slideDevName": "detail"
                                });
                                navEvt.fire();*/
                            }else{
                                //redirect to record
                                console.log('Redirect to Invoice Record>?>'+resultSet[1]);
                                
                                var dismissActionPanel = $A.get("e.force:closeQuickAction");
                                dismissActionPanel.fire();
                                var navEvt = $A.get("e.force:navigateToSObject");
                                navEvt.setParams({
                                    "recordId": resultSet[1],
                                    "slideDevName": "detail"
                                });
                                navEvt.fire();
                            }
                        }
                        
                    }
            }else if( state == "ERROR" ){
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message:>> " + 
                                    errors[0].message);
                    }
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Error!",
                        "type" : "error",
                        "mode" : "dismissible",
                        "duration" : 5000,
                        "message": errors[0].message
                    });
                    toastEvent.fire();
                    helper.dismissQuickAction( component, event, helper );
                } else {
                    console.log("Unknown error");
                    helper.dismissQuickAction( component, event, helper );
                }
            }
        });
        $A.enqueueAction( action );
    },
    dismissQuickAction : function( component, event, helper ){
        // Close the action panel
        var dismissActionPanel = $A.get("e.force:closeQuickAction");
        dismissActionPanel.fire();
    },
    
})
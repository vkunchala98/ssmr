({
	doit : function(component, event, helper) {
        helper.accountDetails( component, event, helper );
    },
    save : function(component, event, helper) {
        //disbling save button
        var button = component.find('disableSave');
        button.set('v.disabled',true);
        component.find("edit").get("e.recordSave").fire();
        component.set("v.isEdit",false);
        $A.get('e.force:refreshView').fire();
    },
    Edit : function(component, event, helper) {
        var button = component.find('disableEdit');
        button.set('v.disabled',true);
        component.set("v.isEdit",true);

    },
})
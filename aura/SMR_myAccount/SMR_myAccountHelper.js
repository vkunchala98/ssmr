({
    accountDetails : function(component, event, helper) {
        var action = component.get("c.getAccountDetails");
        action.setCallback(this, function( response ){
            var state = response.getState();
            if( state == "SUCCESS" ){
                var data = response.getReturnValue();
                console.log('Return value from server'+data);
                if(data == "" || data == null){
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Error!",
                        "type" : "error",
                        "mode" : "dismissible",
                        "duration" : 5000,
                        "message": "Unknown Error"
                    });
                    toastEvent.fire();
                    // helper.dismissQuickAction( component, event, helper );
                    
                    
                }
                else {                
                    
                    console.log('data Accountid2>>>>>'+data);
                    component.set("v.recordId", data);
                    componenet.set("v.isEdit",true);
                    //helper.dismissQuickAction( component, event, helper );
                    
                }
                
            }else if( state == "ERROR" ){
                var errors = response.getError();
                if (errors) {
                    if (errors[0] && errors[0].message) {
                        console.log("Error message:>> " + 
                                    errors[0].message);
                    }
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        "title": "Error!",
                        "type" : "error",
                        "mode" : "dismissible",
                        "duration" : 5000,
                        "message": errors[0].message
                    });
                    toastEvent.fire();
                    
                    // helper.dismissQuickAction( component, event, helper );
                } else {
                    console.log("Unknown error");
                    
                    // helper.dismissQuickAction( component, event, helper );
                }
            }
        });
        $A.enqueueAction( action );
        
    },
})
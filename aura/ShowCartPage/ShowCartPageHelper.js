({
	getVendorBoothList : function(component) {
		var action = component.get("c.getBoothList");

		action.setCallback(this, function(a){
			//component.set("v.vendorBoothList", a.getReturnValue());
			var boothList = a.getReturnValue();
			var vendorBoothList = [];
			
			for (var i = 0; i < boothList.length; i++) {
				var booth = {
					"label": boothList[i].Name,
					"value": boothList[i].Id,
				};
				vendorBoothList.push(booth);
			}
			component.set("v.vendorBoothList", vendorBoothList);
		});

		$A.enqueueAction(action);
	},

	buildCart : function(component){
		var action = component.get("c.buildShowCart");

		action.setCallback(this, function(a){
			component.set("v.showCart", a.getReturnValue());
		});

		$A.enqueueAction(action);
	},

	toggleHide : function(component, componentName){
		var targetName = component.find(componentName);

		$A.util.toggleClass(targetName, "slds-hide");
	},

	toggleVendorPopup : function(component){
		var showAlternativePopup = component.get("v.showAlternativePopup");
		component.set("v.showAlternativePopup", !showAlternativePopup);
	},
})
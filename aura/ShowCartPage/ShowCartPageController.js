({
	doInit : function(component, event, helper) {
		helper.buildCart(component);
	},

	toggleAlternativeVendorPopup : function(component, event, helper){
		helper.toggleVendorPopup(component);
		
		var showAlternativePopup = component.get("v.showAlternativePopup");

		if(showAlternativePopup){
			var vendorBoothList = component.get("v.vendorBoothList");
			
			console.log(vendorBoothList.length);
			if(vendorBoothList.length < 1){
				helper.getVendorBoothList(component);
				component.set("v.selectedVendor", "");
			}
		}
	},

	selectedVendorChange : function(component, event, helper){
		var selectedOptionValue = event.getParam("value");
		component.set("v.selectedVendor", selectedOptionValue);
	},

	addVendorToCart : function(component, event, helper){
		var selectedVendor = component.get("v.selectedVendor");
		var cartId = component.get("v.showCart.Id");

		var action = component.get("c.addVendor");

		action.setParams({
			"cartId" : cartId,
			"selectedVendor" : selectedVendor
		});

		action.setCallback(this, function(a){
			console.log(a.getReturnValue());
			component.set("v.selectedVendor", "");
			helper.toggleHide(component, "alternativePopupSpinner");
			helper.toggleVendorPopup(component);
			helper.getCartVendors(component);
		});

		$A.enqueueAction(action);
		helper.toggleHide(component, "alternativePopupSpinner");
	}
})
({
    doInit: function(component, event, helper) {
        helper.createObjectData(component, event);
        
    },

    handleEvent : function(cmp, event) {
        var message = event.getParam("message");

        console.log('event message: ' + message);

        if(message == 'true'){
            cmp.set('v.shipToMultiple', true);
        } else {
            cmp.set('v.shipToMultiple', false);
        }

        console.log('shipToMultiple: ' + cmp.get('v.shipToMultiple'));
    },
    
    addNewLineItemRow: function(component, event, helper) {
        helper.createObjectData(component, event);
    },
 
    removeDeletedLineItemRow: function(component, event, helper) {
        var index = event.getParam("indexVar");        
        var AllRowsList = component.get("v.lineItemList");
        AllRowsList.splice(index, 1);
        component.set("v.lineItemList", AllRowsList);
        helper.calculateOrderTotal(component, event);
        component.getEvent("UpdateFinalTotal").fire();
    },
    
    AddNewRow : function(component, event, helper){
        component.getEvent("AddNewVendorRowEvt").fire();        
    },
    
    removeRow : function(component, event, helper){
        var lineItemList = component.get("v.lineItemList");
        var total = 0;
        lineItemList.forEach(function (arrayItem) {
            if (arrayItem.Total_Deal_Cost__c == undefined){
               total = total + 0; 
            }else{
               total = total + arrayItem.Total_Deal_Cost__c; 
            }           
		});
       component.getEvent("DeleteNewVendorRowEvt").setParams({
           "indexVar" : component.get("v.rowIndex"),
           "amountPerVendor" : total
       }).fire();
       component.getEvent("SelectedVendorChange").setParams({
           "differentBillType" : false,
           "rowIndex" : component.get("v.rowIndex")
       }).fire(); 
    },
    
    updateVendorInstanceValues : function(component, event, helper){
    	var vendorInstance = component.get("v.vendorInstance");
		var selectedLookUpRecord = component.get("v.selectedLookUpRecord");        
        vendorInstance.Id = selectedLookUpRecord.Id;
        vendorInstance.Name = selectedLookUpRecord.Name;        
        component.set("v.vendorInstance", vendorInstance);
    },
    
    calculateOrderTotalController : function(component, event, helper){
    	helper.calculateOrderTotal(component, event);
    },

    

    
    vendorChange: function(component, event, helper){
        /*var selectedLookUpRecord = component.get("v.selectedLookUpRecord");
        var customerBillType = component.get("v.customerBillType");
		var differentBillType = true;        
        var vendorBillTypes = selectedLookUpRecord.Acceptable_Bill_Type__c;
        if (vendorBillTypes != undefined){
        	var vendorBillTypesArray = vendorBillTypes.split(";");
        	vendorBillTypesArray.forEach(function (actualBillType) {
                
            	if (vendorBillTypes.includes(actualBillType)){
                    
					differentBillType = false;
            	}      
                
                
                
			});       
            component.getEvent("SelectedVendorChange").setParams({
               "differentBillType" : differentBillType,
               "rowIndex" : component.get("v.rowIndex")
           }).fire();            
        }else{*/
            var lineItemList = component.get("v.lineItemList");
            lineItemList.forEach(function (arrayItem) {
            	arrayItem.Total_Deal_Cost__c = 0;
            });
            component.set("v.lineItemList", lineItemList);
            component.set("v.lineItemList", []);            
            helper.createObjectData(component, event);
            helper.calculateOrderTotal(component, event);
            component.getEvent("UpdateFinalTotal").fire();
        //} 
    }    
})
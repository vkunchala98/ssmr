({
    createObjectData: function(component, event) {
        var RowItemList = component.get("v.lineItemList");
        RowItemList.push({
            'sobjectType': 'Order_Line_Items__c'
        });
        component.set("v.lineItemList", RowItemList);
    },
    
    calculateOrderTotal : function(component, event){  
        var lineItemList = component.get("v.lineItemList");
		var total = 0;
        lineItemList.forEach(function (arrayItem) {
            if (arrayItem.Total_Deal_Cost__c == undefined){
               total = total + 0; 
            }else{
               total = total + arrayItem.Total_Deal_Cost__c; 
            }           
		});
        component.find("total").set('v.value', total);
    }
})
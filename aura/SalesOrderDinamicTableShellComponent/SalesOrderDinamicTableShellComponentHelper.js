({
    createObjectData: function(component, event) {
        var RowItemList = component.get("v.vendorList");
        RowItemList.push({
            'sobjectType': 'Account',
            'Name': ''
        });
        component.set("v.vendorList", RowItemList);
        
        
    },
 
    validateRequired: function(component, event) {
        var isValid = true;
        var allContactRows = component.get("v.vendorList");
        for (var indexVar = 0; indexVar < allContactRows.length; indexVar++) {
            if (allContactRows[indexVar].Name == '') {
                isValid = false;
                component.set("v.requiredFieldsMissing", true);
                break;
            }
        }
        return isValid;
    },
})
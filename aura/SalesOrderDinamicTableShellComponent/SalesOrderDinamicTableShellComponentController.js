({ 
    doInit: function(component, event, helper) {
        helper.createObjectData(component, event);

        

        var array = [];

        var action = component.get("c.getBillTypeAvailable");
            action.setParams({                
                "recordId": component.get("v.accountId")
            });

            action.setCallback(this, function(response) {
                var state = response.getState();
                if (state === "SUCCESS") { 
                    response = response.getReturnValue();

                    //var array = response.split(";");

                    //

                    

                    if(response.length > 1){
                        array.push('--Select--');
                    }   
                    
                    for (var i = response.length - 1; i >= 0; i--) {
                        
                        array.push(response[i]);
                    }

                    

                    component.set('v.customerBillType', array[0]);

                    component.set('v.options', array);
                    //window.open('/'+recordId, '_self');
                }
            });
        $A.enqueueAction(action);
    
    },

    close: function(component, event, helper){
        window.history.back();
    },

    Save: function(component, event, helper) {

        var requiredFieldsMissing = false;
        component.set("v.itemContainerStyle", "");
        
        component.set("v.requiredFieldsMissing", false);
        component.set("v.recordsSaved", false);
                
        var applicationEvent = $A.get("e.c:RefreshLineItemElements");
        applicationEvent.fire();
        
        var missingRequiredFields = component.get("v.requiredFieldsMissing");
        var vendorsBillValidationFailed = component.get("v.vendorsBillValidationFailed");

        var billingType = component.get("v.customerBillType");
        var recordId = '';

        

        

        
        if(billingType == '' || billingType == undefined){
            billingType = component.find("billType").get('v.value');
            
        }

        if(billingType.includes('Select')){
            
            component.set("v.itemContainerStyle", component.get("v.errorStyle"));
            requiredFieldsMissing = true;    
        } 

        if(requiredFieldsMissing == true){
            component.getEvent("RequiredFieldsMissing").setParams({
                "currentLineItemFailedValidation" : requiredFieldsMissing
                //"lineItemElement" : component.get("v.lineItemInstance")      
            }).fire();

        } else {
                component.set("v.itemContainerStyle", "");                        
                requiredFieldsMissing = false;  

                if (helper.validateRequired(component, event) && (!missingRequiredFields) && (!vendorsBillValidationFailed)) {
                component.set("v.toggleSpinner", true);
                var action = component.get("c.saveStoreOrders");
                action.setParams({                
                    "vendorList": component.get("v.vendorList"),
                    "lineItemList": component.get("v.allLineItemList"),
                    "customerId": component.get("v.accountId"),
                    "billType": billingType,
                    "masterShip": component.get("v.SelectedCustomer.Id")
                });

                //                "billType": component.find("billType").get('v.value'),         


                

                action.setCallback(this, function(response) {
                    var state = response.getState();
                    if (state === "SUCCESS") { 
                        component.set("v.vendorList", []);
                        helper.createObjectData(component, event);
                        component.set("v.recordsSaved", true);
                        component.find("finalTotal").set('v.value', 0);

                        

                        recordId = response.getReturnValue();
                        window.open('/'+recordId, '_blank');
                        window.close();
                    }
                    component.set("v.toggleSpinner", false);
                });
                $A.enqueueAction(action);
            }
            component.set("v.allLineItemList", []);
        }
        
        

        

        //
    },

    fireApplicationEvent : function(cmp, event) {
        // Get the application event by using the
        // e.<namespace>.<event> syntax
        var checked = cmp.get("v.allAtOne");


        if(!checked){
            cmp.set('v.allAtOne', true);
        } else {
            cmp.set('v.allAtOne', false); 
        } 

        var appEvent = $A.get("e.c:MultipleShipToEvent");
        if(cmp.get("v.allAtOne")){
            console.log('true');
            appEvent.setParams({"message" : "true" });
        } else {
            console.log('false');
            appEvent.setParams({"message" : "false" });
            
        }
        appEvent.fire();
    },
 
    addNewRow: function(component, event, helper) {   
        helper.createObjectData(component, event);
    },
 
    removeDeletedRow: function(component, event, helper) {
        var index = event.getParam("indexVar");
        var amountPerVendor = event.getParam("amountPerVendor");        
        var AllRowsList = component.get("v.vendorList");
        var actualFinalTotal = component.find("finalTotal").get('v.value');        
        AllRowsList.splice(index, 1);
        component.set("v.vendorList", AllRowsList);                      
        component.find("finalTotal").set('v.value', actualFinalTotal-amountPerVendor);        
    },
       
    showRequiredFieldsError: function(component, event, helper) {
        var currentLineItemFailedValidation = event.getParam("currentLineItemFailedValidation");        
        if (currentLineItemFailedValidation){
            component.set("v.requiredFieldsMissing", true);     
        }else{
            var lineItemElement = event.getParam("lineItemElement");
            var allRowsList = component.get("v.allLineItemList");        
            allRowsList.push(lineItemElement);          
            component.set("v.allLineItemList", allRowsList);
        }    
    },

    UpdateFinalTotal: function(component, event, helper) {
        component.find("finalTotal").set('v.value', 0);
        var applicationEvent = $A.get("e.c:getLineItemTotalCosts");
        applicationEvent.fire();        
    },
    
    SendCurrentTotalCostToShell : function(component, event, helper) {
        var itemTotalCost = event.getParam("currentTotal");
        var actualFinalTotal = component.find("finalTotal").get('v.value');
        component.find("finalTotal").set('v.value', itemTotalCost+actualFinalTotal);        
    },
    
    SelectedVendorChange: function(component, event, helper) {       
        var differentBillType = event.getParam("differentBillType");
        var rowIndex = event.getParam("rowIndex");
        var vendorsBillValidationState = component.get("v.vendorsBillValidationState");
        vendorsBillValidationState[rowIndex] = differentBillType;  
        var vendorsBillValidationFailed = false;

        for (var key in vendorsBillValidationState){
            if(vendorsBillValidationState[key]){
                vendorsBillValidationFailed = true;
            }
        }
        component.set("v.vendorsBillValidationFailed", vendorsBillValidationFailed);
        component.set("v.vendorsBillValidationState", vendorsBillValidationState);
    },
    
    cleanVendorsList: function(component, event, helper) {        
        var selectedBillType = component.find("billType").get("v.value");
        component.set("v.billingType", selectedBillType);
        
        component.set("v.customerBillType", selectedBillType);
        component.set("v.vendorList", []);
        component.set("v.vendorsBillValidationFailed", false);
        helper.createObjectData(component, event);        
    },

})
({
    doInit: function(component, event, helper) {

      var message = event.getParam("message");
      var type = component.get("v.conditions");
      var masterShip = component.get("v.masterShip");

      if(type.includes('Customer') && message == 'true'){
        component.find("input").set("v.disabled", true);
      } else {
        component.find("input").set("v.disabled", false);
      }

      if(message == undefined && type.includes('Customer')){ 
        component.find("input").set("v.disabled", true);
      }

      

      if(masterShip.includes('true')){
        
        component.find("input").set("v.disabled", false);
      }


      var defaultShipTo = component.get("v.defaultShipTo");
      


      if(defaultShipTo != undefined || defaultShipTo != null || defaultShipTo != ''){
        component.set("v.SearchKeyWord", defaultShipTo);
      }

      var message = component.get("v.multipleShipToFlag");
      var type = component.get("v.conditions");

      if(type.includes('ParentId')){
        console.log('message on lookup: ' + message + ' type: ' + type);
        if(message == false){
          console.log('disabled');
          component.find("input").set("v.disabled", true);
        } else {
          console.log('enabled');
          component.find("input").set("v.disabled", false);
        }
      }

      //defaultShipTo
      //component.set("v.selectedRecord" , "{'sobjectType':'Account', 'Name':'Account123'}"); 

      /*var action = component.get("c.getAcc");
      // set param to method  
        //action.setParams({
        //  });
      // set a callBack    
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                var storeResponse = response.getReturnValue();
                
                var compEvent = component.getEvent("selectedRecordEvent");
                compEvent.setParams({"recordByEvent" : storeResponse });  
                compEvent.fire();
                
                var selectedAccountGetFromEvent = compEvent.getParam("recordByEvent");
                component.set("v.selectedRecord" , selectedAccountGetFromEvent); 
                

              // if storeResponse size is equal 0 ,display No Result Found... message on screen.                }
            }
 
        });
      // enqueue the Action  
        $A.enqueueAction(action);

      


      // {'sobjectType':'Account', 'Name':'Account123'}*/

    },
   onfocus : function(component,event,helper){
       $A.util.addClass(component.find("mySpinner"), "slds-show");
        var forOpen = component.find("searchRes");
            $A.util.addClass(forOpen, 'slds-is-open');
            $A.util.removeClass(forOpen, 'slds-is-close');
        // Get Default 5 Records order by createdDate DESC 

            // component.find("input").set("v.disabled", true);
            // 

         var getInputkeyWord = '';
         helper.searchHelper(component,event,getInputkeyWord);
    },
    handleApplicationEvent : function(cmp, event) {
        var message = event.getParam("message");
        var type = cmp.get("v.conditions");

        if(type.includes('ParentId') && message != 'true'){
          console.log('disabled');
          cmp.find("input").set("v.disabled", true);
        } else {
          console.log('enabled');
          cmp.find("input").set("v.disabled", false);
        }
    },
    onblur : function(component,event,helper){       
        component.set("v.listOfSearchRecords", null );
        var forclose = component.find("searchRes");
        $A.util.addClass(forclose, 'slds-is-close');
        $A.util.removeClass(forclose, 'slds-is-open');
    },
    keyPressController : function(component, event, helper) {
       // get the search Input keyword   
         var getInputkeyWord = component.get("v.SearchKeyWord");
       // check if getInputKeyWord size id more then 0 then open the lookup result List and 
       // call the helper 
       // else close the lookup result List part.   
        if( getInputkeyWord.length > 0 ){
             var forOpen = component.find("searchRes");
               $A.util.addClass(forOpen, 'slds-is-open');
               $A.util.removeClass(forOpen, 'slds-is-close');
            helper.searchHelper(component,event,getInputkeyWord);
        }
        else{  
             component.set("v.listOfSearchRecords", null ); 
             var forclose = component.find("searchRes");
               $A.util.addClass(forclose, 'slds-is-close');
               $A.util.removeClass(forclose, 'slds-is-open');
          }
	},
    
  // function for clear the Record Selaction 
    clear :function(component,event,heplper){
         var pillTarget = component.find("lookup-pill");
         var lookUpTarget = component.find("lookupField"); 
        
         $A.util.addClass(pillTarget, 'slds-hide');
         $A.util.removeClass(pillTarget, 'slds-show');
        
         $A.util.addClass(lookUpTarget, 'slds-show');
         $A.util.removeClass(lookUpTarget, 'slds-hide');
      
         component.set("v.SearchKeyWord",null);
         component.set("v.listOfSearchRecords", null );
         component.set("v.selectedRecord", {} );   
    },
    
  // This function call when the end User Select any record from the result list.   
    handleComponentEvent : function(component, event, helper) {
    // get the selected Account record from the COMPONETN event 	 
     var selectedAccountGetFromEvent = event.getParam("recordByEvent");
     component.set("v.selectedRecord" , selectedAccountGetFromEvent); 

     
       
        var forclose = component.find("lookup-pill");
           $A.util.addClass(forclose, 'slds-show');
           $A.util.removeClass(forclose, 'slds-hide');
  
        var forclose = component.find("searchRes");
           $A.util.addClass(forclose, 'slds-is-close');
           $A.util.removeClass(forclose, 'slds-is-open');
        
        var lookUpTarget = component.find("lookupField");
            $A.util.addClass(lookUpTarget, 'slds-hide');
            $A.util.removeClass(lookUpTarget, 'slds-show');  
      
	},
})
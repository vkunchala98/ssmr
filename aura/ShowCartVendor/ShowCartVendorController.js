({
	toggleSection : function(component, event, helper) {
		var detailsSection = component.find("detailsSection");
		var sectionButton = component.find("sectionButton");

		$A.util.toggleClass(detailsSection, 'slds-is-open');

		if(sectionButton.get("v.iconName") == "utility:chevrondown"){
			sectionButton.set("v.iconName", "utility:chevronup");
		}else{
			sectionButton.set("v.iconName", "utility:chevrondown");
		}
	}
})
({
    doInit : function(component, event, helper){
        var multiple = component.get("v.shipToAvailable");
        console.log('multiple on row: ' + multiple);

    },

    AddNewRow : function(component, event, helper){
        component.getEvent("AddNewLineItemRowEvt").fire();
       
        /*if(multiple == 'true'){
            component.set("v.shipToAvailable", false);
        } else {
            component.set("v.shipToAvailable", true);
        }*/
    },
    
    removeRow : function(component, event, helper){
       component.set("v.deleted", true);
       component.getEvent("DeleteNewLineItemRowEvt").setParams({"indexVar" : component.get("v.rowIndex") }).fire();             
    },
    
    removeArrivalDate : function(component, event, helper){
        var checked = component.get("v.atOnce");		
        if (!checked){            
            component.set("v.atOnce", true);
        	component.find("arrivalDate").set('v.value', '');
            component.find("arrivalDate").set('v.disabled', true); 
        }else{
            component.set("v.atOnce", false);
            component.find("arrivalDate").set('v.disabled', false); 
        }       
    },
    
    updatelineItemInstanceValues : function(component, event, helper){
    	var lineItemInstance = component.get("v.lineItemInstance");
		var selectedLookUpItemRecord = component.get("v.selectedLookUpItemRecord");
        var selectedLookUpRecord = component.get("v.selectedLookUpRecord");        
        lineItemInstance.Store_Product__c = selectedLookUpItemRecord.Id;
        lineItemInstance.Ship_To__c = selectedLookUpRecord.Id;        
        component.set("v.lineItemInstance", lineItemInstance);
    },

    getDealCost: function(component, event, helper) {
        var record = component.get("v.selectedLookUpItemRecord").Cost_Per_Deal__c;
        component.set("v.lineItemInstance.Cost_per_Deal__c", record);
        
        /*var 'dealCostValue' = component.find("costPerDeal").get('v.value');
        
        var itemCost = component.get("v.costDeal");
        
        // discount costDeal

        var valueTest = itemCost - dealCostValue;
        

        var discountValue = component.find("discount").get('v.value');

        //var applicationEvent = $A.get("e.c:CalculateDealCost");
        //applicationEvent.fire(); */
    },

    updateDealCost: function(component, event, helper){
        var dealCostValue = component.find("costPerDeal").get('v.value');
        
        var itemCost = component.get("v.selectedLookUpItemRecord").Cost_Per_Deal__c;
        
        // discount costDeal

        var itemLessDeal = ((itemCost - dealCostValue)*100)/itemCost;

        
        

        if(itemLessDeal > 0){
            component.find("discount").set('v.value', itemLessDeal.toFixed(2));
        } else {
            component.find("discount").set('v.value', 0);
        }

        
        //this.resetDealCost(component, event, helper);      
        var quantity = component.find("quantity").get('v.value');
        var costPerDeal = component.get("v.selectedLookUpItemRecord").Cost_Per_Deal__c;
        var discount = component.find("discount").get('v.value');
        var totalCost = 0; 

        
        

        if (discount === undefined) {
            discount = 0;
        }
        if (quantity != 0 && dealCostValue != 0){
            totalCost = (dealCostValue.toFixed(2) * quantity);   
            if (discount != 0) {  
                //totalCost = totalCost - ((totalCost * discount) / 100);
            }
        }
        
        component.find("totalDealCost").set('v.value', totalCost);
        component.getEvent("calculateOrderTotal").fire();
        component.getEvent("UpdateFinalTotal").fire();

        

        //var applicationEvent = $A.get("e.c:CalculateDealCost");
        //applicationEvent.fire();
    },
    
    calculateTotalDealCost : function(component, event, helper){  
        
        //this.resetDealCost(component, event, helper);      
        var quantity = component.find("quantity").get('v.value');
        var costPerDeal = component.get("v.selectedLookUpItemRecord").Cost_Per_Deal__c;
        var discount = component.find("discount").get('v.value');
        var totalCost = 0; 

        
        

        if (discount === undefined) {
            discount = 0;
        }
        if (quantity != 0 && dealCostValue != 0){
        	totalCost = (dealCostValue * quantity);   
            if (discount != 0) {  
            	//totalCost = totalCost - ((totalCost * discount) / 100);
            }
        }
        
        component.find("totalDealCost").set('v.value', totalCost);
        component.getEvent("calculateOrderTotal").fire();
        component.getEvent("UpdateFinalTotal").fire();
    },

    calculateTotalWithDiscount : function(component, event, helper){  

        

        var record = component.get("v.selectedLookUpItemRecord").Cost_Per_Deal__c;
        component.set("v.lineItemInstance.Cost_per_Deal__c", record);


        if(component.find("discount").get('v.value') > 100){
            component.find("discount").set('v.value', 100);
        }
        //this.resetDealCost(component, event, helper);      
        var quantity = component.find("quantity").get('v.value');
        var costPerDeal = record;
        var discount = component.find("discount").get('v.value');
        var totalCost = 0;        

        if (discount === undefined) {
            discount = 0;
        }
        if (quantity != 0 && dealCostValue != 0){
            totalCost = (dealCostValue * quantity);    
            if (discount != 0) {  
               // totalCost = totalCost - ((totalCost * discount) / 100);
            }
        }
        component.find("totalDealCost").set('v.value', totalCost);
        component.getEvent("calculateOrderTotal").fire();
        component.getEvent("UpdateFinalTotal").fire();
    },

    resetDealCost : function(component, event, helper){
        
    },

    
    RefreshLineItemElements : function(component, event, helper){
        
        var requiredFieldsMissing = false;
        component.set("v.itemContainerStyle", "");
        component.set("v.quantityContainerStyle", "");
        component.set("v.customerContainerStyle", "");

        component.set("v.calendarContainerStyle", "");
        component.set("v.atOnceContainerStyle", "");
        
        //Get Values to validate
        var selectedLookUpItemRecord = component.get("v.selectedLookUpItemRecord");
        var selectedLookUpRecord = component.get("v.selectedLookUpRecord");  
        var quantity = component.find("quantity").get('v.value');

        var atOnceCheckBox = component.find("atOnceCheckBox").get('v.value');
        var arrivalDate = component.find("arrivalDate").get('v.value');

        var message = event.getParam("message");
        var shipToAvailable = component.get("v.shipToAvailable");

        
        
        
        //Validations
        if (selectedLookUpItemRecord.Id === undefined){
            component.set("v.itemContainerStyle", component.get("v.errorStyle"));
        	requiredFieldsMissing = true;    
        }
        if (selectedLookUpRecord.Id === undefined && shipToAvailable == true){
			component.set("v.customerContainerStyle", component.get("v.errorStyle"));
        	requiredFieldsMissing = true;    
        }

        console.log('quantity: ' + quantity);

        if (quantity === undefined || quantity === 0 || quantity == null){
            component.set("v.quantityContainerStyle", component.get("v.errorStyle"));                        
            requiredFieldsMissing = true;	   
        }

        if (arrivalDate === undefined || arrivalDate == null || arrivalDate == '' || atOnceCheckBox == false){
            component.set("v.calendarContainerStyle", component.get("v.errorStyle"));                        
            requiredFieldsMissing = true;      
        }


        if (atOnceCheckBox === undefined || atOnceCheckBox == null || atOnceCheckBox == false){
            component.set("v.atOnceContainerStyle", component.get("v.errorStyle"));                        
            requiredFieldsMissing = true;      
        }

        if(atOnceCheckBox != undefined || atOnceCheckBox != null || atOnceCheckBox == true){
            
            component.set("v.calendarContainerStyle", null);                        
            requiredFieldsMissing = false;  
        }

        if(arrivalDate == '' && atOnceCheckBox == false){
            component.set("v.calendarContainerStyle", component.get("v.errorStyle"));                        
            requiredFieldsMissing = true;      

            
        }

        if(arrivalDate != '' && arrivalDate != undefined && arrivalDate != null){
            component.set("v.atOnceContainerStyle", "");                        
            requiredFieldsMissing = false;           

            
        }

        
		//Fire event to be handled by Shell component        
		component.getEvent("RequiredFieldsMissing").setParams({
        	"currentLineItemFailedValidation" : requiredFieldsMissing,
        	"lineItemElement" : component.get("v.lineItemInstance")      
        }).fire();
        
    },

    handleApplicationEvent : function(component, event) {
        var message = event.getParam("message");

        if(message == 'true'){
            component.set("v.shipToAvailable", false);
        } else {
            component.set("v.shipToAvailable", true);
        }

        

    },
    
    selectedItemChanged : function(component, event, helper){
    	component.find("quantity").set('v.value', 0);
   		component.find("discount").set('v.value', 0);
    	component.find("totalDealCost").set('v.value', 0);
        component.getEvent("calculateOrderTotal").fire();
    },
    
    getLineItemTotalCosts : function(component, event, helper){
        var isDeleted = component.get("v.deleted");
        if(!isDeleted){
            var totalDealCost = component.find("totalDealCost").get('v.value');
            if (totalDealCost === undefined || totalDealCost == null){totalDealCost = 0}
            component.getEvent("SendCurrentTotalCostToShell").setParams({"currentTotal" : totalDealCost }).fire();   
        }       
    }
    
})
trigger ShowCartTrigger on Show_Cart__c (after delete, after insert, after undelete, after update, before delete, before insert, before update) {
	
	if(Trigger.isAfter)
	{
		if(Trigger.isUpdate){
			ShowCartTriggerHandler.handleAfterUpdate();
		}
	}
	else
	{
		if(Trigger.isInsert){
			ShowCartTriggerHandler.handleBeforeInsert();
		}
	/*	else if(Trigger.isUpdate){
			ShowCartTriggerHandler.handleBeforeUpdate();
		}*/
	}
    
}
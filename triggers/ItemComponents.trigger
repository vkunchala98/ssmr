Trigger ItemComponents on Item_Component__c (before insert, after insert, after update) {
	
	List<Item_Component__c> records = Trigger.isDelete ? Trigger.old : Trigger.new;

	if(Trigger.isInsert){
		if(Trigger.isBefore){
			UItemComponent.setSuggestedRetail(records, Trigger.oldMap);
		}else if(Trigger.isAfter){
			UItemComponent.checkItemCost(records, Trigger.oldMap);
		}
	}else if(Trigger.isUpdate){
		if(Trigger.isAfter){
			UItemComponent.checkItemCost(records, Trigger.oldMap);
		}
	}
}
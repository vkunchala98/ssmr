trigger POs on PO__c (before insert, after insert, after update) {

	List<PO__c> records = Trigger.isDelete ? Trigger.old : Trigger.new;

	if(Trigger.isBefore){
		if(Trigger.isInsert){
			UPO.setCustomerVendorTerms(records, Trigger.oldMap);
		}
	}

	if(Trigger.isBefore){
		Id rtId = [SELECT Id FROM RecordType WHERE Name = 'Assorted' LIMIT 1].Id;
		for(PO__c p : Trigger.New){
			if(p.RecordTypeID == rtId){
				p.Sent_to_Vendor__c = true;
				p.Sent_to_Customer__c = true;
			}
		}
	}
	if(Trigger.isAfter){
		if(Trigger.isInsert){
			Id rtId = [SELECT Id FROM RecordType WHERE Name = 'Assorted' LIMIT 1].Id;
			List<PO_Line_Item__c> newPOLIs = new List<PO_Line_Item__c>();
			List<Id> centralPOIds = new List<Id>();
			
			for(PO__c p : Trigger.New){
				if(p.RecordTypeID == rtId){
					PO_Line_Item__c poli = new PO_Line_Item__c();
					poli.PO__c = p.Id;
					poli.Store_Product__c = p.Item__c;
					poli.Cost_per_Deal__c = p.Amount_Assorted__c;
					poli.Item_Number__c = 'ASSORTED';
					poli.Quantity__c = 1;
					poli.Registered__c = true;
					poli.Vendor_Invoice_Num__c = p.Vendor_Invoice_Num__c;
					poli.Vendor_Invoice_Date__c = p.Vendor_Invoice_Date__c;
					if(p.PO_Type__c == 'Central Bill'){
						centralPOIds.add(p.Id);
					}
					newPOLIs.add(poli);
				}
			}
			if(newPOLIs.size() > 0){
				insert newPOLIs;
			}
			for(Id poId : centralPOIds){
				CreateCentralBillInvoiceController.CreateCentralBillInvoiceController(poId);
			}
		}

		if(Trigger.isUpdate){
			List<Id> poIds = new List<Id>();
			for(PO__c purchaseOrder : Trigger.New){
				poIds.add(purchaseOrder.Id);
			}

			List<PO__c> posanditems = [SELECT Name, Register_ALL_Items__c, Vendor_Invoice_Num__c, Vendor_Invoice_Date__c, (SELECT Id FROM PO_Line_Items__r) FROM PO__c WHERE Id IN :poIds];
			List<PO_Line_Item__c> updateThese = new List<PO_Line_Item__c>();

			for(PO__c po : posanditems){
				if(po.Register_ALL_Items__c == TRUE){
					for(PO_Line_Item__c poli : po.PO_Line_Items__r){
						poli.Registered__c = TRUE;
						poli.Vendor_Invoice_Num__c = po.Vendor_Invoice_Num__c;
						poli.Vendor_Invoice_Date__c = po.Vendor_Invoice_Date__c;
						updateThese.add(poli);
					}
				}
			}
			update updateThese;
		}
	}
}
trigger POLineItems on PO_Line_Item__c (after delete, after insert, after undelete, after update, before delete, before insert, before update){

	if(Trigger.isAfter)
	{
		if(Trigger.isInsert){
			PoLineItemTriggerHandler.handleAfterInsert();
		}
		else if(Trigger.isUpdate){
			PoLineItemTriggerHandler.handleAfterUpdate();
		}
	}
	
}
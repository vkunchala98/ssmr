/** Class Name   : ContentdocLinkTrigger 
*  Description  : Trigger to share the files to community users
*  Created By   : BTG
*  Created On   : 03-05-2019
*  Modification Log:  
*  --------------------------------------------------------------------------------------------------------------------------------------
*   Developer                Date                   Modification ID      Description 
*  ---------------------------------------------------------------------------------------------------------------------------------------
*                                                                                    
**/
trigger ContentdocLinkTrigger on ContentDocumentLink (before insert,After insert) {
     /**
    * Method description :This is to share the files to community users.
    * @param  : 
    * @return : 
    **/
    if(Trigger.isInsert && Trigger.isBefore){
        for(ContentDocumentLink cdl:trigger.new){
            cdl.Visibility = 'AllUsers';
        } 
    }
     /**
    * Method description :This is to share the atatchement url to upload pictures. This url is used in drawloop template.
    * @param  : 
    * @return : 
    **/
    
     if(Trigger.isInsert && Trigger.isAfter){
        Schema.DescribeSObjectResult inv = Upload_Picture__c.sObjectType.getDescribe();
        String invKeyPrefix = inv.getKeyPrefix();
        Map<Id,Id> contDocLinkedMap = new Map<Id,Id>();
        Map<Id,Id> contentversiondocMap= new Map<Id,Id>();
        Map<Id,Upload_Picture__c> uploadMap= new Map<Id,Upload_Picture__c>();
        List<Upload_Picture__c> PicturestoUpdate= new List<Upload_Picture__c>();
        for(ContentDocumentLink conlink:Trigger.new){
            if(String.Valueof(conlink.LinkedEntityId).StartsWith(invKeyPrefix)){
                System.debug('@####$#$%%$');
                contDocLinkedMap.put(conlink.ContentDocumentId,conlink.LinkedEntityId);
            }
        }
        system.debug('RERERUYYYYYT#$$#'+contDocLinkedMap);
        List<ContentDocument> contentdocList=[select id,Title,Description from ContentDocument where id In:contDocLinkedMap.keyset()];
        system.debug('TTTYJTTI'+contentdocList);
        List<ContentVersion> contentvesrionslist=[select id,Title,ContentDocumentId,VersionData,VersionNumber,ContentBodyId,ContentLocation,ContentUrl,FileType from ContentVersion where ContentDocumentId In:contDocLinkedMap.keyset()];
        for(ContentVersion version:contentvesrionslist){
            contentversiondocMap.put(version.ContentDocumentId,version.id);
        }
        system.debug('RDERDDRR%$#%$'+contentvesrionslist);
        //Blob afterblob = EncodingUtil.base64Decode(contentvesrionslist[0].VersionData);
        // System.debug('$#^&^&%^%'+afterblob);
        List<Upload_Picture__c> pictures=[select id,Name,Display_Component_Image__c from Upload_Picture__c where Id=:contDocLinkedMap.Values()];
        for(Upload_Picture__c Upic:pictures){
            uploadMap.put(Upic.id,Upic);
        }
        for(ContentDocument content:contentdocList){
            if(contDocLinkedMap.containskey(content.id)){
                System.debug('YFGUYIU');
                String uploadid=contDocLinkedMap.get(content.id);
                Upload_Picture__c upload=uploadMap.get(uploadid);
                String VersionId=contentversiondocMap.get(content.id);
                upload.Version_Id__c=VersionId;
                PicturestoUpdate.add(upload);
            }
        }
        if(!PicturestoUpdate.isEmpty()){
            Update PicturestoUpdate;
            system.debug('EEER'+PicturestoUpdate);
        }
        
    }
}